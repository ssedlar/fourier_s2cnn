# Fourier_S2CNN

Fourier domain spherical CNN for microstructure parameter estimation from dMRI data.

![architecture_1](.architecture_CG.png)

![architecture_2](.architecture_zonal.png)

# Description:

# Dependencies:
* . python 3.6.13       
* . dipy                
* . matplotlib          
* . tensorflow 2.6.2    

# Stand-alone examples:


# References:

[1] Sedlar, S., Alimi, A., Papadopoulo, T., Deriche, R., & Deslauriers-Gauthier, S. (2021, September). A spherical convolutional neural network for white matter structure imaging via dMRI. In MICCAI 2021-24th International Conference on Medical Image Computing and Computer Assisted Intervention.

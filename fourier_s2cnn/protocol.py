"""Mother class for dMRI acquisition protocol management."""

import numpy as np
from collections import OrderedDict
from dipy.core.geometry import cart2sphere

PI = 3.141592653589793      # pi constant
GM_RATIO_HZ = 42.58 * 1.e6  # gyromagnetic ration [Hz/T]


class Protocol(object):
    """Class for dMRI acquisition protocol management."""

    """
        Attributes:
            prt_path        (str): path to the protocol
            prt_lib         (str): library to use to create gradient table;
                                   options: 'dipy'/'dmipy'
            gm_ratio      (float): gyro-magnetic ratio
            b0_th         (float): threshold below which b values 
                                   are consider to be 0
            b_shell_abs_d (float): minimum b shell absolute distance
            b_shell_rel_d (float): minimum b shell relative distance

        Methods:
            set_acquisition_path : sets protocol/acquisition path
            load_gradient_scheme : loads acquisition protocol
            name                 : returns object's name including parameters
    """
    def __init__(self, prt_path=None, prt_lib='dipy',
                 gm_ratio=2*PI*GM_RATIO_HZ,
                 b0_th=10*1.e6, b_shell_abs_d=40*1.e6, b_shell_rel_d=0.05):
        """Initialization of class attributes."""
        self.prt_path = prt_path
        self.prt_lib = prt_lib

        self.gm_ratio = gm_ratio

        self.b0_th = b0_th
        self.b_shell_abs_d = b_shell_abs_d
        self.b_shell_rel_d = b_shell_rel_d

    def set_acquisition_path(self, prt_path):
        """Set acquisition protocol path."""
        """
            Arguments:
                prt_path (str): path to the acquisition protocol
        """
        self.prt_path = prt_path

    def load_gradient_scheme(self):
        """Load acquisition protocol table."""
        raise NotImplementedError()

    def get_s2_signal_points(self):
        """Create dictionary of shells and gradient coordinates."""
        """
            Arguments:

            Returns:
                OrderedDict: dictionary containing S2 gradient coordinates
                for each shell
        """
        bvecs = OrderedDict()
        for k in self.shell_masks:
            bvecs[k] = np.zeros((self.shell_masks[k].shape[0], 3))
            if self.prt_lib == 'dipy':
                bvecs_crt = self.gradient_scheme.bvecs[self.shell_masks[k], :]
            elif self.prt_lib == 'dmipy':
                bvecs_crt = self.gradient_scheme.\
                    gradient_directions[self.shell_masks[k], :]
            for i in range(self.shell_masks[k].shape[0]):
                bvecs[k][i, :] = cart2sphere(bvecs_crt[i, 0],
                                             bvecs_crt[i, 1],
                                             bvecs_crt[i, 2])
                if bvecs[k][i, 2] < 0:
                    bvecs[k][i, 2] = 2 * np.pi + bvecs[k][i, 2]
        return bvecs

    def name(self):
        """Return object's name including parameters."""
        raise NotImplementedError()

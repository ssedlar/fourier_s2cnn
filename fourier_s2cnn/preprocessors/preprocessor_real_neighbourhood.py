"""Class for real dMRI data preprocessing per 3x3x3 patch"""

from .. import Preprocessor

import sys
import os
import numpy as np
import nibabel as nib

from scipy.ndimage.morphology import binary_dilation
from scipy import ndimage

from collections import OrderedDict
from fourier_s2cnn.utils.fourier_transforms \
    import gram_schmidt_sh_inv, lms_laplace_beltrami_sh_inv


class PreprocessorRealNeighbourhood(Preprocessor):
    """Class for real dMRI preprocessing and preparing per single voxel."""

    """
        Attributes:

            data_type (str): input data type
                options: 'raw' / 'sph'
            sph_inv (str): spherical harmonic basis inversion
                options: 'gs'-Gram-Schmidt, 'lb' - with Laplace Beltrami

            n_in_len (int): length of input data
            n_in_ch (int): number of channels of input data
            n_out_len (int): length of ground truth
            n_out_ch (int): number of channels of ground truth

        Methods:
            prepare_data: prepares data and gold standard for estimator
            prepare_data_raw: prepares raw data and gold standard for
                estimator
            prepare_data_sph: prepares sh data and gold standard for
                estimator
            name: returns object's name
    """
    def __init__(self, data_type='raw', sph_inv='gs', **kwargs):
        """Attribute initialization of the class PreprocessorReal."""
        super(PreprocessorRealNeighbourhood, self).__init__(**kwargs)

        self.data_type = data_type
        self.sph_inv = sph_inv

        self.n_in_len = None
        self.n_in_ch = None
        self.n_out_len = None
        self.n_out_ch = None

    def _load_data_scan(self, db, data_path):
        """Load dMRI data, mask and protocol of one scan."""
        """Filters out erroneous voxels with low mean b0 values from
            brain mask"""
        """
            Arguments:
                db (Database instance): database object
                data_path (str): path to the scan's dMRI data and protocol
            Returns:
                (4D numpy array, 3D numpy array, Protocol instance):
                    dMRI data, filtered brain mask and acquisition protocol
                    all of one scan
        """
        dMRI_path = os.path.join(data_path, db.data_name + '.nii.gz')
        data = np.asarray(nib.load(dMRI_path).dataobj)

        h, w, d, n = data.shape

        gt_path = os.path.join(data_path, '{}_bf_whole_brain.npy'.
                               format(self.gt_type, self.mask_type))
        gt = np.load(gt_path)

        if self.gt_type == 'smt':
            gt[:, 1, :] /= 3

        gt_3d = np.zeros((h, w, d, gt.shape[1]), dtype=np.float32)
        mask_wb_path = os.path.join(data_path, 'nodif_brain_mask.nii.gz')
        mask_wb = nib.load(mask_wb_path).get_data().astype(np.int32)
        gt_3d[mask_wb == 1, :] = gt[:, :, 0]

        if self.mask_type == 'whole_brain':
            mask = np.copy(mask_wb)
        elif self.mask_type == 'white_matter':
            tissue_path = os.path.join(data_path, '5ttgen.nii.gz')
            tissues = nib.load(tissue_path).get_data()[::-1]
            mask = np.argmax(tissues, axis=3) == 2
        prt = db.load_protocol(data_path)

        mask = self.filter_brain_mask(data, prt, mask)

        return data, np.expand_dims(gt_3d[mask == 1, :], axis=2), mask, prt

    def prepare_data(self, db):
        """Prepare data for estimator input."""
        if self.data_type == 'raw':
            self.prepare_data_raw(db)
        else:
            self.prepare_data_sph(db)

    def prepare_data_raw(self, db):
        """Prepare raw input data for the estimator and ground truth."""
        """
            Arguments:
                db (Database): Database object
        """
        # 1. Defining preprocessor's output path
        preproc_path = os.path.join(self.exp_out, db.name, self.name)
        preproc_gt_path = os.path.join(self.exp_out, db.name, 'ground_truth')

        # 2. Check if data have been already preprocessed
        if os.path.exists(os.path.join(preproc_path, 'data_info')):
            return

        # 3. If downsample, determine downsampling point indices
        ds_points = self.select_downsample_points(db)

        mask_shifts = []
        for i in range(-1, 2):
            for j in range(-1, 2):
                for k in range(-1, 2):
                    mask_shifts.append([i, j, k])

        # 4. Normalizing dMRI with b0 and reshaping
        for s in self.data_list:
            # 4.1. Get data output path, check if it is already computed
            data_name = 'data_{}.npy'.format(s)
            if os.path.exists(os.path.join(preproc_path, data_name)):
                continue
            if not os.path.exists(preproc_path):
                os.makedirs(preproc_path)
            if not os.path.exists(preproc_gt_path):
                os.makedirs(preproc_gt_path)

            # 4.2. Loading raw dMRI, brain mask and protocol
            data_path = db.dmri_template_path.format(db.subjects_list[s])
            data, gt, mask, prt = self._load_data_scan(db, data_path)

            struct_elem = \
                ndimage.generate_binary_structure(rank=3, connectivity=3)
            mask_d = binary_dilation(mask, struct_elem)

            # 4.3. Normalizing data with b0
            data_n_shells = self.normalize_data_scan(data, prt, mask_d)
            s2_coords_full = prt.get_s2_signal_points()

            s2_coords, data_n_shells =\
                self.downsample_data_scan(s2_coords_full, data_n_shells,
                                          ds_points)

            # 4.4. Allocating memory for spherical harmonics
            n_vox = int(np.sum(mask))
            signal_len = self.n_points // self.n_shells
            coords = np.zeros((signal_len, 3, self.n_shells),
                              dtype=np.float32)
            h, w, d = mask.shape
            data_raw = np.zeros((h, w, d, signal_len, self.n_shells),
                                dtype=np.float32)
            for sh_idx, sh in enumerate(data_n_shells):
                data_raw[mask_d == 1, :, sh_idx] = data_n_shells[sh]
                coords[:, :, sh_idx] = s2_coords[sh]

            data_raw_nb = np.zeros((n_vox, signal_len, 27 * self.n_shells),
                                   dtype=np.float32)
            bm_idx = np.where(mask)
            for sh_idx in range(self.n_shells):
                for m_idx, m_shift in enumerate(mask_shifts):
                    x_sh, y_sh, z_sh = m_shift
                    data_raw_nb[:, :, sh_idx * 27 + m_idx] = \
                        data_raw[bm_idx[0] + x_sh,
                                 bm_idx[1] + y_sh,
                                 bm_idx[2] + z_sh, :, sh_idx]

            # 4.6. Check compatibility of input data
            _, n_in_len, n_in_ch = data_raw_nb.shape
            _, n_out_len, n_out_ch = gt.shape
            if not self.n_in_len:
                self.n_in_len, self.n_in_ch = n_in_len, n_in_ch
                self.n_out_len, self.n_out_ch = n_out_len, n_out_ch
            else:
                if self.n_in_len != n_in_len or self.n_in_ch != n_in_ch:
                    raise ValueError('Input data not compatible!')
                if self.n_out_len != n_out_len or self.n_out_ch != n_out_ch:
                    raise ValueError('Ground truth data not compatible!')

            # 4.7. Saving data
            gt_name = '{}_{}.npy'.format(self.gt_type, s)
            np.save(os.path.join(preproc_gt_path, gt_name), gt)
            data_name = 'data_{}.npy'.format(s)
            np.save(os.path.join(preproc_path, data_name), data_raw_nb)
            coords_name = 'coords_{}.npy'.format(s)
            np.save(os.path.join(preproc_path, coords_name), coords)
            n_vox_name = 'n_vox_{}'.format(s)
            with open(os.path.join(preproc_path, n_vox_name), 'w+') as f:
                f.write('%d' % n_vox)

            # 4.8. Print progress in data preparation
            sys.stdout.write("\rPreparing raw data (b0 normalization): "
                             "%.3f %% / 100.00 %%"
                             % (100 * float(s + 1) / db.n_subjects))
            sys.stdout.flush()
        sys.stdout.write("\n")

        # 5. Saving information about input and ground truth data dimensions
        data_info_path = os.path.join(preproc_path, 'data_info')
        self.save_info(data_info_path, self.n_in_len, self.n_in_ch)
        gt_info_path = os.path.join(preproc_gt_path, self.gt_type + '_info')
        self.save_info(gt_info_path, self.n_out_len, self.n_out_ch)

    def prepare_data_sph(self, db):
        """Prepare raw input data for the estimator and ground truth."""
        """
            Arguments:
                db (Database): Database object
        """

        # 1. Defining preprocessor's output path
        preproc_path = os.path.join(self.exp_out, db.name, self.name)
        preproc_gt_path = os.path.join(self.exp_out, db.name, 'ground_truth')

        # 2. Check if data have been already preprocessed
        if os.path.exists(os.path.join(preproc_path, 'data_info')):
            return

        # 3. If downsample, determine downsampling point indices
        ds_points = self.select_downsample_points(db)

        mask_shifts = []
        for i in range(-1, 2):
            for j in range(-1, 2):
                for k in range(-1, 2):
                    mask_shifts.append([i, j, k])

        # 4. Normalizing dMRI with b0 and reshaping
        for s in self.data_list:
            # 4.1. Get data output path, check if it is already computed
            data_name = 'data_{}.npy'.format(s)
            if os.path.exists(os.path.join(preproc_path, data_name)):
                continue
            if not os.path.exists(preproc_path):
                os.makedirs(preproc_path)
            if not os.path.exists(preproc_gt_path):
                os.makedirs(preproc_gt_path)

            # 4.2. Loading raw dMRI, brain mask and protocol
            data_path = db.dmri_template_path.format(db.subjects_list[s])
            data, gt, mask, prt = self._load_data_scan(db, data_path)

            struct_elem = \
                ndimage.generate_binary_structure(rank=3, connectivity=3)
            mask_d = binary_dilation(mask, struct_elem)

            # 4.3. Normalizing data with b0
            data_n_shells = self.normalize_data_scan(data, prt, mask_d)
            s2_coords_full = prt.get_s2_signal_points()

            s2_coords, data_n_shells =\
                self.downsample_data_scan(s2_coords_full, data_n_shells,
                                          ds_points)

            self.spherical_harmonic_max_degree(s2_coords)

            Y_inv = OrderedDict()
            for sh_idx, sh in enumerate(s2_coords):
                s2_per_shell = s2_coords[sh][:, 1:]
                if self.sph_inv == 'lb':
                    Y_inv[sh] = \
                        lms_laplace_beltrami_sh_inv(s2_coord=s2_per_shell,
                                                    L=self.sh_B,
                                                    lambda_=0.00001)
                else:
                    Y_inv[sh] = \
                        gram_schmidt_sh_inv(s2_coord=s2_per_shell,
                                            L=self.sh_B,
                                            n_iters=1000)

            # 4.4. Allocating memory for spherical harmonics
            n_vox = int(np.sum(mask))
            h, w, d = mask.shape
            data_sph = np.zeros((h, w, d, self.n_sh, self.n_shells),
                                dtype=np.float32)
            for sh_idx, sh in enumerate(data_n_shells):
                data_sph[mask_d == 1, :, sh_idx] = \
                    np.dot(data_n_shells[sh], Y_inv[sh].transpose())

            data_sph_nb = np.zeros((n_vox, self.n_sh, 27 * self.n_shells),
                                   dtype=np.float32)

            bm_idx = np.where(mask)
            for sh_idx in range(self.n_shells):
                for m_idx, m_shift in enumerate(mask_shifts):
                    x_sh, y_sh, z_sh = m_shift
                    data_sph_nb[:, :, sh_idx * 27 + m_idx] = \
                        data_sph[bm_idx[0] + x_sh,
                                 bm_idx[1] + y_sh,
                                 bm_idx[2] + z_sh, :, sh_idx]

            # 4.6. Check compatibility of input data
            _, n_in_len, n_in_ch = data_sph_nb.shape
            _, n_out_len, n_out_ch = gt.shape
            if not self.n_in_len:
                self.n_in_len, self.n_in_ch = n_in_len, n_in_ch
                self.n_out_len, self.n_out_ch = n_out_len, n_out_ch
            else:
                if self.n_in_len != n_in_len or self.n_in_ch != n_in_ch:
                    raise ValueError('Input data not compatible!')
                if self.n_out_len != n_out_len or self.n_out_ch != n_out_ch:
                    raise ValueError('Ground truth data not compatible!')

            # 4.7. Saving data
            gt_name = '{}_{}.npy'.format(self.gt_type, s)
            np.save(os.path.join(preproc_gt_path, gt_name), gt)
            data_name = 'data_{}.npy'.format(s)
            np.save(os.path.join(preproc_path, data_name), data_sph_nb)
            n_vox_name = 'n_vox_{}'.format(s)
            with open(os.path.join(preproc_path, n_vox_name), 'w+') as f:
                f.write('%d' % n_vox)

            # 4.8. Print progress in data preparation
            sys.stdout.write("\rPreparing sph data (b0 normalization): "
                             "%.3f %% / 100.00 %%"
                             % (100 * float(s + 1) / db.n_subjects))
            sys.stdout.flush()
        sys.stdout.write("\n")

        # 5. Saving information about input and ground truth data dimensions
        data_info_path = os.path.join(preproc_path, 'data_info')
        self.save_info(data_info_path, self.n_in_len, self.n_in_ch)
        gt_info_path = os.path.join(preproc_gt_path, self.gt_type + '_info')
        self.save_info(gt_info_path, self.n_out_len, self.n_out_ch)

    @property
    def name(self):
        """Return object's name including parameters."""
        return ("%s(mask_type=%s, n_shells=%s, n_points=%s, data_type=%s)"
                % (type(self).__name__,
                   self.mask_type, self.n_shells, self.n_points,
                   self.data_type))

"""Functions related to Clebsch-Gordan matrices."""
from sympy.physics.quantum.cg import CG
import numpy as np
import tensorflow as tf
from .fourier_transforms import convert_complex_to_real
from .fourier_transforms import convert_real_to_complex

def clebsch_gordan_matrix_C(L1, L2, L):
    """Define Clebsch-Gordan matrix."""
    """
        Arguments:
            L1 (int): degree of the first Wigner-D coefficients
            L2 (int): degree of the second Wigner-D coefficients
            L (int): degree of output RH coefficients

        Return:
            Clebsch-Gordan matrix
    """
    CG_M = np.zeros(((2 * L1 + 1) * (2 * L2 + 1), 2 * L + 1),
                    dtype=np.float32)
    for m in range(-L1, L1 + 1):
        for m_p in range(-L2, L2 + 1):
            M_p = m + m_p
            cg = CG(L1, m, L2, m_p, L, M_p).doit()
            if cg:
                CG_M[(m + L1) * (2 * L2 + 1) + m_p + L2, m + m_p + L] = cg
    return CG_M.T


def clebsch_gordan_matrix_R(L1, L2, L):
    """Create Clebsch-Gordan matrix associated to real WignerD matrices."""
    """
        Arguments:
            L1 (int): RH degree of the left RH coefficients
            L2 (int): RH degree of the right RH coefficients
            L (int): RH degree of the output RH coefficients
    """
    CG_M = np.zeros(((2 * L1 + 1) * (2 * L2 + 1), 2 * L + 1))
    for m in range(-L1, L1 + 1):
        for m_p in range(-L2, L2 + 1):
            M_p = m + m_p
            cg = CG(L1, m, L2, m_p, L, M_p).doit()
            if cg:
                CG_M[(m + L1) * (2 * L2 + 1) + m_p + L2, m + m_p + L] = cg

    M1 = convert_complex_to_real(L1)
    M2 = convert_complex_to_real(L2)
    M = convert_complex_to_real(L)

    if (L1 + L2 + L) % 2:
        return np.imag(np.dot(np.dot(np.kron(M1, M2), CG_M), M.conj().T)).T
    else:
        return np.real(np.dot(np.dot(np.kron(M1, M2), CG_M), M.conj().T)).T

def clebsch_gordan_dict(L, L_max, is_tf=True):
    """Create dictionary with Clebsch-Gordan matrices."""
    """Associated to complex Wigner-D matrices."""
    """
        Arguments:
            L (int) : maximal RH degree of input
            L_max (int): maximal RH degree of output
            is_tf (bool): flag indicating whether to convert np to tf tensor
        Returns:
            dictionary of Clebsch-Gordan matrices
    """
    if L_max > 2 * L:
        print("L_max should not be higher than 2*L")
        raise

    CG_dict = {}
    for l in range(0, L_max + 1):
        CG_dict[l] = {}
        for l1 in range(0, L + 1):
            for l2 in range(0, L + 1):
                if l2 < l1:
                    continue
                if np.abs(l2 - l1) <= l <= (l1 + l2):

                    cg_m = clebsch_gordan_matrix_C(l1, l2, l)

                    if is_tf:
                        cg_m = tf.convert_to_tensor(cg_m, dtype=tf.float32)

                    if l2 != l1:
                        CG_dict[l][l1, l2] = np.sqrt(2) * cg_m
                    else:
                        CG_dict[l][l1, l2] = cg_m
    return CG_dict


def clebsch_gordan_dict_R(L, L_max, is_tf=True):
    """Create dictionaries with Clebsch-Gordan matrices."""
    """Associated to real Wigner-D matrices."""
    """
        Arguments:
            L (int) : maximal RH degree of input
            L_max (int): maximal RH degree of output
            is_tf (bool): flag indicating whether to convert np to tf tensor
        Returns:
            dictionary of Clebsch-Gordan matrices
    """
    if L_max > 2 * L:
        print("L_max should not be higher than 2*L")
        raise

    CG_dict = {}
    for l in range(0, L_max + 1):
        CG_dict[l] = {}
        for l1 in range(0, L + 1):
            for l2 in range(0, L + 1):
                if l2 < l1:
                    continue
                if np.abs(l2 - l1) <= l <= (l1 + l2):

                    cg_m = clebsch_gordan_matrix_R(l1, l2, l)

                    if is_tf:
                        cg_m = tf.convert_to_tensor(cg_m, dtype=tf.float32)

                    if l2 != l1:
                        CG_dict[l][l1, l2] = np.sqrt(2) * cg_m
                    else:
                        CG_dict[l][l1, l2] = cg_m
    return CG_dict


def clebsch_gordan_dict_3d(L, L_max, is_tf=True):
    """Create dictionaries with Clebsch-Gordan reshaped matrices."""
    """Associated to complex Wigner-D matrices."""
    """
        Arguments:
            L (int) : maximal RH degree of input
            L_max (int): maximal RH degree of output
            is_tf (bool): flag indicating whether to convert np to tf tensor
        Returns:
            dictionary of Clebsch-Gordan matrices
    """
    if L_max > 2 * L:
        print("L_max should not be higher than 2*L")
        raise

    CG_dict_r = {}
    CG_dict_l = {}
    for l in range(0, L_max + 1):
        CG_dict_r[l] = {}
        CG_dict_l[l] = {}
        for l1 in range(0, L + 1):
            for l2 in range(0, L + 1):
                if l2 < l1:
                    continue
                if np.abs(l2 - l1) <= l <= (l1 + l2):

                    cg_ = clebsch_gordan_matrix_C(l1, l2, l)

                    cg_r = np.reshape(cg_,
                                      [2 * l + 1, 2 * l1 + 1, 2 * l2 + 1])
                    cg_l = np.reshape(cg_,
                                      [2 * l + 1, 2 * l1 + 1, 2 * l2 + 1]).\
                        transpose(0, 2, 1)

                    if is_tf:
                        cg_r = tf.convert_to_tensor(cg_r, dtype=tf.float32)
                        cg_l = tf.convert_to_tensor(cg_l, dtype=tf.float32)

                    if l2 != l1:
                        CG_dict_r[l][l1, l2] = np.sqrt(2, dtype=np.float32) * cg_r
                        CG_dict_l[l][l1, l2] = np.sqrt(2, dtype=np.float32) * cg_l
                    else:
                        CG_dict_r[l][l1, l2] = cg_r
                        CG_dict_l[l][l1, l2] = cg_l
    return CG_dict_r, CG_dict_l


def CG_dict_3d_R(L, L_max, is_tf=True):
    """Create dictionaries with Clebsch-Gordan reshaped matrices."""
    """Associated to real Wigner-D matrices."""
    """
        Arguments:
            L (int) : maximal RH degree of input
            L_max (int): maximal RH degree of output
            is_tf (bool): flag indicating whether to convert np to tf tensor
        Returns:
            dictionary of Clebsch-Gordan matrices
    """
    if L_max > 2 * L:
        print("L_max should not be higher than 2*L")
        raise

    CG_dict_r = {}
    CG_dict_l = {}
    for l in range(0, L_max + 1):
        CG_dict_r[l] = {}
        CG_dict_l[l] = {}
        for l1 in range(0, L + 1):
            for l2 in range(0, L + 1):
                if l2 < l1:
                    continue
                if np.abs(l2 - l1) <= l <= (l1 + l2):

                    cg_ = clebsch_gordan_matrix_R(l1, l2, l)

                    cg_r = np.reshape(cg_, [2 * l + 1, 2 * l1 + 1, 2 * l2 + 1])
                    cg_l = cg_r.transpose(0, 2, 1)

                    if is_tf:
                        cg_r = tf.convert_to_tensor(cg_r, dtype=tf.float32)
                        cg_l = tf.convert_to_tensor(cg_l, dtype=tf.float32)

                    if l2 != l1:
                        CG_dict_r[l][l1, l2] = np.sqrt(2, dtype=np.float32) * cg_r
                        CG_dict_l[l][l1, l2] = np.sqrt(2, dtype=np.float32) * cg_l
                    else:
                        CG_dict_r[l][l1, l2] = cg_r
                        CG_dict_l[l][l1, l2] = cg_l

    return CG_dict_r, CG_dict_l

def CG_dict_3d_R_sparse(L, L_max, is_tf=True):
    """Create dictionaries with Clebsch-Gordan reshaped matrices."""
    """Associated to real Wigner-D matrices."""
    """
        Arguments:
            L (int) : maximal RH degree of input
            L_max (int): maximal RH degree of output
            is_tf (bool): flag indicating whether to convert np to tf tensor
        Returns:
            dictionary of Clebsch-Gordan matrices
    """
    if L_max > 2 * L:
        print("L_max should not be higher than 2*L")
        raise

    CG_dict_r = {}
    CG_dict_l = {}
    for l in range(0, L_max + 1):
        CG_dict_r[l] = {}
        CG_dict_l[l] = {}
        for l1 in range(0, L + 1):
            for l2 in range(0, L + 1):
                if l2 < l1:
                    continue
                if np.abs(l2 - l1) <= l <= (l1 + l2):

                    cg_ = clebsch_gordan_matrix_R(l1, l2, l)

                    cg_r = np.reshape(cg_, [2 * l + 1, 2 * l1 + 1, 2 * l2 + 1])
                    cg_l = cg_r.transpose(0, 2, 1)

                    if is_tf:
                        cg_r = tf.convert_to_tensor(cg_r, dtype=tf.float32)
                        cg_l = tf.convert_to_tensor(cg_l, dtype=tf.float32)

                    if l2 != l1:
                        CG_dict_r[l][l1, l2] = \
                            tf.sparse.from_dense(tf.transpose(tf.reshape(np.sqrt(2, dtype=np.float32) * cg_r, 
                                                                         [-1, 2 * l2 + 1]), (1, 0)))
                        CG_dict_l[l][l1, l2] = \
                            tf.sparse.from_dense(tf.transpose(tf.reshape(np.sqrt(2, dtype=np.float32) * cg_l, 
                                                                         [-1, 2 * l1 + 1]), (1, 0)))
                    else:
                        CG_dict_r[l][l1, l2] = \
                            tf.sparse.from_dense(tf.transpose(tf.reshape(cg_r, [-1, 2 * l2 + 1]), (1, 0)))
                        CG_dict_l[l][l1, l2] = \
                            tf.sparse.from_dense(tf.transpose(tf.reshape(cg_l, [-1, 2 * l1 + 1]), (1, 0)))

    return CG_dict_r, CG_dict_l

"""Conversion of sampling points between Cartesian and spherical coordinates."""

import numpy as np
from dipy.core.geometry import cart2sphere, sphere2cart


def convert_s2_to_cart(points_sph):
    """Covert array of points from S2 to Cartesian coordinate system."""
    """
        Arguments:
            points_cart (numpy array N x 2): array of points in S2
                coordinate system (assumed r=1)
        Returns:
            numpy array N x 3: array of points in Cartesian coordinate system
    """
    points_cart = np.zeros((points_sph.shape[0], 3))
    for i in range(points_sph.shape[0]):
        theta, phi = points_sph[i, :]
        x, y, z = sphere2cart(1, theta, phi)
        points_cart[i, 0] = x
        points_cart[i, 1] = y
        points_cart[i, 2] = z
    return points_cart


def convert_cart_to_s2(points_cart):
    """Covert array of points from Cartesian to S2 coordinate system."""
    """
        Arguments:
            points_cart (numpy array N x 3): array of points in Cartesian
                coordinate system
        Returns:
            numpy array N x 2: array of points in S2 coordinate system
                (where r=1)
    """
    points_s2 = np.zeros((points_cart.shape[0], 2))
    for i in range(points_cart.shape[0]):
        x, y, z = points_cart[i, :]
        r, theta, phi = cart2sphere(x, y, z)
        points_s2[i, 0] = theta
        points_s2[i, 1] = phi

    return points_s2

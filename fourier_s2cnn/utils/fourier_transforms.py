
import numpy as np
from scipy.special import sph_harm
import wigner as wm


def convert_real_to_complex(L):
    """Convert real to complex spherical harmonics."""
    """
        Arguments:
            L (int): maximum SH degree
        Returns:
            numpy array: mapping between real and complex SH basis
    """
    n_sph = 2 * L + 1

    M = np.zeros((n_sph, n_sph), dtype=np.complex64)

    M[L, L] = 1
    for m in range(-L, 0):
        M[m + L, m + L] = -1 / np.sqrt(2) * 1j
        M[m + L, -m + L] = 1 / np.sqrt(2)
        M[-m + L, m + L] = np.power(-1, np.abs(m)) / np.sqrt(2) * 1j
        M[-m + L, -m + L] = np.power(-1, np.abs(m)) / np.sqrt(2)

    return M


def convert_complex_to_real(L):
    """Convert complex to real spherical harmonics."""
    """
        Arguments:
            L (int): maximum SH degree
        Returns:
            numpy array: mapping between complex and real SH basis
    """
    M = convert_real_to_complex(L)

    return M.conj().T


def sh_basis_complex(s2_coord, L, even=True):
    """Complex spherical harmonic basis."""
    """
        Arguments:
            s2_coord (numpy array): S2 points coordinates
            L (int): maximal spherical harmonic degree
            even (bool): flag indicating whether to compute only
                spherical harmonics of even degree
        Returns:
            numpy array: spherical harmonic bases (each column is a basis)
    """
    s = 2 if even else 1
    n_sph = np.sum([2 * i + 1 for i in range(0, L + 1, s)])
    Y = np.zeros((s2_coord.shape[0], n_sph), dtype=np.complex64)
    n_sph = 0
    for i in range(0, L + 1, s):
        ns, ms = np.zeros(2 * i + 1) + i, np.arange(-i, i + 1)
        Y_n_m = sph_harm(ms, ns, s2_coord[:, 1:2], s2_coord[:, 0:1])
        Y[:, n_sph:n_sph + 2 * i + 1] = Y_n_m
        n_sph += 2 * i + 1
    return Y


def sh_basis_real(s2_coord, L, even=True):
    """Real spherical harmonic basis of even degree."""
    """
        Arguments:
            s2_coord (numpy array): S2 points coordinates
            L (int): spherical harmonic degree
            even (bool): flag indicating whether to compute only
                spherical harmonics of even degree
        Returns:
            numpy array: spherical harmonic bases (each column is a basis)
    """
    s = 2 if even else 1
    n_sph = np.sum([2 * i + 1 for i in range(0, L + 1, s)])
    Y = np.zeros((s2_coord.shape[0], n_sph), dtype=np.float32)
    n_sph = 0
    for i in range(0, L + 1, s):
        ns, ms = np.zeros(2 * i + 1) + i, np.arange(-i, i + 1)
        Y_n_m = sph_harm(ms, ns, s2_coord[:, 1:2], s2_coord[:, 0:1])
        if i > 0:
            Y_n_m[:, 0:i] = np.sqrt(2) * \
                np.power(-1, np.arange(i, 0, -1)) * np.imag(Y_n_m[:, :i:-1])
            Y_n_m[:, (i + 1):] = np.sqrt(2) * \
                np.power(-1, np.arange(1, i + 1)) * np.real(Y_n_m[:, (i + 1):])
        Y[:, n_sph:n_sph + 2 * i + 1] = Y_n_m
        n_sph += 2 * i + 1
    return Y


def lms_sh_inv(s2_coord, L, b_type='real'):
    """Inversion of spherical harmonic basis with least-mean square."""
    """
        Arguments:
            s2_coord (numpy array): S2 points coordinates
            L (int): spherical harmonic degree
        Returns:
            numpy array: inverted spherical harmonic bases
    """
    if b_type == 'real':
        Y = sh_basis_real(s2_coord, L)
    else:
        Y = sh_basis_complex(s2_coord, L)

    return np.linalg.pinv(Y)


def lms_tikhonov_sh_inv(s2_coord, L, lambda_, b_type='real'):
    """Inversion of spherical harmonic basis with least-mean square
       requilarized with Tikhonov regularization term."""
    """
        Reference:
        [1] Hess, Christopher P., et al.
        "Q-ball reconstruction of multimodal fiber orientations
         using the spherical harmonic basis."
        Magnetic Resonance in Medicine:
        An Official Journal of the International Society for
        Magnetic Resonance in Medicine 56.1 (2006):
        104-117.
        Arguments:
            s2_coord (numpy array): S2 points coordinates
            L (int): spherical harmonic degree
            lambda_ (float): regularization weight
        Returns:
            numpy array: inverted spherical harmonic bases
    """
    if b_type == 'real':
        Y = sh_basis_real(s2_coord, L)
    else:
        Y = sh_basis_complex(s2_coord, L)

    tikhonov_reg = np.eye(Y.shape[1], dtype=np.float32)
    return np.dot(np.linalg.inv(np.dot(Y.T, Y) + lambda_ * tikhonov_reg), Y.T)


def lms_laplace_beltrami_sh_inv(s2_coord, L, lambda_, b_type='real'):
    """Inversion of spherical harmonic basis with least-mean square
       requilarized with Laplace-Beltrami regularization term."""

    """
        References:
        [1] Descoteaux, Maxime, et al.
        "Regularized, fast, and robust analytical Q-ball imaging."
        Magnetic Resonance in Medicine: An Official Journal of the
        International Society for Magnetic Resonance in Medicine 58.3 (2007):
        497-510.
        Arguments:
            s2_coord (numpy array): S2 points coordinates
            L (int): spherical harmonic degree
            lambda_ (float): regularization weight
        Returns:
            numpy array: inverted spherical harmonic bases
    """
    if b_type == 'real':
        Y = sh_basis_real(s2_coord, L)
    else:
        Y = sh_basis_complex(s2_coord, L)

    lb_reg = np.zeros((Y.shape[1], Y.shape[1]), dtype=np.float32)
    count = 0
    for l in range(0, L + 1, 2):
        for m in range(-l, l + 1):
            lb_reg[count, count] = l ** 2 * (l + 1) ** 2
            count += 1
    return np.dot(np.linalg.inv(np.dot(Y.T, Y) + lambda_ * lb_reg), Y.T)


def gram_schmidt_sh_inv(s2_coord, L, n_iters=1000, b_type='real', even=True):
    """Inversion of spherical harmonic basis with Gram-Schmidt
       orthonormalization process"""
    """
        Arguments:
            s2_coord (numpy array): S2 points coordinates
            L (int): spherical harmonic degree
            n_iters (float): number of iterations for degree shuffling
        Returns:
            numpy array: inverted spherical harmonic bases
    """
    np.random.seed(1234)
    if b_type == 'real':
        Y = sh_basis_real(s2_coord, L)
    else:
        Y = sh_basis_complex(s2_coord, L)

    s = 2 if even else 1
    Y_inv_final = np.zeros_like(Y.T)
    for k in range(n_iters):
        order = []
        count_h = 0
        for i in range(0, L + 1, s):
            order_h = count_h + np.arange(0, 2 * i + 1)
            np.random.shuffle(order_h)
            order.extend(list(order_h))
            count_h += 2 * i + 1

        deorder = np.argsort(order)
        Y_inv = np.zeros_like(Y.T)
        for i in range(Y_inv.shape[0]):
            Y_inv[i, :] = Y[:, order[i]]
            for j in range(0, i):
                if np.sum(Y_inv[j, :] ** 2) > 1.e-8:
                    Y_inv[i, :] -= np.sum(Y_inv[i, :] * Y_inv[j, :]) / \
                        (np.sum(Y_inv[j, :] ** 2) + 1.e-8) * Y_inv[j, :]
            Y_inv[i, :] /= np.sqrt(np.sum(Y_inv[i, :] ** 2))
        Y_inv_final += Y_inv[deorder, :]
    Y_inv_final /= n_iters

    n = np.dot(Y[:, 0:1].T, Y[:, 0:1])[0, 0]
    Y_inv_final /= np.sqrt(n)

    return Y_inv_final


def wigner_D_complex(so3_points, L):
    """Create Wigner D matrices for given sampling SO(3) points."""
    """
        Arguments:
            so3_points (numpy array) N x 3: SO(3) sampling point coordinates
            L (int): Wigner D degree
    """
    wd_matrices = np.zeros((2 * L + 1, 2 * L + 1, so3_points.shape[0]), dtype=np.complex64)

    for p in range(so3_points.shape[0]):
        wd_matrices[:, :, p] = wm.D_matrix(L, so3_points[p, 1], so3_points[p, 0], so3_points[p, 2]).conj()
    return wd_matrices


def _convert_Wigner_D_complex2real(wd_c, L):
    """Conversion of normal (complex) Wigner D matrices to real ones."""
    """
        Arguments:
            wd_c (numpy array): complex Wigner D matrix
        Returns:
            numpy array: real Wigner D matrix
    """

    M = convert_complex_to_real(L)

    return np.dot(M, np.dot(wd_c, M.conj().T))


def wigner_D_real(so3_points, L):
    """Create real Wigner D matrices for given sampling SO(3) points."""
    """
        Arguments:
            so3_points (numpy array) N x 3: SO(3) sampling point coordinates
            L (int): Wigner D degree
    """
    wd_matrices = np.zeros((2 * L + 1, 2 * L + 1, so3_points.shape[0]), dtype=np.float32)

    for p in range(so3_points.shape[0]):
        wd_c = wm.D_matrix(L, so3_points[p, 1], so3_points[p, 0], so3_points[p, 2]).conj()
        wd_matrices[:, :, p] = _convert_Wigner_D_complex2real(wd_c, L)
    return wd_matrices


def rh_basis_complex(so3_points, L, even=True):
    """Create complex rotational harmonic basis / Wigner D matrices."""
    """
        Arguments:
            so3_points (numpy array) N x 3: SO(3) sampling point coordinates
            L (int): maximal rotation harmonic degree
        Returns:
            numpy array: rotational harmonic basis (each column is a basis)
    """
    s = 2 if even else 1
    n_rh = np.sum([(2 * i + 1)**2 for i in range(0, L + 1, s)])
    D = np.zeros((so3_points.shape[0], n_rh), dtype=np.complex64)
    rh_count = 0
    for l in range(0, L + 1, s):
        n_rh = (2 * l + 1) ** 2
        for ang_idx, ang in enumerate(so3_points):
            wd_c = wm.D_matrix(l, ang[1], ang[0], ang[2]).conj()
            D[ang_idx, rh_count:rh_count + n_rh] = np.ravel(wd_c)
        rh_count += n_rh
    return D


def rh_basis_real(so3_points, L, even=True):
    """Create real rotational harmonic basis / Wigner D matrices."""
    """
        Arguments:
            so3_points (numpy array) N x 3: SO(3) sampling point coordinates
            L (int): maximal rotation harmonic degree
        Returns:
            numpy array: rotational harmonic bases (each column is a basis)
    """
    s = 2 if even else 1
    n_rh = np.sum([(2 * i + 1)**2 for i in range(0, L + 1, s)])
    D = np.zeros((so3_points.shape[0], n_rh), dtype=np.float32)
    rh_count = 0
    for l in range(0, L + 1, s):
        n_rh = (2 * l + 1) ** 2
        for ang_idx, ang in enumerate(so3_points):
            wd_c = wm.D_matrix(l, ang[1], ang[0], ang[2]).conj()
            wd_r = _convert_Wigner_D_complex2real(wd_c, l)
            D[ang_idx, rh_count:rh_count + n_rh] = np.ravel(wd_r)
        rh_count += n_rh
    return D




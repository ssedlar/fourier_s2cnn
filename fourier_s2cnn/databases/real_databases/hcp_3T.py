"""Class for HCP 3T database management; child class of DatabaseReal."""

import os

from .. import DatabaseReal
from ...protocols import ProtocolHCP3T

# Lists of HCP subjects used in the experiments
exp_subjects = {50: ['100307', '100408', '101915', '103414', '105115',
                     '106016', '110411', '111312', '111716', '113619',
                     '115320', '117122', '118730', '118932', '120111',
                     '122317', '123117', '124422', '125525', '126325',
                     '127933', '128632', '129028', '130013', '130316',
                     '133928', '135932', '136833', '138534', '139637',
                     '148335', '149337', '149539', '151223', '151526',
                     '151627', '153025', '156637', '159340', '160123',
                     '161731', '162733', '163129', '176542', '178950',
                     '188347', '189450', '190031', '192540', '196750'],

                200: ['100206', '100307', '100408', '101006', '101915',
                      '103414', '104820', '105115', '106016', '108323',
                      '108828', '110411', '110613', '111312', '111413',
                      '111514', '111716', '113619', '114217', '115320',
                      '116524', '117122', '118730', '118932', '119126',
                      '120111', '122317', '122822', '123117', '123521',
                      '123723', '124422', '124826', '125525', '126325',
                      '127226', '127832', '127933', '128632', '129028',
                      '130013', '130316', '130417', '133928', '135932',
                      '136126', '136833', '137633', '138534', '139637',
                      '140824', '141422', '142626', '143830', '146331',
                      '148335', '148941', '149236', '149337', '149539',
                      '151223', '151526', '151627', '151728', '153025',
                      '153126', '154835', '155231', '155635', '156637',
                      '159138', '159340', '159441', '160123', '160830',
                      '161731', '162733', '163129', '164131', '165436',
                      '165840', '169444', '169545', '172938', '174437',
                      '175439', '176441', '176542', '177241', '178950',
                      '180432', '181131', '181636', '186444', '187850',
                      '188347', '189450', '190031', '191841', '192136',
                      '192540', '192843', '194645', '194746', '196750',
                      '198451', '198653', '202719', '203923', '204521',
                      '205725', '206222', '206929', '208630', '211821',
                      '212015', '212823', '214019', '248238', '250427',
                      '256540', '285345', '287248', '295146', '299760',
                      '303119', '316633', '316835', '342129', '346945',
                      '351938', '361941', '368551', '371843', '389357',
                      '395251', '397154', '406836', '413934', '419239',
                      '453542', '459453', '473952', '481042', '513130',
                      '513736', '516742', '520228', '525541', '529549',
                      '541943', '550439', '553344', '555651', '559457',
                      '565452', '567759', '570243', '572045', '579867',
                      '581450', '590047', '594156', '609143', '627852',
                      '633847', '638049', '654754', '672756', '673455',
                      '687163', '698168', '728454', '744553', '757764',
                      '760551', '771354', '789373', '792564', '809252',
                      '818859', '825654', '826353', '856463', '856766',
                      '856968', '872562', '878877', '885975', '896879',
                      '898176', '901442', '910443', '912447', '922854',
                      '947668', '952863', '969476', '984472', '992673']}


class DatabaseHCP3T(DatabaseReal):
    """Class for HCP 3T database management."""

    """Child class of DatabaseReal."""

    """
        Attributes:
            n_subjects (int): number of subjects (or to consider) in the
                database
            split (list[float]): fractions for split of data into
                train, valid and test subsets

            subjects_list (list[str]): list of the subjects in the database
            train_subjects (list[str]): list of the train subjects
            valid_subjects (list[str]): list of the valid subjects
            test_subjects (list[str]): list of the test subjects

            data_name (str): name of the dMRI scan
            dmri_template_path (str): template path to the dMRI data and
                protocol for one subject

        Methods:
            get_dmri_template_path: gets template path to the dMRI data
            set_dmri_template_path: sets template path to the dMRI data
            load_protocol: loads database compatible protocol for one subject
            load_subjects: loads list of subjects and splits them into
                train, valid and test subsets
            name: returns object's name
    """
    def __init__(self, n_subjects=50, split=[0.6, 0.2, 0.2], data_name='data',
                 **kwargs):
        """DatabaseHCP3T attribute initialization."""
        super(DatabaseHCP3T, self).__init__(**kwargs)

        assert n_subjects in exp_subjects,\
            "Invalid number of subjects in the experiments"
        self.n_subjects = n_subjects

        self.split = split

        self.subjects_list = None
        self.train_subjects = None
        self.valid_subjects = None
        self.test_subjects = None

        self.data_name = data_name
        self.dmri_template_path = None

    def get_dmri_template_path(self, template_inter='{}/T1w/Diffusion'):
        """Get template path to the dMRI data and protocol."""
        """
            Returns:
                str: template path to the dMRI data and protocol
        """
        return self.dmri_template_path

    def set_dmri_template_path(self, template_inter='{}/T1w/Diffusion'):
        """Set template path to the dMRI data and protocol."""
        """
            Arguments:
                template_iter (str): relative template path
                    to the dMRI data and protocol with respect
                    to database path
        """
        self.dmri_template_path = os.path.join(self.db_path, template_inter)

    @staticmethod
    def load_protocol(prt_path, prt_lib='dipy'):
        """Load corresponding acquisition protocol for a subject."""
        """
            Arguments:
                prt_path (str): path to the protocol of one subject
                prt_lib (str): library to use for protocol
                    options: 'dipy'/'dmipy'
            Returns:
                Protocol: object containing acquisition protocol
        """
        prt = ProtocolHCP3T(prt_path=prt_path, prt_lib=prt_lib)
        prt.load_gradient_scheme()
        return prt

    def load_subjects(self):
        """Load and splits list of subjects into train, valid and test."""

        if self.n_subjects == 50:
            self.subjects_list = exp_subjects[50]
        elif self.n_subjects == 200:
            self.subjects_list = exp_subjects[50]
            for s in exp_subjects[200]:
                if s not in self.subjects_list:
                    self.subjects_list.append(s)

        n_train = int(len(self.subjects_list) * self.split[0])
        self.train_subjects = self.subjects_list[0:n_train]
        n_valid = int(len(self.subjects_list) * self.split[1])
        self.valid_subjects = self.subjects_list[n_train:n_train + n_valid]
        self.test_subjects = self.subjects_list[n_train + n_valid:]

    @property
    def name(self):
        """Return object's name including parameters."""
        return ("%s(n_subjects=%s, split=%s)"
                % (type(self).__name__, self.n_subjects, self.split))

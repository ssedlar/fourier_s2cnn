"""Class for diffusion MRI real database management."""
from .. import Database


class DatabaseReal(Database):
    """Class for diffusion MRI real database management."""

    """Child class of Database."""

    """
        Attributes:

        Methods:
            load_protocol: loads protocol for a given subject
            load_subjects: loads subject names and splits them into
                train, valid and test subsets
            name: returns objects name
    """
    def __init__(self, **kwargs):
        """Attribute initialization of DatabaseReal."""
        super(DatabaseReal, self).__init__(**kwargs)

    @staticmethod
    def load_protocol(protocol_path, protocol_lib, reduced, ds):
        """Load acquisition protocol."""
        raise NotImplementedError()

    def load_subjects(self):
        """Load and split subject names into train, valid and test subsets."""
        raise NotImplementedError()

    @property
    def name(self):
        """Return object's name."""
        raise NotImplementedError()

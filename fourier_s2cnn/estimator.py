"""Class for dMRI microstructure estimation."""

import os
import numpy as np


class Estimator(object):
    """Class for dMRI microstructure estimation estimation."""

    """
        Attributes:
            exp_out (str): path where to store model and results
            signal_domain (str): signal domain that will be used as input to
                estimator; options: raw/sph
            gt_type (str): ground truth to be estimated

        Methods:
            get_exp_out_path: gets path to the experiment output
            set_exp_out_path: sets path to the experiment output
            train_and_valid: runs training and validation of the model
            test: runs testing of the model
            name: returns object's name
    """
    def __init__(self, exp_out=None, gt_type=None, signal_domain=None):
        """Initialize class attributes."""
        self.exp_out = exp_out
        self.gt_type = gt_type
        self.signal_domain = signal_domain

    def get_exp_out_path(self):
        """Get experiment output path."""
        """
            Returns:
                (str): path to the experiment output
        """
        return self.exp_out

    def set_exp_out_path(self, exp_out):
        """Set experiment output path."""
        """
            Arguments:
                exp_out_path (str): path to the experiment output
        """
        self.exp_out = exp_out

    @staticmethod
    def _load_data(db, prep, data_list):

        data_path = os.path.join(prep.exp_out, db.name, prep.name)
        gt_path = os.path.join(prep.exp_out, db.name, 'ground_truth')

        c = 0
        for f in data_list:
            n_vox_path = os.path.join(data_path, 'n_vox_' + str(f))
            with open(n_vox_path, 'r') as f_vox:
                c += int(f_vox.readline())

        data = np.zeros((c, prep.n_in_len, prep.n_in_ch), dtype=np.float32)
        gt = np.zeros((c, prep.n_out_len, prep.n_out_ch), dtype=np.float32)
        c = 0
        for f in data_list:
            dMRI_s_path = os.path.join(data_path, 'data_' + str(f) + '.npy')
            data_f = np.load(dMRI_s_path)
            gt_s_name = prep.gt_type + '_' + str(f) + '.npy'
            gt_s_path = os.path.join(gt_path, gt_s_name)
            gt_f = np.load(gt_s_path)
            data[c:c + gt_f.shape[0], :, :] = data_f
            gt[c:c + gt_f.shape[0], :, :] = gt_f
            c += gt_f.shape[0]
        return data, gt

    @staticmethod
    def _load_data_n(db, prep, data_list, n_samples=12800):

        data_path = os.path.join(prep.exp_out, db.name, prep.name)
        gt_path = os.path.join(prep.exp_out, db.name, 'ground_truth')

        c = n_samples * len(data_list)
        data = np.zeros((c, prep.n_in_len, prep.n_in_ch), dtype=np.float32)
        gt = np.zeros((c, prep.n_out_len, prep.n_out_ch), dtype=np.float32)
        c = 0
        for f in data_list:
            dMRI_s_path = os.path.join(data_path, 'data_' + str(f) + '.npy')
            data_f = np.load(dMRI_s_path)
            gt_s_name = prep.gt_type + '_' + str(f) + '.npy'
            gt_s_path = os.path.join(gt_path, gt_s_name)
            gt_f = np.load(gt_s_path)
            select = np.random.choice(gt_f.shape[0], n_samples, replace=False)
            data[c:c + n_samples, :, :] = data_f[select, :, :]
            gt[c:c + n_samples, :, :] = gt_f[select, :, :]
            c += n_samples
        return data, gt

    def train_and_valid(self, prt, db):
        """Run training and validation of the model."""
        """
            Arguments:
                prt (Protocol instance): protocol object
                db (Database instance): database object
        """
        raise NotImplementedError()

    def test(self, prt, db, epoch=None):
        """Run testing of the model (for a given epoch)."""
        """
            Arguments:
                prt (Protocol instance): protocol object
                db (Database instance): database object
                epoch (int): ordinal number of the epoch of model training
        """
        raise NotImplementedError()

    def name(self):
        """Return object's name."""
        raise NotImplementedError()

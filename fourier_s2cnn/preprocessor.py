"""Class for diffusion MRI data preprocessing."""

import os
from os.path import join
import numpy as np
import json
from collections import OrderedDict

class Preprocessor(object):
    """Class for diffusion MRI data preprocessing."""

    """
        Attributes:
            exp_out      (str): path to the experiment output
            prep_src_dir (str): path to the preprocessor's directory

            bw_type      (str): bandwidth type; options: strict/over
            sh_B         (int): maximal SH degree (bandwidth)

            n_shells     (int): number of shells
            n_points     (int): total number of sampling points

            b0_th      (float): b0 threshold to filter out erroneous voxels
            gt_type      (str): ground truth to be estimated
            mask_type    (str): brain mask; options: whole_brain/white_matter

            data_list   (list): list of all exp. subjects (generalized names)
            train_list  (list): list of train subjects (generalized names)
            valid_list  (list): list of valid subjects (generalized names)
            test_list   (list): list of test subjects (generalized names)

        Methods:
            set_exp_out_path         : sets path to the experiment output
            get_exp_out_path         : gets path to the experiment output
            set_prep_src_dir         : set path to the preprocessors'
                source directory
            get_prep_src_dir         : get path to the preprocessors'
                                       source directory

            generalize_data_names    : translates original subjects ids into
                                       numbers

            load_info                : loads info about dMRI and 
                                       ground truth dimensions
            save_info                : saves info about dMRI and 
                                       ground truth dimensions

            sh_max_degree            : computes spherical harmonics
                                       max degree given number of points

            filter_brain_mask        : filters out voxels with low b0 values

            normalize_data           : normalizes input dMRI data with average b0
            normalize_data_scan      : masks and normalizes data of one scan
                                       with mean b0 values

            select_downsample_points : computes optimal downsampling points
                                       based on a Q-sampling scheme
            downsample_data_scan     : downsamples acquisition scheme and 
                                       dMRI data
            name                     : returns object's name
    """
    def __init__(self, exp_out=None, bw_type='over', 
                 n_shells=2, n_points=60, gt_type='noddi',
                 b0_th=100, mask_type='white_matter'):
        """Preprocessor attribute initialization."""
        self.exp_out = exp_out
        self.prep_src_dir = None

        self.b0_th = b0_th
        self.gt_type = gt_type
        self.mask_type = mask_type

        self.bw_type = bw_type
        self.sh_B = None

        self.n_shells = n_shells
        self.n_points = n_points

        self.data_list = []
        self.train_list = []
        self.valid_list = []
        self.test_list = []

    def get_exp_out_path(self):
        """Get experiment output path."""
        """
            Returns:
                (str): path to the experiment output
        """
        return self.exp_out

    def set_exp_out_path(self, exp_out):
        """Set experiment output path."""
        """
            Arguments:
                exp_out_path (str) : path to the experiment output
        """
        self.exp_out = exp_out

    def get_prep_src_dir(self):
        """Get preprocessors' source directory path."""
        """
            Returns:
                (str) : preprocessors' directory path
        """
        return self.prep_src_dir

    def set_prep_src_dir(self, src_dir):
        """Set preprocessors' source directory path."""
        """
            Arguments:
                source_dir (str) : entire module's source directory path
        """
        self.prep_src_dir = join(src_dir, 'fourier_s2cnn', 'preprocessors')

    def generalize_data_names(self, db):
        """Change database specific naming into more general one."""
        """Creates corresponding train, valid and test lists."""
        """
            Arguments:
                db (Database) : database object
        """
        for s_idx, s in enumerate(db.subjects_list):
            self.data_list.append(s_idx)
            if s in db.train_subjects:
                self.train_list.append(s_idx)
            elif s in db.valid_subjects:
                self.valid_list.append(s_idx)
            elif s in db.test_subjects:
                self.test_list.append(s_idx)

    def load_info(self, db):
        """Load info about input and ground truth dimensions."""
        """
            db (Database) : database object
        """

        # 1. Input data length and the number of channels
        exp_path = join(self.exp_out, db.name)
        signal_info_path = join(exp_path, self.name, 'data_info')
        with open(signal_info_path, 'r') as f:
            info = [int(i) for i in str.split(f.readline(), ' ')[:-1]]
        self.n_in_len, self.n_in_ch = info[0:2]

        # 2. Output estimate length and the number of channels
        gt_info_path = join(exp_path, 'ground_truth', self.gt_type + '_info')
        with open(gt_info_path, 'r') as f:
            info = [int(i) for i in str.split(f.readline(), ' ')[:-1]]
            self.n_out_len, self.n_out_ch = info[0:2]

        # 3. Signal bandwidth
        self.sh_B = 0
        while (self.sh_B + 1) * (self.sh_B + 2) / 2 <= self.n_in_len:
            self.sh_B += 2
        if self.bw_type == 'strict': self.sh_B = self.sh_B - 2

    @staticmethod
    def save_info(info_path, ft_len, ft_ch):
        """Save info related to data dimensions."""
        """
            Arguments:
                info_path (str) : path to the file where to store info
                ft_len    (int) : feature len
                ft_ch     (int) : number of feature channels
        """
        if not os.path.exists(info_path):
            with open(info_path, 'w+') as f:
                f.write('%d %d ' % (ft_len, ft_ch))

    def sh_max_degree(self, s2_coord):
        """Compute SH max degree and number of SH basis elements."""
        """
            Arguments:
                s2_coords (dict) : dict containing sampling points per shell
        """
        max_p = max([s2_coord[k].shape[0] for k in s2_coord])
        if self.bw_type == 'strict' or self.bw_type == 'over':
            self.sh_B = 0
            while (self.sh_B + 1) * (self.sh_B + 2) / 2 <= max_p:
                self.sh_B += 2
            if self.bw_type == 'strict': self.sh_B = self.sh_B - 2
        else:
            raise ValueError("Bandwidth criterion not known.")

        self.n_sh = np.sum([2 * i + 1 for i in range(0, self.sh_B + 1, 2)])

    @staticmethod
    def normalize_data(prt, data):
        """Normalize input dMRI data with b0."""
        """
            Arguments:
                prt (Protocol instance) : object containing acquisition
                                          information
                data      (numpy array) : 2D numpy array; each row is one
                                          sample/voxel; 
                                          each column one acquisition point
            Returns:
                (numpy array) : 2D array with normalized dMRI;
                                each row is one sample/voxel; each column 
                                one acquisition point
        """
        data_b0 = np.mean(data[:, prt.gradient_scheme.b0s_mask], axis=1)
        data = data[:, :] / (np.expand_dims(data_b0, axis=1) + 1.e-10)
        data = np.clip(data, 0, 1)
        return data

    def normalize_data_scan(self, data, prt, mask):
        """Normalize data of one scan with mean b0 values."""
        """Create dictionary containing normalized data per
           shell for within mask voxels."""
        """
            Arguments:
                data      (numpy array) : 4D numpy array containing dMRI data
                prt (Protocol instance) : object containing acquisition
                                          protocol
                mask      (numpy array) : 3D numpy array containing brain mask
            Returns:
                (OrderedDict) : dictionary containing normalized dMRI data
                                per each shell
        """
        data_n = self.normalize_data(prt, data[mask == 1, :])
        data_n_shell = OrderedDict()
        for sh in prt.shell_masks:
            data_n_shell[sh] = data_n[:, prt.shell_masks[sh]]

        return data_n_shell

    def filter_brain_mask(self, data, prt, mask):
        """Filter out erroneous voxels with very low mean b0 values."""
        """
            Arguments:
                data      (numpy array) : 4D dMRI data
                prt (Protocol instance) : object containing acquisition
                    protocol
                mask      (numpy array) : 3D brain mask
            Returns:
                (numpy array) : 3D filtered brain mask
        """
        b0_mean = np.mean(data[:, :, :, prt.gradient_scheme.b0s_mask], axis=3)
        return mask * (b0_mean > self.b0_th)

    @staticmethod
    def _get_rotation_matrices(n_steps):
        """Create rotation matrices."""
        """
            Arguments:
                n_steps (int) : number of rotation steps over
                                inclination, azimuth and tilt

            Returns:
                (numpy array n_rots x 3 x 3) : rotation matrices
        """
        phi_ls = np.linspace(0, 2 * np.pi, n_steps, endpoint=False)
        th_ls = np.linspace(0, np.pi, n_steps, endpoint=False)
        psi_ls = np.linspace(0, 2 * np.pi, n_steps, endpoint=False)

        phi_grid, th_grid, psi_grid = np.meshgrid(phi_ls, th_ls, psi_ls)
        phi_grid = np.ravel(phi_grid)
        th_grid = np.ravel(th_grid)
        psi_grid = np.ravel(psi_grid)

        n_rots = np.power(n_steps, 3)
        r_phi = np.zeros((n_rots, 3, 3), dtype=np.float32)
        r_th = np.zeros((n_rots, 3, 3), dtype=np.float32)
        r_psi = np.zeros((n_rots, 3, 3), dtype=np.float32)

        r_phi[:, 0, 0], r_phi[:, 0, 1] = np.cos(phi_grid), -np.sin(phi_grid)
        r_phi[:, 1, 0], r_phi[:, 1, 1] = np.sin(phi_grid), np.cos(phi_grid)
        r_phi[:, 2, 2] = np.ones(n_rots, dtype=np.float32)

        r_th[:, 0, 0], r_th[:, 0, 2] = np.cos(th_grid), np.sin(th_grid)
        r_th[:, 2, 0], r_th[:, 2, 2] = -np.sin(th_grid), np.cos(th_grid)
        r_th[:, 1, 1] = np.ones(n_rots)

        r_psi[:, 0, 0], r_psi[:, 0, 1] = np.cos(psi_grid), -np.sin(psi_grid)
        r_psi[:, 1, 0], r_psi[:, 1, 1] = np.sin(psi_grid), np.cos(psi_grid)
        r_psi[:, 2, 2] = np.ones(n_rots, dtype=np.float32)

        r_phi_th = np.einsum('nij,njk->nik', r_phi, r_th)
        r_matrices = np.einsum('nij,njk->nik', r_phi_th, r_psi)

        return r_matrices

    def select_downsample_points(self, db, n_steps=100):
        """Select downsampled set of points."""
        """Select from the original full sampling scheme
           using Q-ball sampling scheme [1].
           The best match between full sampling schemes and
           Q-ball template are determined on training subset."""
        """
            Arguments:
                db (Dtabase instance) : database object
                n_steps         (int) : number of rotations per each
                                        azimuth, inclination and tilt angles
            Returns:
                (OrderedDict) : dictionary containing indices of selected
                                dowsampled points per each shell

            References:
            [1] Caruyer, Emmanuel, et al. "Design of multishell sampling
                schemes with uniform coverage in diffusion MRI."
                Magnetic resonance in medicine 69.6 (2013): 1534-1540.
        """

        # 1. Paths to templates
        ds_path = join(self.prep_src_dir, 'downsampled_schemes')
        ds_scheme_path = join(ds_path, 'samples_{}_shells_{}_points.txt'). \
            format(self.n_shells, self.n_points)
        ds_points_path = join(ds_path, 'points_{}_shells_{}_points.json'). \
            format(self.n_shells, self.n_points)

        # 2. If downsampling points are already computed, just load them
        if os.path.exists(ds_points_path):
            with open(ds_points_path, 'r') as f_json:
                return json.loads(f_json.read())

        # 3. Loading template sampling scheme with n_points and n_shells
        ds_scheme = {}
        with open(ds_scheme_path, 'r') as f_template_scheme:
            ds_scheme_lines = f_template_scheme.readlines()
        for l in ds_scheme_lines:
            if l[0] == '#':
                continue
            l_split = str.split(l, '\t')
            if int(l_split[0]) not in ds_scheme:
                ds_scheme[int(l_split[0])] = []
            ds_p = [float(ls) for ls in l_split[1:4]]
            ds_scheme[int(l_split[0])]. append(ds_p)
        for k in ds_scheme:
            ds_scheme[k] = np.asarray(ds_scheme[k], dtype=np.float32).T

        # 4. Load full sampling schemes of training subset
        train_coords = OrderedDict()
        for s in self.train_list:
            subj_id = db.subjects_list[s]
            prt = db.load_protocol(db.dmri_template_path.format(subj_id))
            train_coords[s] = OrderedDict()
            for sh in prt.shell_masks:
                train_coords[s][sh] = \
                    prt.gradient_scheme.bvecs[prt.shell_masks[sh], :]

        # 5. Get number of training samples, rotations, in and out points
        n_rots = n_steps ** 3
        n_p_in = train_coords[s][sh].shape[0]
        n_p_out = ds_scheme[k].shape[1]
        n_train = len(self.train_list)

        # 6. Visualization of Q-ball sampling scheme - template
        '''
        import matplotlib.pyplot as plt
        from mpl_toolkits.mplot3d import Axes3D

        colors = ['r', 'b', 'y', 'c', 'm', 'c', 'k']
        fig = plt.figure()
        ax = Axes3D(fig)
        for k_idx, k in enumerate(ds_scheme):
            k_orig = (k_idx + 1) * 1000
            ax.scatter(k_orig * ds_scheme[k][0, :],
                       k_orig * ds_scheme[k][1, :],
                       k_orig * ds_scheme[k][2, :], c=colors[k_idx])
        plt.show()
        '''

        # 6. Allocate memory for the best matches
        points_ds = {}
        for sh_idx in range(self.n_shells):
            points_ds[sh_idx] = np.zeros((n_rots, n_p_out), dtype=np.int32)

        # 7. Rotate template points over azimuth, inclination and tilt angles
        # (zyz convention) and look for the best match over subjects and
        # over shells and compute cosine similarities
        mean_cos_sim = np.zeros((n_rots, self.n_shells), dtype=np.float32)
        rot_matrices = self._get_rotation_matrices(n_steps=n_steps)
        for n in range(0, n_rots):
            rot_matrix = rot_matrices[n, :, :]
            for sh_idx in range(self.n_shells):
                sh_template = list(ds_scheme.keys())[sh_idx]
                tmp_rot = np.dot(rot_matrix, ds_scheme[sh_template])

                for s_idx, s in enumerate(train_coords):
                    sh_train = list(train_coords[s].keys())[sh_idx]
                    cs = np.dot(train_coords[s][sh_train], tmp_rot)
                    if not s_idx:
                        cos_sim = cs
                    else:
                        cos_sim += cs
                cos_sim /= n_train

                sims = 0
                for j in range(n_p_out):
                    arg_max = np.argmax(cos_sim)
                    r, c = arg_max // n_p_out, arg_max % n_p_out
                    points_ds[sh_idx][n, j] = r
                    sims += cos_sim[r, c]
                    cos_sim[r, :], cos_sim[:, c] = -2, -2
                mean_cos_sim[n, sh_idx] = sims / n_p_out

        # 8. Select points with the highest cosine similarity
        selected_ds = {}
        arg_max = np.argmax(np.sum(mean_cos_sim, axis=1))
        for sh_idx in range(self.n_shells):
            selected_ds[sh_idx] = sorted(points_ds[sh_idx][arg_max, :])
            selected_ds[sh_idx] = [int(i) for i in selected_ds[sh_idx]]
 
        # 9. Saving indices of selected downsampled points
        with open(ds_points_path, 'w') as f_json:
            json.dump(selected_ds, f_json)

        return selected_ds

    @staticmethod
    def downsample_data_scan(s2_coord, data_n_shells, ds_points):
        """Downsample acquisition protocol and dMRI data."""
        """
            Arguments:
                s2_coords     (OrderedDict) : dictionary containing original
                                              full sampling scheme per shell
                data_n_shells (OrderedDict) : dictionary containing dMRI
                                              data per shell
                ds_points            (dict) : dictionary containing indices of
                                              downsampled scheme per shell
            Returns:
                (OrderedDict) : containing downsampled acquisition points per
                                shell
                (OrderedDict) : containing downsampled dMRI data per shell
        """

        # 1. Downsampling full acquisition scheme and dMRI data
        s2_coord_ds = OrderedDict()
        data_n_shells_ds = OrderedDict()
        for sh_idx, sh in enumerate(ds_points):
            k_orig = list(s2_coord.keys())[sh_idx]
            s2_coord_ds[k_orig] = s2_coord[k_orig][ds_points[sh]]
            data_n_shells_ds[k_orig] = data_n_shells[k_orig][:, ds_points[sh]]

        # 2. Visualization of full and downsampled acquisition points per
        # shell
        '''
        #Visualization
        import matplotlib.pyplot as plt
        from mpl_toolkits.mplot3d import Axes3D
        from dipy.core.geometry import sphere2cart

        cart_coords = OrderedDict()
        for k in ds_points:
            k_orig = list(s2_coord.keys())[int(k)]
            cart_coords[k_orig] = []
            for i in range(s2_coord[k_orig].shape[0]):
                s2_point = sphere2cart(s2_coord[k_orig][i, 0],
                                       s2_coord[k_orig][i, 1],
                                       s2_coord[k_orig][i, 2])
                cart_coords[k_orig].append(s2_point)
            cart_coords[k_orig] = np.asarray(cart_coords[k_orig])

        cart_coord_ds = OrderedDict()
        for k in ds_points:
            k_orig = list(s2_coord.keys())[int(k)]
            cart_coord_ds[k] = cart_coords[k_orig][ds_points[k]]

        colors = ['r', 'b', 'y', 'c', 'm', 'c', 'k']
        fig = plt.figure()
        ax = Axes3D(fig)
        for k_idx, k in enumerate(ds_points):
            k_orig = list(s2_coord.keys())[int(k)]

            ax.scatter(k_orig * cart_coords[k_orig][:, 0],
                       k_orig * cart_coords[k_orig][:, 1],
                       k_orig * cart_coords[k_orig][:, 2],
                       c=colors[k_idx + 2])

            ax.scatter(k_orig * cart_coord_ds[k][:, 0],
                       k_orig * cart_coord_ds[k][:, 1],
                       k_orig * cart_coord_ds[k][:, 2],
                       c=colors[k_idx])
        plt.show()
        '''
        return s2_coord_ds, data_n_shells_ds

    @property
    def name(self):
        """Return object's name."""
        raise NotImplementedError()

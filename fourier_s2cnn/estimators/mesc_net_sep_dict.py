"""Class for dMRI descriptor (micro-structure parameters, fODF)
    estimation using MEDN."""

"""
Implementation of the article:
Chuyang Ye, \"Tissue microstructure estimation using a deep network 
inspired by a dictionary-based learning \"
MEDN - Microstructure Estimation using a Deep Network
Github page: https://github.com/PkuClosed/MEDN 

"""
import sys
import os

import numpy as np
import numpy.matlib

import time

import tensorflow as tf
from .. import Estimator


from tensorflow.keras.models import Sequential, Model
from tensorflow.keras.layers import Lambda, Dropout
from tensorflow.keras.optimizers import SGD, Adam
from tensorflow.keras.layers import ThresholdedReLU
from tensorflow.keras.layers import Dense, Input, Activation,  Flatten, Permute
from tensorflow.keras.layers import concatenate, add, multiply, subtract
from tensorflow.keras.constraints import NonNeg


class MESCNetSepDict(Model):

    def __init__(self, n_dictS=300, n_dictQ=300, n_channelsS=75, n_channelsQ=75, n_layers1=8, n_layers2=3,
                 relu_thresh=0.01, lr=1.e-4, n_feats=3):
        super(MESCNetSepDict, self).__init__()

        self.n_dictS = n_dictS
        self.n_dictQ = n_dictQ
        self.n_channelsS = n_channelsS
        self.n_channelsQ = n_channelsQ

        self.n_layers1 = n_layers1
        self.n_layers2 = n_layers2

        self.relu_thresh = relu_thresh

        self.n_feats = n_feats

        self.Ws = Dense(self.n_dictS, activation='linear', use_bias=True, name="Ws")
        self.Wq = Dense(self.n_dictQ, activation='linear', use_bias=True, name="Wq")

        self.Wfs = Dense(self.n_dictS, activation='linear', use_bias=True, name="Wfs")
        self.Wfq = Dense(self.n_dictQ, activation='linear', use_bias=True, name="Wfq")

        self.Wis = Dense(self.n_dictS, activation='linear', use_bias=True, name="Wis")
        self.Wiq = Dense(self.n_dictQ, activation='linear', use_bias=True, name="Wiq")

        self.W_fx = Sequential(name="W_fx")
        self.W_fx.add(Dense(self.n_dictS, activation='linear', use_bias=True, name="W_fx_1",
                            input_shape=(self.n_dictQ, self.n_dictS)))
        self.W_fx.add(Permute((2, 1)))
        self.W_fx.add(Dense(self.n_dictQ, activation='linear', use_bias=True, name="W_fx_2",
                            input_shape=(self.n_dictS, self.n_dictQ)))
        self.W_fx.add(Permute((2, 1)))

        self.W_ix = Sequential(name="W_ix")
        self.W_ix.add(Dense(self.n_dictS, activation='linear', use_bias=True, name="W_ix_1",
                            input_shape=(self.n_dictQ, self.n_dictS)))
        self.W_ix.add(Permute((2, 1)))
        self.W_ix.add(Dense(self.n_dictQ, activation='linear', use_bias=True, name="W_ix_2",
                            input_shape=(self.n_dictS, self.n_dictQ)))
        self.W_ix.add(Permute((2, 1)))

        self.S = Sequential(name="S")
        self.S.add(Dense(self.n_dictS, activation='linear', use_bias=True, name="S_1",
                         input_shape=(self.n_dictQ, self.n_dictS)))
        self.S.add(Permute((2, 1)))
        self.S.add(Dense(self.n_dictQ, activation='linear', use_bias=True, name="S_2",
                         input_shape=(self.n_dictS, self.n_dictQ)))
        self.S.add(Permute((2, 1)))

        self.H = Sequential(name="H")
        self.H.add(Dense(self.n_channelsS, activation='relu', name="Hs" + str(0),
                         input_shape=(self.n_dictQ, self.n_dictS)))
        self.H.add(Permute((2, 1)))
        self.H.add(Dense(self.n_channelsQ, activation='relu', name="Hq" + str(0)))
        self.H.add(Permute((2, 1)))
        for i in range(self.n_layers2 - 1):
            self.H.add(Dense(self.n_channelsS, activation='relu', name="Hs" + str(i + 1)))
            self.H.add(Permute((2, 1)))
            self.H.add(Dense(self.n_channelsQ, activation='relu', name="Hq" + str(i + 1)))
            self.H.add(Permute((2, 1)))
        self.H.add(Dense(1, activation='relu', name="Hs" + str(self.n_layers2)))
        self.H.add(Permute((2, 1)))
        self.H.add(Dense(self.n_feats, activation='relu', name="Hq" + str(self.n_layers2)))
        self.H.add(Permute((2, 1)))

        self.loss = tf.keras.losses.MeanSquaredError()

        self.optimizer = tf.keras.optimizers.Adam(lr)

    def __call__(self, input):

        input_1 = tf.gather(input, tf.range(0, 27), axis=2)
        input_2 = tf.gather(input, tf.range(27, 54), axis=2)
        Y = tf.concat([input_1, input_2], axis=1)

        Ws_output = self.Ws(Y)
        Ws_output = Permute((2, 1))(Ws_output)

        Wq_output = self.Wq(Ws_output)
        Wq_output = Permute((2, 1))(Wq_output)

        Wfs_output = self.Wfs(Y)
        Wfs_output = Permute((2, 1))(Wfs_output)

        Wfq_output = self.Wfq(Wfs_output)
        Wfq_output = Permute((2, 1))(Wfq_output)

        Wis_output = self.Wis(Y)
        Wis_output = Permute((2, 1))(Wis_output)

        Wiq_output = self.Wiq(Wis_output)
        Wiq_output = Permute((2, 1))(Wiq_output)

        Ctilde = Wq_output
        I = Activation('sigmoid')(Wiq_output)  # x^{0} = 0
        C = multiply([I, Ctilde])  # c^{0} = 0
        X = ThresholdedReLU(theta=self.relu_thresh)(C)

        for l in range(self.n_layers1 - 1):
            S_output = self.S(X)
            Diff_X = subtract([X, S_output])  # temp variable
            Ctilde = add([Wq_output, Diff_X])

            Wfx_Wfy = add([self.W_fx(X), Wfq_output])
            F = Activation('sigmoid')(Wfx_Wfy)
            Wix_Wiy = add([self.W_ix(X), Wiq_output])
            I = Activation('sigmoid')(Wix_Wiy)
            Cf = multiply([F, C])
            Ci = multiply([I, Ctilde])
            C = add([Cf, Ci])
            X = ThresholdedReLU(theta=self.relu_thresh)(C)

        outputs = self.H(X)

        return outputs


class EstimatorMESCNetSepDict(Estimator):

    def __init__(self, batch_size=128, n_epochs=100, n_in=2, lr=1.e-4,
                 n_dictS=300, n_dictQ=300, n_channelsS=75, n_channelsQ=75,
                 n_layers1=8, n_layers2=3,
                 relu_thresh=0.01, exp_name=None, **kwargs):
        super(EstimatorMESCNetSepDict, self).__init__(**kwargs)
        self.batch_size = batch_size
        self.n_epochs = n_epochs
        self.n_in = n_in
        self.lr = lr
        self.exp_name = exp_name

        self.n_dictS = n_dictS
        self.n_dictQ = n_dictQ
        self.n_channelsS = n_channelsS
        self.n_channelsQ = n_channelsQ

        self.n_layers1 = n_layers1
        self.n_layers2 = n_layers2

        self.relu_thresh = relu_thresh

    @tf.function
    def train_step(self, data_batch, labels_batch):

        with tf.GradientTape() as clf_tape:

            # Estimated class probabilities
            clf_estims = self.mescnet_model(data_batch)

            clf_loss = self.mescnet_model.loss(labels_batch, clf_estims)

        t_variables = self.mescnet_model.trainable_variables

        clf_grads = clf_tape.gradient(clf_loss, t_variables)

        # Updating trainable weights of classifier
        self.mescnet_model.optimizer.\
            apply_gradients(zip(clf_grads, t_variables))
        return clf_loss

    @tf.function
    def valid_step(self, data_batch):

        # Estimated class probabilities
        clf_estims = self.mescnet_model(data_batch)

        return clf_estims

    @tf.function
    def get_trainable_parameters(self):
        print(self.mescnet_model.trainable_variables)
        return self.mescnet_model.trainable_variables

    def save_model_weights(self, output_model_path):

        if not os.path.exists(output_model_path):
            os.makedirs(output_model_path)

        t_variables = self.mescnet_model.trainable_variables

        for v_idx, v in enumerate(t_variables):
            np.save(os.path.join(output_model_path, 'var_'+str(v_idx)+'.npy'), v.numpy())

    def _featurs_norm(self, db, prep, data_list):

        n_vox = 0
        for f_idx, f in enumerate(data_list):
            vox_f = os.path.join(prep.exp_out, db.name, prep.name, 'n_vox_{}'.format(f_idx))
            with open(vox_f) as f:
                n_vox += int(f.readlines()[0])
        print(n_vox)

        all_train_features = np.zeros((n_vox, prep.n_out_len, prep.n_out_ch))
        n_vox = 0

        for f_idx, f in enumerate(data_list):
            gt_path = os.path.join(prep.exp_out, db.name,
                                   'ground_truth', '{}_{}.npy'.format(prep.gt_type, f_idx))
            gt = np.load(gt_path)
            all_train_features[n_vox:n_vox+gt.shape[0], :] = gt[:, :, :]
            n_vox += gt.shape[0]

        if prep.gt_type == 'smt':
            all_train_features[:, 1, 0] *= 3 * 1.e-9

        feat_means = np.mean(all_train_features, axis=0)
        feat_scales_log = np.log10(feat_means)
        feat_scales_int = np.floor(feat_scales_log)
        feat_scales = np.power(10, feat_scales_int)

        return feat_scales

    @staticmethod
    def _load_data_n(db, prep, data_list, n_samples=25600):
        count = 0
        for f in data_list:
            n_vox_path = os.path.join(prep.exp_out, db.name, prep.name,
                                      'n_vox_' + str(f))
            with open(n_vox_path, 'r') as f_vox:
                count += n_samples

        data = np.zeros((count, prep.n_in_len, prep.n_in_ch),
                        dtype=np.float32)
        gt = np.zeros((count, prep.n_out_len, prep.n_out_ch),
                      dtype=np.float32)
        count = 0
        for f in data_list:
            data_f = np.load(os.path.join(prep.exp_out, db.name,
                                          prep.name,
                                          'data_' + str(f) + '.npy'))
            gt_f = np.load(os.path.join(prep.exp_out, db.name,
                                        'ground_truth',
                                        prep.gt_type + '_' + str(f) + '.npy'))
            select = np.random.choice(data_f.shape[0], n_samples, replace=False)
            data[count:count + n_samples, :, :] = data_f[select, :, :]
            gt[count:count + n_samples, :, :] = gt_f[select, :, :]
            count += n_samples

        if prep.gt_type == 'smt':
            gt[:, 1, 0] *= 3 * 1.e-9

        return data, gt

    def train_and_valid(self, db, prep):
        """Run training and validation of the model."""
        """
            Arguments:
                prep (Preprocessor): Preprocessor object
        """

        np.random.seed(1234)

        self.train_feat_scales = self._featurs_norm(db, prep, prep.train_list)

        self.mescnet_model = MESCNetSepDict(n_dictS=self.n_dictS, n_dictQ=self.n_dictQ,
                                            n_channelsS=self.n_channelsS, n_channelsQ=self.n_channelsQ,
                                            n_layers1=self.n_layers1, n_layers2=self.n_layers2,
                                            relu_thresh=self.relu_thresh, lr=self.lr,
                                            n_feats=prep.n_out_len)

        output_dir = os.path.join(prep.exp_out, self.name())
        if not os.path.exists(output_dir):
            os.makedirs(output_dir)

        np.save(os.path.join(output_dir, 'train_feat_scales.npy'), self.train_feat_scales)

        print("Loading validation data...")

        output_valid_error_path = os.path.join(output_dir, 'errors_valid.txt')
        output_model_path = os.path.join(output_dir, 'model_{}')

        for e in range(self.n_epochs + 1):
            print("Epoch:", e)
            select_train = np.random.choice(len(prep.train_list),
                                            np.min([10,
                                                    len(prep.train_list)]),
                                            replace=False)

            train_data, train_gt = self._load_data_n(db, prep, select_train)
            train_gt[:, :, 0] /= self.train_feat_scales[:, 0]
            n_samples_t = train_gt.shape[0]
            train_tf = \
                tf.data.Dataset.from_tensor_slices((train_data, train_gt)).shuffle(n_samples_t).batch(self.batch_size)

            for t_idx, (t_batch, t_labs) in enumerate(train_tf):
                if t_idx == 2000:
                    break
                train_loss = self.train_step(t_batch, t_labs)

            select_valid = np.random.choice(len(prep.valid_list),
                                            np.min([10,
                                                    len(prep.valid_list)]),
                                            replace=False)
            v_list = [prep.valid_list[idx] for idx in select_valid]
            valid_data, valid_gt = self._load_data_n(db, prep, v_list)
            n_samples_v = valid_gt.shape[0]
            valid_tf = tf.data.Dataset.from_tensor_slices((valid_data, valid_gt)).batch(4 * self.batch_size)

            valid_mse = np.zeros(n_samples_v)
            valid_abs = np.zeros((n_samples_v, prep.n_out_len))
            for v_idx, (v_batch, v_labs) in enumerate(valid_tf):
                v_estims = self.valid_step(v_batch).numpy()
                v_estims[:, :, 0] *= self.train_feat_scales[:, 0]
                valid_mse[v_idx * 4 * self.batch_size:(v_idx + 1) * 4 * self.batch_size] = \
                    np.sum((v_labs - v_estims) ** 2, axis=1)[:, 0]
                valid_abs[v_idx * 4 * self.batch_size:(v_idx + 1) * 4 * self.batch_size, :] = \
                    np.abs(v_labs - v_estims)[:, :, 0]

            with open(output_valid_error_path, 'a+') as f_err:
                f_err.write('%s %s \n' % (np.ravel(np.mean(valid_abs, axis=0)), np.mean(valid_mse)))

            if e % 10 == 0:
                self.save_model_weights(output_model_path.format(e))

            if e % 100 == 0 and e:
                self.test(db, prep, e)

            if e == 60:
                self.mescnet_model.optimizer.lr.assign(self.lr/5)
                print(self.mescnet_model.optimizer.lr.read_value())

            if e == 80:
                self.mescnet_model.optimizer.lr.assign(self.lr/10)
                print(self.mescnet_model.optimizer.lr.read_value())

    def test(self, db, prep, model_no):
        """Run testing of the model."""
        """
            Arguments:
                db (Database): Database object
                prep (Preprocessor): Preprocessor object
                model_no (int): ordinal number of model
        """
        output_dir = os.path.join(prep.exp_out, self.name())

        test_output_dir = os.path.join(output_dir, 'test_' + str(model_no))
        if not os.path.exists(test_output_dir):
            os.makedirs(test_output_dir)

        for t in prep.test_list:

            test_data, test_gt = self._load_data(db, prep, [t])
            test_tf = tf.data.Dataset.from_tensor_slices((test_data, test_gt)).batch(self.batch_size)

            estims = np.zeros_like(test_gt)
            for t_idx, (t_batch, t_labs) in enumerate(test_tf):
                t_estim = self.valid_step(t_batch).numpy()
                t_estim[:, :, 0] *= self.train_feat_scales[:, 0]
                estims[t_idx * self.batch_size:(t_idx + 1) * self.batch_size, :, :] = t_estim

            estim_out_path = os.path.join(test_output_dir, db.subjects_list[t])
            if not os.path.exists(estim_out_path):
                os.makedirs(estim_out_path)
            np.save(os.path.join(estim_out_path, 'estim.npy'), estims)

    def name(self):
        """Method that returns object's name including parameters."""
        return "%s(name=%s, lr=%s)" % (type(self).__name__, self.exp_name, self.lr)
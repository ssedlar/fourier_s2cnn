"""Class for dMRI descriptor (micro-structure parameters, fODF)
    estimation using  FCN."""

import sys
import os

import numpy as np

import time

import tensorflow as tf
from .. import Estimator
from tensorflow.keras import Model
from tensorflow.keras.layers import Dense, Flatten, Dropout


class FCN(Model):

    def __init__(self, n_feats=3, dp=0.05, lr=1.e-4):
        super(FCN, self).__init__()

        self.n_feats = n_feats
        self.lr = lr
        self.dp = dp

        self.fcn1 = Dense(256, activation='relu', kernel_initializer=tf.keras.initializers.Orthogonal())
        self.dp1 = Dropout(self.dp)
        self.fcn2 = Dense(192, activation='relu', kernel_initializer=tf.keras.initializers.Orthogonal())
        self.dp2 = Dropout(self.dp)
        self.fcn3 = Dense(128, activation='relu', kernel_initializer=tf.keras.initializers.Orthogonal())
        self.dp3 = Dropout(self.dp)
        self.fcn4 = Dense(64, activation='relu', kernel_initializer=tf.keras.initializers.Orthogonal())
        self.dp4 = Dropout(self.dp)
        self.fcn5 = Dense(32, activation='relu', kernel_initializer=tf.keras.initializers.Orthogonal())
        self.dp5 = Dropout(self.dp)
        self.fcn6 = Dense(16, activation='relu', kernel_initializer=tf.keras.initializers.Orthogonal())
        self.dp6 = Dropout(self.dp)
        self.fcn7 = Dense(self.n_feats, activation='sigmoid', kernel_initializer=tf.keras.initializers.Orthogonal())

        self.loss = tf.keras.losses.MeanSquaredError()

        self.optimizer = tf.keras.optimizers.Adam(lr)

    def __call__(self, input, training=None):

        x = Flatten()(input)

        x1 = self.dp1(self.fcn1(x), training=training)
        x2 = self.dp2(self.fcn2(x1), training=training)
        x3 = self.dp3(self.fcn3(x2), training=training)
        x4 = self.dp4(self.fcn4(x3), training=training)
        x5 = self.dp5(self.fcn5(x4), training=training)
        x6 = self.dp6(self.fcn6(x5), training=training)
        x7 = self.fcn7(x6)
        x = tf.expand_dims(x7, axis=2)
        return x


class EstimatorFCNPlus(Estimator):

    def __init__(self, batch_size=128, n_epochs=200, lr=1.e-4, dp=0.05, n_feats=2, exp_name=None, **kwargs):
        super(EstimatorFCNPlus, self).__init__(**kwargs)
        self.batch_size = batch_size
        self.n_epochs = n_epochs
        self.n_feats = n_feats
        self.lr = lr
        self.dp = dp
        self.exp_name = exp_name


    @tf.function
    def train_step(self, data_batch, labels_batch):

        with tf.GradientTape() as clf_tape:

            # Estimated class probabilities
            clf_estims = self.fcn_model(data_batch, training=True)

            clf_loss = self.fcn_model.loss(labels_batch, clf_estims)

        t_variables = self.fcn_model.trainable_variables

        clf_grads = clf_tape.gradient(clf_loss, t_variables)

        # Updating trainable weights of classifier
        self.fcn_model.optimizer.\
            apply_gradients(zip(clf_grads, t_variables))
        return clf_loss

    @tf.function
    def valid_step(self, data_batch):

        # Estimated class probabilities
        clf_estims = self.fcn_model(data_batch, training=False)

        return clf_estims

    @tf.function
    def get_trainable_parameters(self):
        print(self.fcn_model.trainable_variables)
        return self.fcn_model.trainable_variables

    def save_model_weights(self, output_model_path):

        if not os.path.exists(output_model_path):
            os.makedirs(output_model_path)

        t_variables = self.fcn_model.trainable_variables
        for v_idx, v in enumerate(t_variables):
            np.save(os.path.join(output_model_path, 'var_'+str(v_idx)+'.npy'), v)

    @staticmethod
    def _load_data_n(db, prep, data_list, n_samples=12800):
        count = 0
        for f in data_list:
            n_vox_path = os.path.join(prep.exp_out, db.name, prep.name,
                                      'n_vox_' + str(f))
            with open(n_vox_path, 'r') as f_vox:
                count += n_samples

        data = np.zeros((count, prep.n_in_len, prep.n_in_ch),
                        dtype=np.float32)
        gt = np.zeros((count, prep.n_out_len, prep.n_out_ch),
                      dtype=np.float32)
        count = 0
        for f in data_list:
            data_f = np.load(os.path.join(prep.exp_out, db.name,
                                          prep.name,
                                          'data_' + str(f) + '.npy'))
            gt_f = np.load(os.path.join(prep.exp_out, db.name,
                                        'ground_truth',
                                        prep.gt_type + '_' + str(f) + '.npy'))
            select = np.random.choice(data_f.shape[0], n_samples, replace=False)
            data[count:count + n_samples, :, :] = data_f[select, :, :]
            gt[count:count + n_samples, :, :] = gt_f[select, :, :]
            count += n_samples

        return data, gt

    def train_and_valid(self, db, prep):
        """Run training and validation of the model."""
        """
            Arguments:
                prep (Preprocessor): Preprocessor object
        """

        np.random.seed(1234)

        self.fcn_model = FCN(n_feats=prep.n_out_len, lr=self.lr, dp=self.dp)

        output_dir = os.path.join(prep.exp_out, self.name())
        if not os.path.exists(output_dir):
            os.makedirs(output_dir)

        print("Loading validation data...")

        output_valid_error_path = os.path.join(output_dir, 'errors_valid.txt')
        output_model_path = os.path.join(output_dir, 'model_{}')

        for e in range(self.n_epochs + 1):
            print("Epoch:", e)
            select_train = np.random.choice(len(prep.train_list),
                                            np.min([10,
                                                    len(prep.train_list)]),
                                            replace=False)

            train_data, train_gt = self._load_data_n(db, prep, select_train)
            n_samples_t = train_gt.shape[0]
            train_tf = \
                tf.data.Dataset.from_tensor_slices((train_data, train_gt)).shuffle(n_samples_t).batch(self.batch_size)

            for t_idx, (t_batch, t_labs) in enumerate(train_tf):
                if t_idx == 1000:
                    break
                train_loss = self.train_step(t_batch, t_labs)

            select_valid = np.random.choice(len(prep.valid_list),
                                            np.min([10,
                                                    len(prep.valid_list)]),
                                            replace=False)
            v_list = [prep.valid_list[idx] for idx in select_valid]
            valid_data, valid_gt = self._load_data_n(db, prep, v_list)
            n_samples_v = valid_gt.shape[0]
            valid_tf = tf.data.Dataset.from_tensor_slices((valid_data, valid_gt)).shuffle(n_samples_v).batch(self.batch_size)

            valid_mse = np.zeros(self.batch_size * 1000)
            valid_abs = np.zeros((self.batch_size * 1000, prep.n_out_len))
            for v_idx, (v_batch, v_labs) in enumerate(valid_tf):
                if v_idx == 1000:
                    break
                v_estims = self.valid_step(v_batch)
                valid_mse[v_idx * self.batch_size:(v_idx + 1) * self.batch_size] = \
                    np.sum((v_labs - v_estims) ** 2, axis=1)[:, 0]
                valid_abs[v_idx * self.batch_size:(v_idx + 1) * self.batch_size, :] = \
                    np.abs(v_labs - v_estims)[:, :, 0]

            with open(output_valid_error_path, 'a+') as f_err:
                f_err.write('%s %s \n' % (np.ravel(np.mean(valid_abs, axis=0)), np.mean(valid_mse)))

            if e % 50 == 0 and e:
                self.test(db, prep, e)
                self.save_model_weights(output_model_path.format(e))

            if e == 50:
                self.fcn_model.optimizer.lr.assign(self.lr/5)
                print(self.fcn_model.optimizer.lr.read_value())

            if e == 100:
                self.fcn_model.optimizer.lr.assign(self.lr/10)
                print(self.fcn_model.optimizer.lr.read_value())

    def test(self, db, prep, model_no):
        """Run testing of the model."""
        """
            Arguments:
                db (Database): Database object
                prep (Preprocessor): Preprocessor object
                model_no (int): ordinal number of model
        """
        output_dir = os.path.join(prep.exp_out, self.name())

        test_output_dir = os.path.join(output_dir, 'test_' + str(model_no))
        if not os.path.exists(test_output_dir):
            os.makedirs(test_output_dir)

        for t in prep.test_list:

            test_data, test_gt = self._load_data(db, prep, [t])
            test_tf = tf.data.Dataset.from_tensor_slices((test_data, test_gt)).batch(self.batch_size)

            estims = np.zeros_like(test_gt)
            for t_idx, (t_batch, t_labs) in enumerate(test_tf):
                t_estim = self.valid_step(t_batch)
                estims[t_idx * self.batch_size:(t_idx + 1) * self.batch_size, :, :] = t_estim

            estim_out_path = os.path.join(test_output_dir, db.subjects_list[t])
            if not os.path.exists(estim_out_path):
                os.makedirs(estim_out_path)
            np.save(os.path.join(estim_out_path, 'estim.npy'), estims)

    def name(self):
        """Method that returns object's name including parameters."""
        return "%s(name=%s, lr=%s, dp=%s)" % (type(self).__name__, self.exp_name, self.lr, self.dp)

"""Class for dMRI descriptor (micro-structure parameters, fODF)
    estimation using MEDN."""

"""
Method from paper:
Chuyang Ye, \"Tissue microstructure estimation using a deep network
inspired by a dictionary-based learning \"
MEDN - Microstructure Estimation using a Deep Network

Code taken and adjusted from Github page: https://github.com/PkuClosed/MEDN

"""
import os

import numpy as np

import tensorflow as tf
from .. import Estimator

from tensorflow.keras.models import Sequential, Model
from tensorflow.keras.layers import Lambda
from tensorflow.keras.layers import ThresholdedReLU
from tensorflow.keras.layers import concatenate, Dense, Input, Flatten
from tensorflow.keras.constraints import NonNeg


def split_last(x):
    Viso = x[:, -1]
    return tf.reshape(Viso, [x.shape[0], 1])


def split_last_output_shape(input_shape):
    shape = list(input_shape)
    assert len(shape) == 2  # only valid for 2D tensors
    shape[-1] = 1
    return tuple(shape)


def split_others(x):
    Vother = x[:, :-1]
    return Vother


def split_others_output_shape(input_shape):
    shape = list(input_shape)
    assert len(shape) == 2  # only valid for 2D tensors
    shape[-1] = shape[-1] - 1

    return tuple(shape)


class MEDN(Model):

    def __init__(self, nDict=301, tau=1e-10, lr=1.e-4):
        super(MEDN, self).__init__()

        self.tau = tau
        self.W = Dense(nDict, activation='linear', use_bias=True)
        self.TNS = Sequential()

        self.W_vic = Dense(2, kernel_constraint=NonNeg(),
                           activation='linear', use_bias=True)
        self.ReLUThres = 0.01
        self.TNS.add(ThresholdedReLU(theta=self.ReLUThres,
                                     input_shape=(nDict,)))
        self.TNS.add(Dense(nDict, activation='linear', use_bias=True))

        self.loss = tf.keras.losses.MeanSquaredError()

        self.optimizer = tf.keras.optimizers.Adam(lr)

    def __call__(self, input):

        input = Flatten()(input)

        w = self.W(input)
        Z = self.TNS(w)

        nLayers = 8
        for l in range(nLayers - 1):
            Y = Z + w
            Z = self.TNS(Y)
        Y = Z + w
        T = ThresholdedReLU(theta=self.ReLUThres)(Y)

        Viso = Lambda(split_last,
                      output_shape=split_last_output_shape)(T)
        Vother = Lambda(split_others,
                        output_shape=split_others_output_shape)(T)

        normVother = Lambda(lambda x:
                            (x + self.tau) /
                            tf.reshape(tf.norm(x + self.tau, axis=1),
                                       [x.shape[0], 1]))(Vother)
        VicK = self.W_vic(normVother)
        Kappa = Lambda(split_last,
                       output_shape=split_last_output_shape)(VicK)
        Vic = Lambda(split_others,
                     output_shape=split_others_output_shape)(VicK)
        OD = Lambda(lambda x:
                    2.0 / np.pi * tf.math.atan(1.0 / (x + self.tau)))(Kappa)

        x = tf.concat([Vic, Viso, OD], axis=1)

        return tf.expand_dims(x, axis=2)


class EstimatorMEDN(Estimator):

    def __init__(self, batch_size=128, n_epochs=200, n_in=2, lr=1.e-4,
                 n_dict=301, exp_name=None, **kwargs):
        super(EstimatorMEDN, self).__init__(**kwargs)
        self.batch_size = batch_size
        self.n_dict = n_dict
        self.n_epochs = n_epochs
        self.n_in = n_in
        self.lr = lr
        self.exp_name = exp_name

    @tf.function
    def train_step(self, data_batch, labels_batch):

        with tf.GradientTape() as clf_tape:

            clf_estims = self.medn_model(data_batch)

            clf_loss = self.medn_model.loss(labels_batch, clf_estims)

        t_variables = self.medn_model.trainable_variables

        clf_grads = clf_tape.gradient(clf_loss, t_variables)

        self.medn_model.optimizer.apply_gradients(zip(clf_grads, t_variables))

        return clf_loss

    @tf.function
    def valid_step(self, data_batch):
        # Estimated class probabilities
        clf_estims = self.medn_model(data_batch)

        return clf_estims

    @tf.function
    def get_trainable_parameters(self):
        print(self.medn_model.trainable_variables)
        return self.medn_model.trainable_variables

    def save_model_weights(self, output_model_path):

        if not os.path.exists(output_model_path):
            os.makedirs(output_model_path)

        t_variables = self.medn_model.trainable_variables

        for v_idx, v in enumerate(t_variables):
            np.save(os.path.join(output_model_path, 'var_'+str(v_idx)+'.npy'), v.numpy())

    def train_and_valid(self, db, prep):
        """Run training and validation of the model."""
        """
            Arguments:
                prep (Preprocessor): Preprocessor object
        """

        self.medn_model = MEDN(lr=self.lr, nDict=self.n_dict)

        output_dir = os.path.join(prep.exp_out, self.name)
        if not os.path.exists(output_dir):
            os.makedirs(output_dir)
        output_error_path = os.path.join(output_dir, 'errors_valid.txt')
        output_np_error_path = os.path.join(output_dir, 'errors_valid.npy')
        output_model_path = os.path.join(output_dir, 'model_{}')

        valid_errors_np = np.zeros((prep.n_out_len + 1, self.n_epochs + 1))
        n_train = len(prep.train_list)
        n_select = np.min([10, n_train])

        # 4.3 Loading validation data
        val_data, val_gt = self._load_data_n(db, prep, prep.valid_list)
        n_samples_v = val_gt.shape[0]
        valid_tf = \
            tf.data.Dataset.from_tensor_slices((val_data, val_gt)).\
            batch(4 * self.batch_size)

        for e in range(self.n_epochs + 1):
            print("Epoch:", e)

            # 4.1 Loading training data
            select_train = np.random.choice(n_train, n_select, replace=False)
            train_data, train_gt = self._load_data_n(db, prep, select_train)
            n_samples_t = train_gt.shape[0]
            train_tf = \
                tf.data.Dataset.from_tensor_slices((train_data, train_gt)).\
                shuffle(n_samples_t).batch(self.batch_size)

            # 4.2 Run training steps
            for t_idx, (t_batch, t_labs) in enumerate(train_tf):
                train_loss = self.train_step(t_batch, t_labs)

            # 4.4 Run validaton steps and save errors
            valid_mse = np.zeros(n_samples_v)
            valid_abs = np.zeros((n_samples_v, prep.n_out_len))
            for v_idx, (v_batch, v_labs) in enumerate(valid_tf):
                v_estim = self.valid_step(v_batch)
                v_s = v_idx * 4 * self.batch_size
                v_e = (v_idx + 1) * 4 * self.batch_size
                valid_mse[v_s:v_e] = \
                    np.sum((v_labs - v_estim) ** 2, axis=1)[:, 0]
                valid_abs[v_s:v_e, :] = np.abs(v_labs - v_estim)[:, :, 0]

            valid_errors_np[0:prep.n_out_len, e] = np.ravel(np.mean(valid_abs, axis=0))
            valid_errors_np[-1, e] = np.mean(valid_mse)

            with open(output_error_path, 'a+') as f_err:
                f_err.write('%s %s\n' % (np.ravel(np.mean(valid_abs, axis=0)),
                                         np.mean(valid_mse)))

            # 4.5 Decreaseing learning rate
            if e == 50:
                self.medn_model.optimizer.lr.assign(self.lr / 5)
                print(self.medn_model.optimizer.lr.read_value())

            if e == 100:
                self.medn_model.optimizer.lr.assign(self.lr / 10)
                print(self.medn_model.optimizer.lr.read_value())

            # 4.6 Run test over valid and test subset and save model weights
            if e == self.n_epochs:
                self.test(db, prep, e)
                self.save_model_weights(output_model_path.format(e))
                np.save(output_np_error_path, valid_errors_np)

    def test(self, db, prep, model_no):
        """Run testing of the model on valid and test data."""
        """
            Arguments:
                db (Database): Database object
                prep (Preprocessor): Preprocessor object
                model_no (int): ordinal number of model
        """
        output_dir = os.path.join(prep.exp_out, self.name)

        # Run testing on validation data
        val_out_dir = os.path.join(output_dir, 'valid_' + str(model_no))
        if not os.path.exists(val_out_dir):
            os.makedirs(val_out_dir)

        for v in prep.valid_list:
            valid_data, valid_gt = self._load_data(db, prep, [v])
            valid_tf = \
                tf.data.Dataset.from_tensor_slices((valid_data, valid_gt)).\
                batch(4 * self.batch_size)

            estims = np.zeros_like(valid_gt)
            for v_idx, (v_batch, v_labs) in enumerate(valid_tf):
                v_estim = self.valid_step(v_batch)
                v_s = v_idx * 4 * self.batch_size
                v_e = (v_idx + 1) * 4 * self.batch_size
                estims[v_s:v_e, :, :] = v_estim

            estim_out_path = os.path.join(val_out_dir, db.subjects_list[v])
            if not os.path.exists(estim_out_path):
                os.makedirs(estim_out_path)
            np.save(os.path.join(estim_out_path, 'estim.npy'), estims)

        # Run testing on test data
        test_out_dir = os.path.join(output_dir, 'test_' + str(model_no))
        if not os.path.exists(test_out_dir):
            os.makedirs(test_out_dir)

        for t in prep.test_list:
            test_data, test_gt = self._load_data(db, prep, [t])
            test_tf = \
                tf.data.Dataset.from_tensor_slices((test_data, test_gt)).\
                batch(4 * self.batch_size)

            estims = np.zeros_like(test_gt)
            for t_idx, (t_batch, t_labs) in enumerate(test_tf):
                t_estim = self.valid_step(t_batch)
                t_s = t_idx * 4 * self.batch_size
                t_e = (t_idx + 1) * 4 * self.batch_size
                estims[t_s:t_e, :, :] = t_estim

            estim_out_path = os.path.join(test_out_dir, db.subjects_list[t])
            if not os.path.exists(estim_out_path):
                os.makedirs(estim_out_path)
            np.save(os.path.join(estim_out_path, 'estim.npy'), estims)

    @property
    def name(self):
        """Method that returns object's name including parameters."""
        return "%s(name=%s, lr=%s, n_dict=%s)" % \
            (type(self).__name__, self.exp_name, self.lr, self.n_dict)


from .fcn_estimator import EstimatorFCN
from .fcn_golkov import EstimatorFCN_Golkov
from .medn import EstimatorMEDN

from .fourier_s2 import EstimatorFourierS2CNN
from .fourier_zonal import EstimatorFourierZonal

# Neighbourhood input (3D patch)
from .fcn_plus import EstimatorFCNPlus
from .medn_plus import EstimatorMEDNPlus
from .mesc_net import EstimatorMESCNet
from .mesc_net_sep_dict import EstimatorMESCNetSepDict

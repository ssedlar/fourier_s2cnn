"""Class for dMRI descriptor (micro-structure parameters)
    estimation using  FCN."""

"""
References:

[1] Golkov, V., Dosovitskiy, A., Sperl, J. I., Menzel, M. I., 
Czisch, M., Sämann, P., ... & Cremers, D. (2016). 
Q-space deep learning: twelve-fold shorter and model-free diffusion MRI scans. 
IEEE transactions on medical imaging, 35(5), 1344-1351.

"""

import os

import numpy as np

import tensorflow as tf
from .. import Estimator
from tensorflow.keras import Model
from tensorflow.keras.layers import Dense, Flatten, Dropout


class FCN(Model):
    """Fully connected network model."""
    """
        Attributes:
            units (list): list of units for each layer except the last one,
                which corresponds to the number of output parameters
            n_feats (int): number of output features
            dp_rate (float): drop-out rate
            lr (float): learning rate
    """
    def __init__(self, units, n_feats=3, dp_rate=0.05, lr=1.e-4, **kwargs):
        super(FCN, self).__init__(**kwargs)

        self.units = units
        self.n_feats = n_feats
        self.dp_rate = dp_rate
        self.lr = lr

        o_init = tf.keras.initializers.Orthogonal()

        self.fcn, self.dp = [], []
        for u_idx, u in enumerate(self.units):
            self.fcn.append(Dense(u, activation='relu',
                                  kernel_initializer=o_init))
            self.dp.append(Dropout(self.dp_rate))
        self.fcn.append(Dense(self.n_feats, activation='sigmoid',
                              kernel_initializer=o_init))

        self.loss = tf.keras.losses.MeanSquaredError()

        self.optimizer = tf.keras.optimizers.Adam(lr)

    def __call__(self, input, training=None):

        x = [Flatten()(input)]
        for u_idx, u in enumerate(self.units):
            x.append(self.dp[u_idx](self.fcn[u_idx](x[-1]),
                                    training=training))
        x.append(self.fcn[-1](x[-1]))

        return tf.expand_dims(x[-1], axis=2)


class EstimatorFCN(Estimator):
    """Fully connected network estimator class."""
    """
        Attributes:
            batch_size (int): batch size
            n_epochs (int): number of training epochs
            lr (float): initial learning rate
            dp_rate (float): drop-out rate
            units (list): list of units for each layer except the last one,
                which corresponds to the number of output parameters
            exp_name (str): experiment name
            
        Methods:
            
            train_step: runs training step over one training batch
            valid_step: estimate parameters over one batch
            get_trainable_variables: returns list of trainable variables
            save_model_weights: saves model weights
            train_and_valid: runs entire training and validation
            test: runs testing over testing and validation data
            name: returns class name
            
    """
    def __init__(self, batch_size=128, n_epochs=200, lr=1.e-3, dp_rate=0.05,
                 units=[256, 192, 128, 64, 32, 16],
                 exp_name=None, **kwargs):
        super(EstimatorFCN, self).__init__(**kwargs)
        self.batch_size = batch_size
        self.n_epochs = n_epochs
        self.units = units
        self.lr = lr
        self.dp_rate = dp_rate
        self.exp_name = exp_name

    @tf.function
    def train_step(self, data_batch, labels_batch):
        """Run training step for one batch."""
        """
            Arguments:
                data_batch : input data batch
                labels_batch: labels batch
            Returns:
                training loss
        """
        with tf.GradientTape() as clf_tape:

            clf_estims = self.fcn_model(data_batch, training=True)

            clf_loss = self.fcn_model.loss(labels_batch, clf_estims)

        t_variables = self.fcn_model.trainable_variables

        clf_grads = clf_tape.gradient(clf_loss, t_variables)

        self.fcn_model.optimizer.apply_gradients(zip(clf_grads, t_variables))

        return clf_loss

    @tf.function
    def valid_step(self, data_batch):
        """Run validation step for one batch."""
        """
            Arguments:
                data_batch: input data batch
            Returns:
                estimated features
        """
        clf_estims = self.fcn_model(data_batch, training=False)

        return clf_estims

    @tf.function
    def get_trainable_parameters(self):
        """Get trainable parameters."""
        """
            Returns:
                list of trainable variables
        """
        return self.fcn_model.trainable_variables

    def save_model_weights(self, output_m_path):
        """Save model weights."""
        """
            Arguments:
                output_m_path (str): output model path
        """
        if not os.path.exists(output_m_path):
            os.makedirs(output_m_path)

        t_variables = self.fcn_model.trainable_variables
        for v_idx, v in enumerate(t_variables):
            var_name = 'var_' + str(v_idx) + '.npy'
            np.save(os.path.join(output_m_path, var_name), v.numpy())

    def train_and_valid(self, db, prep):
        """Run training and validation of the model."""
        """
            Arguments:
                db (database instance): database object
                prep (preprocessor instance): preprocessor object
        """
        self.fcn_model = FCN(units=self.units,
                             n_feats=prep.n_out_len,
                             lr=self.lr, dp_rate=self.dp_rate)

        output_dir = os.path.join(prep.exp_out, self.name)
        if not os.path.exists(output_dir):
            os.makedirs(output_dir)
        output_error_path = os.path.join(output_dir, 'errors_valid.txt')
        output_np_error_path = os.path.join(output_dir, 'errors_valid.npy')
        output_model_path = os.path.join(output_dir, 'model_{}')

        # 4.3 Loading validation data
        val_data, val_gt = self._load_data_n(db, prep, prep.valid_list)
        n_samples_v = val_gt.shape[0]
        valid_tf = \
            tf.data.Dataset.from_tensor_slices((val_data, val_gt)). \
                batch(4 * self.batch_size)

        # 4. Run model training, validation and testing
        valid_errors_np = np.zeros((prep.n_out_len + 1, self.n_epochs + 1))
        n_train = len(prep.train_list)
        n_select = np.min([10, n_train])
        for e in range(self.n_epochs + 1):
            print("Epoch:", e)

            # 4.1 Loading training data
            select_train = np.random.choice(n_train, n_select, replace=False)
            train_data, train_gt = self._load_data_n(db, prep, select_train)
            n_samples_t = train_gt.shape[0]
            train_tf = \
                tf.data.Dataset.from_tensor_slices((train_data, train_gt)). \
                    shuffle(n_samples_t).batch(self.batch_size)

            # 4.2 Run training steps
            for t_idx, (t_batch, t_labs) in enumerate(train_tf):
                train_loss = self.train_step(t_batch, t_labs)

            # 4.4 Run validaton steps and save errors
            valid_mse = np.zeros(n_samples_v)
            valid_abs = np.zeros((n_samples_v, prep.n_out_len))
            for v_idx, (v_batch, v_labs) in enumerate(valid_tf):
                v_estim = self.valid_step(v_batch)
                v_s = v_idx * 4 * self.batch_size
                v_e = (v_idx + 1) * 4 * self.batch_size
                valid_mse[v_s:v_e] = \
                    np.sum((v_labs - v_estim) ** 2, axis=1)[:, 0]
                valid_abs[v_s:v_e, :] = np.abs(v_labs - v_estim)[:, :, 0]

            valid_errors_np[0:prep.n_out_len, e] = \
                np.ravel(np.mean(valid_abs, axis=0))
            valid_errors_np[-1, e] = np.mean(valid_mse)

            with open(output_error_path, 'a+') as f_err:
                f_err.write('%s %s\n' % (np.ravel(np.mean(valid_abs, axis=0)),
                                         np.mean(valid_mse)))

            # 4.5 Decreaseing learning rate
            if e == 50:
                self.fcn_model.optimizer.lr.assign(self.lr / 5)
                print(self.fcn_model.optimizer.lr.read_value())

            if e == 100:
                self.fcn_model.optimizer.lr.assign(self.lr / 10)
                print(self.fcn_model.optimizer.lr.read_value())

            # 4.6 Run test over valid and test subset and save model weights
            if e == self.n_epochs:
                self.test(db, prep, e)
                self.save_model_weights(output_model_path.format(e))
                np.save(output_np_error_path, valid_errors_np)

    def test(self, db, prep, model_no):
        """Run testing of the model on valid and test data."""
        """
            Arguments:
                db (Database): Database object
                prep (Preprocessor): Preprocessor object
                model_no (int): ordinal number of model
        """
        output_dir = os.path.join(prep.exp_out, self.name)

        # Run testing on validation data
        val_out_dir = os.path.join(output_dir, 'valid_' + str(model_no))
        if not os.path.exists(val_out_dir):
            os.makedirs(val_out_dir)

        for v in prep.valid_list:
            valid_data, valid_gt = self._load_data(db, prep, [v])
            valid_tf = \
                tf.data.Dataset.from_tensor_slices((valid_data, valid_gt)).\
                batch(4 * self.batch_size)

            estims = np.zeros_like(valid_gt)
            for v_idx, (v_batch, v_labs) in enumerate(valid_tf):
                v_estim = self.valid_step(v_batch)
                v_s = v_idx * 4 * self.batch_size
                v_e = (v_idx + 1) * 4 * self.batch_size
                estims[v_s:v_e, :, :] = v_estim

            estim_out_path = os.path.join(val_out_dir, db.subjects_list[v])
            if not os.path.exists(estim_out_path):
                os.makedirs(estim_out_path)
            np.save(os.path.join(estim_out_path, 'estim.npy'), estims)

        # Run testing on test data
        test_out_dir = os.path.join(output_dir, 'test_' + str(model_no))
        if not os.path.exists(test_out_dir):
            os.makedirs(test_out_dir)

        for t in prep.test_list:
            test_data, test_gt = self._load_data(db, prep, [t])
            test_tf = \
                tf.data.Dataset.from_tensor_slices((test_data, test_gt)).\
                batch(4 * self.batch_size)

            estims = np.zeros_like(test_gt)
            for t_idx, (t_batch, t_labs) in enumerate(test_tf):
                t_estim = self.valid_step(t_batch)
                t_s = t_idx * 4 * self.batch_size
                t_e = (t_idx + 1) * 4 * self.batch_size
                estims[t_s:t_e, :, :] = t_estim

            estim_out_path = os.path.join(test_out_dir, db.subjects_list[t])
            if not os.path.exists(estim_out_path):
                os.makedirs(estim_out_path)
            np.save(os.path.join(estim_out_path, 'estim.npy'), estims)

    @property
    def name(self):
        """Method that returns object's name including parameters."""
        return "%s(name=%s, lr=%s, dp_rate=%s)" % \
            (type(self).__name__, self.exp_name, self.lr, self.dp_rate)

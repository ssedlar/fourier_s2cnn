"""Fourier domain S2CNN class."""
import os
import numpy as np
import tensorflow as tf

from tensorflow.keras import Model
from tensorflow.keras.layers import Dense, Flatten, Activation

from .. import Estimator
from fourier_s2cnn.utils.coordinate_conversions import \
    convert_s2_to_cart, convert_cart_to_s2
from fourier_s2cnn.utils.fourier_transforms import \
    gram_schmidt_sh_inv

from .fourier_s2cnn_utils import real_clebsch_gordan_all
from .fourier_s2cnn_utils import get_even_covariants
from .fourier_s2cnn_utils import conv_Zonal
from .fourier_s2cnn_utils import quadratic_non_linearity_S2


class FourierZonal(Model):
    """Fourier domain S2CNN class."""

    """
        Attributes:
            L (int): input signal bandwidth
            n_in (int): number of input shells/channels
            n_len (int): number of input points
            n_feats (int): number of features to estimate
            lr (float): initial learning rate
            Y_inv (numpy): 2D array containing spherical harmonics
            lambda_ (float): denoising weight
        Methods:
    """
    def __init__(self, L, n_in=2, n_len=60, n_feats=3, lr=1.e-4,
                 Y_inv=None, lambda_=0.1):
        """Initialize class attributes."""
        super(FourierZonal, self).__init__()

        self.L = L
        self.n_in = n_in
        self.n_len = n_len
        self.n_feats = n_feats

        self.Y_inv = tf.convert_to_tensor(Y_inv, dtype=tf.float32)
        self.Ids = tf.convert_to_tensor(np.eye(n_len), dtype=np.float32)

        self.lambda_ = lambda_
        r_init = 0.1 * np.random.randn(n_len, n_len).astype(np.float32)
        self.W_denoise_1 = tf.Variable(r_init, trainable=True)
        r_init = 0.1 * np.random.randn(n_len, n_len).astype(np.float32)
        self.W_denoise_2 = tf.Variable(r_init, trainable=True)

        self.L1_out = 4
        self.L2_out = 2
        self.L3_out = 0

        self.CG_r, self.CG_l = real_clebsch_gordan_all(L, self.L1_out)

        # First layer weights and biases
        n_zh_l = (self.L // 2 + 1)
        r_init = np.random.randn(n_in, 32, n_zh_l).astype(np.float32)
        self.W1 = tf.Variable(0.1 * r_init, trainable=True)
        z_init = np.zeros((32, 1), dtype=np.float32)
        self.b1 = tf.Variable(z_init, trainable=True)

        # Second layer weights and biases
        n_zh_l = (self.L1_out // 2 + 1)
        r_init = np.random.randn(32, 64, n_zh_l).astype(np.float32)
        self.W2 = tf.Variable(0.1 * r_init, trainable=True)
        z_init = np.zeros((64, 1), dtype=np.float32)
        self.b2 = tf.Variable(z_init, trainable=True)

        # Third layer weights and biases
        n_zh_l = (self.L2_out // 2 + 1)
        r_init = np.random.randn(64, 96, n_zh_l).astype(np.float32)
        self.W3 = tf.Variable(0.1 * r_init, trainable=True)
        z_init = np.zeros((96, 1), dtype=np.float32)
        self.b3 = tf.Variable(z_init, trainable=True)

        # Fully connected layers
        o_init = tf.keras.initializers.Orthogonal()
        self.fcn1 = Dense(128, activation='relu', kernel_initializer=o_init)
        self.fcn2 = Dense(64, activation='relu', kernel_initializer=o_init)
        self.fcn3 = Dense(32, activation='relu', kernel_initializer=o_init)
        self.fcn4 = Dense(self.n_feats, activation='sigmoid', kernel_initializer=o_init)

        # Loss function
        self.loss = tf.keras.losses.MeanSquaredError()

        # Optimizer
        self.optimizer = tf.keras.optimizers.Adam(learning_rate=lr)

    def __call__(self, input, L):
        """Call function."""
        # Denoising
        input_r = tf.concat([tf.gather(input, 0, axis=2),
                             tf.gather(input, 1, axis=2)],
                            axis=1)
        W_dn_1 = self.Ids + self.lambda_ * self.W_denoise_1
        input_dn_1 = Activation('relu')(tf.matmul(input_r, W_dn_1))
        W_dn_2 = self.Ids + self.lambda_ * self.W_denoise_2
        input_dn_2 = Activation('relu')(tf.matmul(input_dn_1, W_dn_2))

        n_p_sh = self.n_len // self.n_in
        sh_1_range = tf.range(0, n_p_sh)
        sh_2_range = tf.range(n_p_sh, 2 * n_p_sh)

        input_sh_1 = tf.gather(input_dn_2, sh_1_range, axis=1)
        input_sh_1 = tf.expand_dims(input_sh_1, axis=2)
        input_sh_2 = tf.gather(input_dn_2, sh_2_range, axis=1)
        input_sh_2 = tf.expand_dims(input_sh_2, axis=2)
        input_dn = tf.concat([input_sh_1, input_sh_2], axis=2)

        input_sph = tf.einsum('npc,clp->ncl', input_dn, self.Y_inv)

        # Extracting input rotation invariant power spectrum features
        even_cov = get_even_covariants(input_sph, L)

        feats0_0 = Flatten()(tf.pow(even_cov[0], 2))
        feats0_2 = Flatten()(tf.reduce_sum(tf.pow(even_cov[2], 2), axis=2))
        feats0_4 = Flatten()(tf.reduce_sum(tf.pow(even_cov[4], 2), axis=2))
        feats0_6 = Flatten()(tf.reduce_sum(tf.pow(even_cov[6], 2), axis=2))

        feats0 = tf.concat([feats0_0, (2 * 2 + 1) * feats0_2, (2 * 4 + 1) * feats0_4,
                            (2 * 6 + 1) * feats0_6], axis=1)

        # First convolutional layer
        sh_1 = conv_Zonal(even_cov, self.W1, self.b1, L)
        sh_n_1 = quadratic_non_linearity_S2(sh_1, L, self.L1_out,
                                           self.CG_r, self.CG_l)

        # Extracting rotation invariant power spectrum features
        feats1_0 = Flatten()(tf.pow(sh_n_1[0], 2))
        feats1_2 = Flatten()(tf.reduce_sum(tf.pow(sh_n_1[2], 2), axis=2))
        feats1_4 = Flatten()(tf.reduce_sum(tf.pow(sh_n_1[4], 2), axis=2))

        feats1 = tf.concat([feats1_0,
                           (2 * 2 + 1) * feats1_2,
                           (2 * 4 + 1) * feats1_4],
                           axis=1)

        # Second convolutional layer
        sh_2 = conv_Zonal(sh_n_1, self.W2, self.b2, self.L1_out)
        sh_n_2 = quadratic_non_linearity_S2(sh_2, self.L1_out, self.L2_out,
                                           self.CG_r, self.CG_l)

        # Extracting rotation invariant power spectrum features
        feats2_0 = Flatten()(tf.pow(sh_n_2[0], 2))
        feats2_2 = Flatten()(tf.reduce_sum(tf.pow(sh_n_2[2], 2), axis=2))

        feats2 = tf.concat([feats2_0, (2 * 2 + 1) * feats2_2], axis=1)

        # Third convolutional layer
        sh_3 = conv_Zonal(sh_n_2, self.W3, self.b3, self.L2_out)
        sh_n_3 = quadratic_non_linearity_S2(sh_3, self.L2_out, self.L3_out,
                                           self.CG_r, self.CG_l)

        # Extracting rotation invariant power spectrum features
        feats3_0 = tf.pow(sh_n_3[0], 2)
        feats3_0 = Flatten()(feats3_0)

        feats3 = tf.concat([feats3_0], axis=1)

        # Concatenating all rotation invariant power spectrum features
        feats = tf.concat([feats0, feats1, feats2, feats3], axis=1)

        # Fully connected layers
        x = self.fcn3(self.fcn2(self.fcn1(feats)))
        x = tf.expand_dims(self.fcn4(x), axis=2)

        return x


class EstimatorFourierZonal(Estimator):
    """Fourier domain S2CNN class."""

    def __init__(self, batch_size=128, n_epochs=300, lr=0.001, exp_name=None,
                 lambda_=0.1, lr_dec=[1. / np.sqrt(10), 1. / np.sqrt(10)],
                 **kwargs):
        """Initialize class attributes."""
        super(EstimatorFourierZonal, self).__init__(**kwargs)
        self.batch_size = batch_size
        self.n_epochs = n_epochs
        self.lr = lr
        self.lr_dec = lr_dec
        self.lambda_ = lambda_
        self.exp_name = exp_name

    @tf.function
    def train_step(self, data_batch, labels_batch):
        """Run training step for one batch."""
        """
            Arguments:
                data_batch : input data batch
                labels_batch: labels batch
            Returns:
                training loss
        """
        with tf.GradientTape() as clf_tape:

            # Estimated class probabilities
            clf_estims = self.cg_model(data_batch, L=6)
            clf_loss = self.cg_model.loss(labels_batch, clf_estims)

        t_variables = self.cg_model.trainable_variables

        clf_grads = clf_tape.gradient(clf_loss, t_variables)

        # Updating trainable weights of classifier
        self.cg_model.optimizer.apply_gradients(zip(clf_grads, t_variables))

        return clf_loss

    @tf.function
    def valid_step(self, data_batch):
        """Run validation step for one batch."""
        """
            Arguments:
                data_batch: input data batch
            Returns:
                estimated features
        """
        clf_estims = self.cg_model(data_batch, L=6)
        return clf_estims

    @tf.function
    def get_trainable_parameters(self):
        """Get trainable parameters."""
        """
            Returns:
                list of trainable variables
        """

        t_variables = self.cg_model.trainable_variables

        return t_variables

    def _load_coords(self, db, prep):
        """Load coordinates and compute average positions."""
        """
            Arguments:
                db (database instance): database object
                prep (preprocessor instance): preprocessor object
            Returns:
                Y_gs (numpy array): inverse spherical harmonic basis
                coords_sph (numpy array): average coordinate positions
        """
        exp_path = os.path.join(prep.exp_out, db.name, prep.name)
        coords_cart = {}
        for f_idx, f in enumerate(prep.train_list):
            coords_path = os.path.join(exp_path, 'coords_' + str(f) + '.npy')
            coords_s2_f = np.load(coords_path)
            for sh_idx in range(prep.n_shells):
                coords_cart_f = convert_s2_to_cart(coords_s2_f[:, 1:, sh_idx])
                if not f_idx:
                    coords_cart[sh_idx] = coords_cart_f
                else:
                    coords_cart[sh_idx] += coords_cart_f

        for sh_idx in range(prep.n_shells):
            coords_cart[sh_idx] /= len(prep.train_list)
            for i in range(coords_cart[sh_idx].shape[0]):
                n = np.sqrt(np.sum(coords_cart[sh_idx][i, :] ** 2))
                coords_cart[sh_idx][i, :] /= n

        coords_sph = {}
        for sh_idx in range(prep.n_shells):
            coords_sph[sh_idx] = convert_cart_to_s2(coords_cart[sh_idx])

        n_sph = np.sum([2 * l + 1 for l in range(0, prep.sh_B + 1, 2)])

        Y_gs = np.zeros((prep.n_shells, n_sph, coords_sph[sh_idx].shape[0]))
        for sh_idx in range(prep.n_shells):
            Y_gs[sh_idx, :, :] = gram_schmidt_sh_inv(coords_sph[sh_idx],
                                                     L=prep.sh_B,
                                                     b_type='real',
                                                     n_iters=1000)

        return Y_gs, coords_sph

    def save_model_weights(self, output_m_path):
        """Save model weights."""
        """
            Arguments:
                output_m_path (str): output model path
        """
        if not os.path.exists(output_m_path):
            os.makedirs(output_m_path)

        np.save(os.path.join(output_m_path, 'Y_inv.npy'), self.Y_inv)

        for sh_idx in self.coords:
            coords_name = 'coords_{}.npy'.format(sh_idx)
            coords_path = os.path.join(output_m_path, coords_name)
            np.save(coords_path, self.coords[sh_idx])

        t_variables = self.cg_model.trainable_variables

        for v_idx, v in enumerate(t_variables):
            var_name = 'var_' + str(v_idx) + '.npy'
            var_path = os.path.join(output_m_path, var_name)
            np.save(var_path, v.numpy())

    def train_and_valid(self, db, prep):
        """Run training and validation of the model."""
        """
            Arguments:
                db (database instance): database object
                prep (preprocessor instance): preprocessor object
        """
        # 1. Computing average coordinate position and
        # inverse spherical harmonic basis
        self.Y_inv, self.coords = self._load_coords(db, prep)

        # 2. Defining FourierS2CNN model
        self.cg_model = FourierZonal(L=prep.sh_B,
                                     n_in=prep.n_shells, n_len=prep.n_points,
                                     n_feats=prep.n_out_len,
                                     lr=self.lr, lambda_=self.lambda_,
                                     Y_inv=self.Y_inv)

        # 3. Defining output paths
        output_dir = os.path.join(prep.exp_out, self.name)
        if not os.path.exists(output_dir):
            os.makedirs(output_dir)
        output_error_path = os.path.join(output_dir, 'errors_valid.txt')
        output_np_error_path = os.path.join(output_dir, 'errors_valid.npy')
        output_model_path = os.path.join(output_dir, 'model_{}')

        # 4. Run model training, validation and testing
        valid_errors_np = np.zeros((prep.n_out_len + 1, self.n_epochs + 1))
        n_train = len(prep.train_list)
        n_select = np.min([10, n_train])

        np.random.seed(123)
        # 4.3 Loading validation data
        val_data, val_gt = self._load_data_n(db, prep, prep.valid_list)
        n_samples_v = val_gt.shape[0]
        valid_tf = \
            tf.data.Dataset.from_tensor_slices((val_data, val_gt)).\
            batch(4 * self.batch_size)
        for e in range(self.n_epochs + 1):
            print("Epoch:", e)

            # 4.1 Loading training data
            select_train = np.random.choice(n_train, n_select, replace=False)
            train_data, train_gt = self._load_data_n(db, prep, select_train)
            n_samples_t = train_gt.shape[0]
            train_tf = \
                tf.data.Dataset.from_tensor_slices((train_data, train_gt)).\
                shuffle(n_samples_t).batch(self.batch_size)

            # 4.2 Run training steps
            for t_idx, (t_batch, t_labs) in enumerate(train_tf):
                train_loss = self.train_step(t_batch, t_labs)

            # 4.4 Run validaton steps and save errors
            valid_mse = np.zeros(n_samples_v)
            valid_abs = np.zeros((n_samples_v, prep.n_out_len))
            for v_idx, (v_batch, v_labs) in enumerate(valid_tf):
                v_estim = self.valid_step(v_batch)
                v_s = v_idx * 4 * self.batch_size
                v_e = (v_idx + 1) * 4 * self.batch_size
                valid_mse[v_s:v_e] = \
                    np.sum((v_labs - v_estim) ** 2, axis=1)[:, 0]
                valid_abs[v_s:v_e, :] = np.abs(v_labs - v_estim)[:, :, 0]

            valid_errors_np[0:prep.n_out_len, e] = np.ravel(np.mean(valid_abs, axis=0))
            valid_errors_np[-1, e] = np.mean(valid_mse)

            with open(output_error_path, 'a+') as f_err:
                f_err.write('%s %s\n' % (np.ravel(np.mean(valid_abs, axis=0)),
                                         np.mean(valid_mse)))

            # 4.5 Decreaseing learning rate
            if e == 200:
                lr_c = self.cg_model.optimizer.lr.read_value()
                self.cg_model.optimizer.lr.assign(lr_c * self.lr_dec[0])
                print(self.cg_model.optimizer.lr.read_value())

            if e == 250:
                lr_c = self.cg_model.optimizer.lr.read_value()
                self.cg_model.optimizer.lr.assign(lr_c * self.lr_dec[1])
                print(self.cg_model.optimizer.lr.read_value())

            # 4.6 Run test over valid and test subset and save model weights
            if e == self.n_epochs:
                self.test(db, prep, e)
                self.save_model_weights(output_model_path.format(e))
                np.save(output_np_error_path, valid_errors_np)

    def test(self, db, prep, model_no):
        """Run testing of the model on valid and test data."""
        """
            Arguments:
                db (Database): Database object
                prep (Preprocessor): Preprocessor object
                model_no (int): ordinal number of model
        """
        output_dir = os.path.join(prep.exp_out, self.name)

        # Run testing on validation data
        val_out_dir = os.path.join(output_dir, 'valid_' + str(model_no))
        if not os.path.exists(val_out_dir):
            os.makedirs(val_out_dir)

        for v in prep.valid_list:
            valid_data, valid_gt = self._load_data(db, prep, [v])
            valid_tf = \
                tf.data.Dataset.from_tensor_slices((valid_data, valid_gt)).\
                batch(4 * self.batch_size)

            estims = np.zeros_like(valid_gt)
            for v_idx, (v_batch, v_labs) in enumerate(valid_tf):
                v_estim = self.valid_step(v_batch)
                v_s = v_idx * 4 * self.batch_size
                v_e = (v_idx + 1) * 4 * self.batch_size
                estims[v_s:v_e, :, :] = v_estim

            estim_out_path = os.path.join(val_out_dir, db.subjects_list[v])
            if not os.path.exists(estim_out_path):
                os.makedirs(estim_out_path)
            np.save(os.path.join(estim_out_path, 'estim.npy'), estims)

        # Run testing on test data
        test_out_dir = os.path.join(output_dir, 'test_' + str(model_no))
        if not os.path.exists(test_out_dir):
            os.makedirs(test_out_dir)

        for t in prep.test_list:
            test_data, test_gt = self._load_data(db, prep, [t])
            test_tf = \
                tf.data.Dataset.from_tensor_slices((test_data, test_gt)).\
                batch(4 * self.batch_size)

            estims = np.zeros_like(test_gt)
            for t_idx, (t_batch, t_labs) in enumerate(test_tf):
                t_estim = self.valid_step(t_batch)
                t_s = t_idx * 4 * self.batch_size
                t_e = (t_idx + 1) * 4 * self.batch_size
                estims[t_s:t_e, :, :] = t_estim

            estim_out_path = os.path.join(test_out_dir, db.subjects_list[t])
            if not os.path.exists(estim_out_path):
                os.makedirs(estim_out_path)
            np.save(os.path.join(estim_out_path, 'estim.npy'), estims)

    @property
    def name(self):
        """Return object's name including parameters."""
        return "%s(name=%s, lambda=%s, lr=%s, lr_dec=%s)" % \
               (type(self).__name__, self.exp_name, self.lambda_,
                self.lr, self.lr_dec)

"""Fourier domain S2CNN class."""
import os
import numpy as np
import tensorflow as tf

from tensorflow.keras import Model
from tensorflow.keras.layers import Dense, Flatten, Activation

from .. import Estimator
from fourier_s2cnn.utils.coordinate_conversions import \
    convert_s2_to_cart, convert_cart_to_s2
from fourier_s2cnn.utils.fourier_transforms import \
    gram_schmidt_sh_inv

from .fourier_s2cnn_utils import real_clebsch_gordan_all
from .fourier_s2cnn_utils import get_even_covariants
from .fourier_s2cnn_utils import conv_SH, conv_RH
from .fourier_s2cnn_utils import quadratic_non_linearity


class FourierS2CNN(Model):
    """Fourier domain S2CNN class."""

    """
        Attributes:
            layers_L (list): list of S2 and SO(3) bandwidths per layer
            ap_symm (bool): flag inidicating whether SO(3) feature maps 
                are with or without antipodal symmetry
            n_in (int): number of input shells/channels
            n_len (int): number of input points
            n_feats (int): number of features to estimate
            Y_inv (numpy): 2D array containing spherical harmonics
            lambda_ (float): denoising weight
            dn_N (int): number of denoising layers
            lr (float): initial learning rate
    """
    def __init__(self, layers_L=[6, 4, 2, 0], ap_symm=False, n_in=2, n_len=60, n_feats=3,
                 Y_inv=None, lambda_=0.1, dn_N=2, lr=1.e-4, **kwargs):
        """Initialize class attributes."""
        super(FourierS2CNN, self).__init__(**kwargs)

        self.layers_L = layers_L
        self.ap_symm = ap_symm
        self.step_h = 2 if self.ap_symm else 1

        self.n_in = n_in
        self.n_len = n_len
        self.n_feats = n_feats

        self.lambda_ = lambda_
        self.dn_N = dn_N

        self.lr = lr

        self.Y_inv = tf.convert_to_tensor(Y_inv, dtype=tf.float32)
        self.Ids = tf.convert_to_tensor(np.eye(n_len), dtype=np.float32)

        self.W_dn = []
        for n in range(self.dn_N):
            r_init = 0.1 * np.random.randn(n_len, n_len).astype(np.float32)
            self.W_dn.append(tf.Variable(r_init, trainable=True))

        self.CG_r, self.CG_l = \
            real_clebsch_gordan_all(self.layers_L[0], self.layers_L[1])

        # First layer weights and biases
        self.W1 = {}
        for l in range(0, self.layers_L[0] + 1, 2):
            n_sh_l = 2 * l + 1
            r_init = np.random.randn(n_in, 8, n_sh_l).astype(np.float32)
            self.W1[l] = tf.Variable(0.1 * r_init, trainable=True)
        z_init = np.zeros((8, 1, 1), dtype=np.float32)
        self.b1 = tf.Variable(z_init, trainable=True)

        # Second layer weights and biases
        self.W2 = {}
        for l in range(0, self.layers_L[1] + 1, self.step_h):
            n_rh_l = 2 * l + 1
            r_init = np.random.randn(8, 16, n_rh_l, n_rh_l).astype(np.float32)
            self.W2[l] = tf.Variable(0.1 * r_init, trainable=True)
        z_init = np.zeros((16, 1, 1), dtype=np.float32)
        self.b2 = tf.Variable(z_init, trainable=True)

        # Third layer weights and biases
        self.W3 = {}
        for l in range(0, self.layers_L[2] + 1, self.step_h):
            n_rh_l = 2 * l + 1
            r_init = np.random.randn(16, 32, n_rh_l, n_rh_l).astype(np.float32)
            self.W3[l] = tf.Variable(0.1 * r_init, trainable=True)
        z_init = np.zeros((32, 1, 1), dtype=np.float32)
        self.b3 = tf.Variable(z_init, trainable=True)

        # Fully connected layers
        o_init = tf.keras.initializers.Orthogonal()
        self.fcn1 = Dense(128, activation='relu', kernel_initializer=o_init)
        self.fcn2 = Dense(64, activation='relu', kernel_initializer=o_init)
        self.fcn3 = Dense(32, activation='relu', kernel_initializer=o_init)
        self.fcn4 = Dense(3, activation='sigmoid', kernel_initializer=o_init)

        # Loss function
        self.loss = tf.keras.losses.MeanSquaredError()

        # Optimizer
        self.optimizer = tf.keras.optimizers.Adam(learning_rate=self.lr)

    def __call__(self, input, L):
        """Call function."""
        # Denoising
        input_r = tf.concat([tf.gather(input, 0, axis=2),
                             tf.gather(input, 1, axis=2)],
                            axis=1)

        for n in range(self.dn_N):
            input_r = \
                Activation('relu')(tf.matmul(input_r,
                                             self.Ids +
                                             self.lambda_ * self.W_dn[n]))

        n_p_sh = self.n_len // self.n_in
        sh_1_range = tf.range(0, n_p_sh)
        sh_2_range = tf.range(n_p_sh, 2 * n_p_sh)

        input_sh_1 = tf.gather(input_r, sh_1_range, axis=1)
        input_sh_1 = tf.expand_dims(input_sh_1, axis=2)
        input_sh_2 = tf.gather(input_r, sh_2_range, axis=1)
        input_sh_2 = tf.expand_dims(input_sh_2, axis=2)
        input_dn = tf.concat([input_sh_1, input_sh_2], axis=2)

        input_sph = tf.einsum('npc,clp->ncl', input_dn, self.Y_inv)

        # Extracting input rotation invariant power spectrum features
        even_cov = get_even_covariants(input_sph, L)

        for l in range(0, self.layers_L[0] + 1, 2):
            feats0_l = Flatten()(tf.reduce_sum(tf.pow(even_cov[l], 2),
                                               axis=2))
            if not l:
                feats0 = feats0_l
            else:
                feats0 = tf.concat([feats0, feats0_l], axis=1)

        # First convolutional layer
        rh_1 = conv_SH(even_cov, self.W1, self.b1, L)
        rh_n_1 = quadratic_non_linearity(rh_1,
                                         self.layers_L[0], self.layers_L[1],
                                         self.CG_r, self.CG_l,
                                         symm=self.ap_symm)
        # Extracting rotation invariant power spectrum features
        for l in range(0, self.layers_L[1] + 1, self.step_h):
            n_l = 8 * (np.pi ** 2) / (2 * l + 1)
            feats1_l = tf.reduce_sum(tf.pow(rh_n_1[l], 2), axis=[2, 3])
            if not l:
                feats1 = n_l * Flatten()(feats1_l)
            else:
                feats1 = tf.concat([feats1, n_l * feats1_l], axis=1)

        # Second convolutional layer
        rh_2 = conv_RH(rh_n_1, self.W2, self.b2, self.layers_L[1],
                       symm=self.ap_symm)
        rh_n_2 = quadratic_non_linearity(rh_2,
                                         self.layers_L[1], self.layers_L[2],
                                         self.CG_r, self.CG_l,
                                         symm=self.ap_symm)
        # Extracting rotation invariant power spectrum features
        for l in range(0, self.layers_L[2] + 1, self.step_h):
            n_l = 8 * (np.pi ** 2) / (2 * l + 1)
            feats2_l = tf.reduce_sum(tf.pow(rh_n_2[l], 2), axis=[2, 3])
            if not l:
                feats2 = n_l * Flatten()(feats2_l)
            else:
                feats2 = tf.concat([feats2, n_l * feats2_l], axis=1)

        # Third convolutional layer
        rh_3 = conv_RH(rh_n_2, self.W3, self.b3, self.layers_L[2],
                       symm=self.ap_symm)
        rh_n_3 = quadratic_non_linearity(rh_3,
                                         self.layers_L[2], self.layers_L[3],
                                         self.CG_r, self.CG_l,
                                         symm=self.ap_symm)
        # Extracting rotation invariant power spectrum features
        for l in range(0, self.layers_L[3] + 1, self.step_h):
            n_l = 8 * (np.pi ** 2) / (2 * l + 1)
            feats3_l = tf.reduce_sum(tf.pow(rh_n_3[l], 2), axis=[2, 3])
            if not l:
                feats3 = n_l * Flatten()(feats3_l)
            else:
                feats3 = tf.concat([feats3, n_l * feats3_l], axis=1)

        # Concatenating all rotation invariant power spectrum features
        feats = tf.concat([feats0, feats1, feats2, feats3], axis=1)

        # Fully connected layers
        x = self.fcn3(self.fcn2(self.fcn1(feats)))
        x = tf.expand_dims(self.fcn4(x), axis=2)

        return x


class EstimatorFourierS2CNN(Estimator):
    """Fourier domain S2CNN class."""

    """
        Attributes:
            batch_size (int): batch size
            n_epochs (int): number of training epochs
            lr (float): initial learning rate
            
            lambda_ (float): denoising weights
            dn_N (int): number of denoising layers
            
            layers_L (list): list of S2 and SO(3) bandwidths per layer

            exp_name (str): experiment name
    """
    def __init__(self, batch_size=128, n_epochs=200, lr=0.001, lambda_=0.1,
                 dn_N=2, layers_L=[6, 4, 2, 0], ap_symm=False, exp_name=None,
                 **kwargs):
        """Initialize class attributes."""
        super(EstimatorFourierS2CNN, self).__init__(**kwargs)

        self.batch_size = batch_size
        self.n_epochs = n_epochs
        self.lr = lr

        self.lambda_ = lambda_
        self.dn_N = dn_N

        self.layers_L = layers_L
        self.ap_symm = ap_symm

        self.exp_name = exp_name

    @tf.function
    def train_step(self, data_batch, labels_batch):
        """Run training step for one batch."""
        """
            Arguments:
                data_batch : input data batch
                labels_batch: labels batch
            Returns:
                training loss
        """
        with tf.GradientTape() as clf_tape:

            # Estimated class probabilities
            clf_estims = self.cg_model(data_batch, L=self.layers_L[0])
            clf_loss = self.cg_model.loss(labels_batch, clf_estims)

        Wdn = [self.cg_model.W_dn[n] for n in range(self.dn_N)]
        W1 = [self.cg_model.W1[l] for l in self.cg_model.W1]
        W2 = [self.cg_model.W2[l] for l in self.cg_model.W2]
        W3 = [self.cg_model.W3[l] for l in self.cg_model.W3]

        t_variables = self.cg_model.trainable_variables + Wdn + W1 + W2 + W3

        clf_grads = clf_tape.gradient(clf_loss, t_variables)

        # Updating trainable weights of classifier
        self.cg_model.optimizer.apply_gradients(zip(clf_grads, t_variables))

        return clf_loss

    @tf.function
    def valid_step(self, data_batch):
        """Run validation step for one batch."""
        """
            Arguments:
                data_batch: input data batch
            Returns:
                estimated features
        """
        clf_estims = self.cg_model(data_batch, L=self.layers_L[0])
        return clf_estims

    @tf.function
    def get_trainable_parameters(self):
        """Get trainable parameters."""
        """
            Returns:
                list of trainable variables
        """
        Wdn = [self.cg_model.W_dn[n] for n in range(self.dn_N)]
        W1 = [self.cg_model.W1[l] for l in self.cg_model.W1]
        W2 = [self.cg_model.W2[l] for l in self.cg_model.W2]
        W3 = [self.cg_model.W3[l] for l in self.cg_model.W3]

        t_variables = self.cg_model.trainable_variables + Wdn + W1 + W2 + W3

        return t_variables

    def _load_coords(self, db, prep):
        """Load coordinates and compute average positions."""
        """
            Arguments:
                db (database instance): database object
                prep (preprocessor instance): preprocessor object
            Returns:
                Y_gs (numpy array): inverse spherical harmonic basis
                coords_sph (numpy array): average coordinate positions
        """
        exp_path = os.path.join(prep.exp_out, db.name, prep.name)
        coords_cart = {}
        for f_idx, f in enumerate(prep.train_list):
            coords_path = os.path.join(exp_path, 'coords_' + str(f) + '.npy')
            coords_s2_f = np.load(coords_path)
            for sh_idx in range(prep.n_shells):
                coords_cart_f = convert_s2_to_cart(coords_s2_f[:, 1:, sh_idx])
                if not f_idx:
                    coords_cart[sh_idx] = coords_cart_f
                else:
                    coords_cart[sh_idx] += coords_cart_f

        for sh_idx in range(prep.n_shells):
            coords_cart[sh_idx] /= len(prep.train_list)
            for i in range(coords_cart[sh_idx].shape[0]):
                n = np.sqrt(np.sum(coords_cart[sh_idx][i, :] ** 2))
                coords_cart[sh_idx][i, :] /= n

        coords_sph = {}
        for sh_idx in range(prep.n_shells):
            coords_sph[sh_idx] = convert_cart_to_s2(coords_cart[sh_idx])

        n_sph = np.sum([2 * l + 1 for l in range(0, self.layers_L[0] + 1, 2)])

        Y_gs = np.zeros((prep.n_shells, n_sph, coords_sph[sh_idx].shape[0]))
        for sh_idx in range(prep.n_shells):
            Y_gs[sh_idx, :, :] = gram_schmidt_sh_inv(coords_sph[sh_idx],
                                                     L=self.layers_L[0],
                                                     b_type='real',
                                                     n_iters=1000)

        return Y_gs, coords_sph

    def save_model_weights(self, output_m_path):
        """Save model weights."""
        """
            Arguments:
                output_m_path (str): output model path
        """
        if not os.path.exists(output_m_path):
            os.makedirs(output_m_path)

        np.save(os.path.join(output_m_path, 'Y_inv.npy'), self.Y_inv)

        for sh_idx in self.coords:
            coords_name = 'coords_{}.npy'.format(sh_idx)
            coords_path = os.path.join(output_m_path, coords_name)
            np.save(coords_path, self.coords[sh_idx])

        Wdn = [self.cg_model.W_dn[n] for n in range(self.dn_N)]
        W1 = [self.cg_model.W1[l] for l in self.cg_model.W1]
        W2 = [self.cg_model.W2[l] for l in self.cg_model.W2]
        W3 = [self.cg_model.W3[l] for l in self.cg_model.W3]

        t_variables = self.cg_model.trainable_variables + Wdn + W1 + W2 + W3

        for v_idx, v in enumerate(t_variables):
            var_name = 'var_' + str(v_idx) + '.npy'
            var_path = os.path.join(output_m_path, var_name)
            np.save(var_path, v.numpy())

    def train_and_valid(self, db, prep):
        """Run training and validation of the model."""
        """
            Arguments:
                db (database instance): database object
                prep (preprocessor instance): preprocessor object
        """
        # 1. Computing average coordinate position and
        # inverse spherical harmonic basis

        self.Y_inv, self.coords = self._load_coords(db, prep)

        # 2. Defining FourierS2CNN model
        self.cg_model = FourierS2CNN(layers_L=self.layers_L,
                                     ap_symm=self.ap_symm,
                                     n_in=prep.n_shells, n_len=prep.n_points,
                                     n_feats=prep.n_out_len,
                                     Y_inv=self.Y_inv,
                                     lambda_=self.lambda_,
                                     dn_N=self.dn_N,
                                     lr=self.lr)
        # 3. Defining output paths
        output_dir = os.path.join(prep.exp_out, self.name)
        if not os.path.exists(output_dir):
            os.makedirs(output_dir)
        output_error_path = os.path.join(output_dir, 'errors_valid.txt')
        output_np_error_path = os.path.join(output_dir, 'errors_valid.npy')
        output_model_path = os.path.join(output_dir, 'model_{}')

        # 4.3 Loading validation data
        val_data, val_gt = self._load_data_n(db, prep, prep.valid_list)
        n_samples_v = val_gt.shape[0]
        valid_tf = \
            tf.data.Dataset.from_tensor_slices((val_data, val_gt)). \
                batch(4 * self.batch_size)
        # 4. Run model training, validation and testing
        valid_errors_np = np.zeros((4, self.n_epochs + 1))
        n_train = len(prep.train_list)
        n_select = np.min([10, n_train])
        for e in range(self.n_epochs + 1):
            print("Epoch:", e)

            # 4.1 Loading training data
            select_train = np.random.choice(n_train, n_select, replace=False)
            train_data, train_gt = self._load_data_n(db, prep, select_train)
            n_samples_t = train_gt.shape[0]
            train_tf = \
                tf.data.Dataset.from_tensor_slices((train_data, train_gt)).\
                shuffle(n_samples_t).batch(self.batch_size)

            # 4.2 Run training steps
            for t_idx, (t_batch, t_labs) in enumerate(train_tf):
                train_loss = self.train_step(t_batch, t_labs)

            # 4.4 Run validaton steps and save errors
            valid_mse = np.zeros(n_samples_v)
            valid_abs = np.zeros((n_samples_v, prep.n_out_len))
            for v_idx, (v_batch, v_labs) in enumerate(valid_tf):
                v_estim = self.valid_step(v_batch)
                v_s = v_idx * 4 * self.batch_size
                v_e = (v_idx + 1) * 4 * self.batch_size
                valid_mse[v_s:v_e] = \
                    np.sum((v_labs - v_estim) ** 2, axis=1)[:, 0]
                valid_abs[v_s:v_e, :] = np.abs(v_labs - v_estim)[:, :, 0]

            valid_errors_np[0:3, e] = np.ravel(np.mean(valid_abs, axis=0))
            valid_errors_np[3, e] = np.mean(valid_mse)

            with open(output_error_path, 'a+') as f_err:
                f_err.write('%s %s\n' % (np.ravel(np.mean(valid_abs, axis=0)),
                                         np.mean(valid_mse)))

            # 4.5 Decreaseing learning rate
            if e == 50:
                self.cg_model.optimizer.lr.assign(self.lr / 5)
                print(self.cg_model.optimizer.lr.read_value())

            if e == 100:
                self.cg_model.optimizer.lr.assign(self.lr / 10)
                print(self.cg_model.optimizer.lr.read_value())

            # 4.6 Run test over valid and test subset and save model weights
            if e == self.n_epochs:
                self.test(db, prep, e)
                self.save_model_weights(output_model_path.format(e))
                np.save(output_np_error_path, valid_errors_np)

    def test(self, db, prep, model_no):
        """Run testing of the model on valid and test data."""
        """
            Arguments:
                db (Database): Database object
                prep (Preprocessor): Preprocessor object
                model_no (int): ordinal number of model
        """
        output_dir = os.path.join(prep.exp_out, self.name)

        # Run testing on validation data
        val_out_dir = os.path.join(output_dir, 'valid_' + str(model_no))
        if not os.path.exists(val_out_dir):
            os.makedirs(val_out_dir)

        for v in prep.valid_list:
            valid_data, valid_gt = self._load_data(db, prep, [v])
            valid_tf = \
                tf.data.Dataset.from_tensor_slices((valid_data, valid_gt)).\
                batch(4 * self.batch_size)

            estims = np.zeros_like(valid_gt)
            for v_idx, (v_batch, v_labs) in enumerate(valid_tf):
                v_estim = self.valid_step(v_batch)
                v_s = v_idx * 4 * self.batch_size
                v_e = (v_idx + 1) * 4 * self.batch_size
                estims[v_s:v_e, :, :] = v_estim

            estim_out_path = os.path.join(val_out_dir, db.subjects_list[v])
            if not os.path.exists(estim_out_path):
                os.makedirs(estim_out_path)
            np.save(os.path.join(estim_out_path, 'estim.npy'), estims)

        # Run testing on test data
        test_out_dir = os.path.join(output_dir, 'test_' + str(model_no))
        if not os.path.exists(test_out_dir):
            os.makedirs(test_out_dir)

        for t in prep.test_list:
            test_data, test_gt = self._load_data(db, prep, [t])
            test_tf = \
                tf.data.Dataset.from_tensor_slices((test_data, test_gt)).\
                batch(4 * self.batch_size)

            estims = np.zeros_like(test_gt)
            for t_idx, (t_batch, t_labs) in enumerate(test_tf):
                t_estim = self.valid_step(t_batch)
                t_s = t_idx * 4 * self.batch_size
                t_e = (t_idx + 1) * 4 * self.batch_size
                estims[t_s:t_e, :, :] = t_estim

            estim_out_path = os.path.join(test_out_dir, db.subjects_list[t])
            if not os.path.exists(estim_out_path):
                os.makedirs(estim_out_path)
            np.save(os.path.join(estim_out_path, 'estim.npy'), estims)

    @property
    def name(self):
        """Return object's name including parameters."""
        return "%s(name=%s, lambda=%s, dn_N=%s, " \
               "layers_L=%s, ap_symm=%s, lr=%s)" % \
               (type(self).__name__, self.exp_name, self.lambda_,
                self.dn_N, self.layers_L, self.ap_symm, self.lr)


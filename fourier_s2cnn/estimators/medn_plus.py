"""Class for dMRI descriptor (micro-structure parameters, fODF)
    estimation using MEDN."""

"""
Implementation of the article:
Chuyang Ye, \"Tissue microstructure estimation using a deep network 
inspired by a dictionary-based learning \"
MEDN - Microstructure Estimation using a Deep Network
Github page: https://github.com/PkuClosed/MEDN 

"""
import sys
import os

import numpy as np
import numpy.matlib

import time

import tensorflow as tf
from .. import Estimator

from tensorflow.keras.models import Sequential, Model
from tensorflow.keras.layers import Lambda
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.layers import ThresholdedReLU
from tensorflow.keras.layers import concatenate, Dense, Input, Flatten
from tensorflow.keras.constraints import NonNeg


def split_last(x):
    Viso = x[:, -1]
    return tf.reshape(Viso, [x.shape[0], 1])


def split_last_output_shape(input_shape):
    shape = list(input_shape)
    assert len(shape) == 2  # only valid for 2D tensors
    shape[-1] = 1
    return tuple(shape)


def split_others(x):
    Vother = x[:, :-1]
    return Vother


def split_others_output_shape(input_shape):
    shape = list(input_shape)
    assert len(shape) == 2  # only valid for 2D tensors
    shape[-1] = shape[-1] - 1

    return tuple(shape)


class MEDNPlus(Model):

    def __init__(self, n_in_len=30, n_in_ch=2, n_shells=2, nDict=301, tau=1e-10, lr=1.e-4,
                 ReLUThres=0.01):
        super(MEDNPlus, self).__init__()

        self.tau = tau

        self.n_in_len = n_in_len
        self.n_in_ch = n_in_ch
        self.n_shells = n_shells

        self.nDict = nDict
        self.ReLUThres = ReLUThres

        self.Q = Sequential()
        self.Q.add(Dense(self.n_in_len * self.n_shells, activation='relu', use_bias=True,
                         input_shape=(self.n_in_len * self.n_in_ch,)))

        self.W = Dense(self.nDict, activation='linear', use_bias=True)
        self.TNS = Sequential()
        self.TNS.add(ThresholdedReLU(theta=self.ReLUThres, input_shape=(self.nDict,)))
        self.TNS.add(Dense(self.nDict, activation='linear', use_bias=True))

        self.W_vic = Dense(2, kernel_constraint=NonNeg(), activation='linear', use_bias=True)

        self.loss = tf.keras.losses.MeanSquaredError()

        self.optimizer = tf.keras.optimizers.Adam(lr)

    def __call__(self, input):

        input_nb = Flatten()(input)

        input = self.Q(input_nb)
        w = self.W(input)
        Z = self.TNS(w)

        nLayers = 8
        for l in range(nLayers - 1):
            Y = Z + w
            Z = self.TNS(Y)
        Y = Z + w
        T = ThresholdedReLU(theta=self.ReLUThres)(Y)

        Viso = Lambda(split_last, output_shape=split_last_output_shape)(T)
        Vother = Lambda(split_others, output_shape=split_others_output_shape)(T)

        normVother = Lambda(lambda x: (x + self.tau) / tf.reshape(tf.norm(x + self.tau, axis=1), [x.shape[0], 1]))(Vother)
        VicK = self.W_vic(normVother)
        Kappa = Lambda(split_last, output_shape=split_last_output_shape)(VicK)
        Vic = Lambda(split_others, output_shape=split_others_output_shape)(VicK)
        OD = Lambda(lambda x: 2.0 / np.pi * tf.math.atan(1.0 / (x + self.tau)))(Kappa)

        x = tf.concat([Vic, Viso, OD], axis=1)

        return tf.expand_dims(x, axis=2)


class EstimatorMEDNPlus(Estimator):

    def __init__(self, batch_size=128, n_epochs=100, n_in=2, lr=1.e-4, n_dict=301, exp_name=None, **kwargs):
        super(EstimatorMEDNPlus, self).__init__(**kwargs)
        self.batch_size = batch_size
        self.n_dict = n_dict
        self.n_epochs = n_epochs
        self.n_in = n_in
        self.lr = lr
        self.exp_name = exp_name


    @tf.function
    def train_step(self, data_batch, labels_batch):

        with tf.GradientTape() as clf_tape:

            # Estimated class probabilities
            clf_estims = self.medn_model(data_batch)

            clf_loss = self.medn_model.loss(labels_batch, clf_estims)

        t_variables = self.medn_model.trainable_variables

        clf_grads = clf_tape.gradient(clf_loss, t_variables)

        # Updating trainable weights of classifier
        self.medn_model.optimizer.\
            apply_gradients(zip(clf_grads, t_variables))
        return clf_loss

    @tf.function
    def valid_step(self, data_batch):

        # Estimated class probabilities
        clf_estims = self.medn_model(data_batch)

        return clf_estims

    @tf.function
    def get_trainable_parameters(self):
        print(self.medn_model.trainable_variables)
        return self.medn_model.trainable_variables

    def save_model_weights(self, output_model_path):

        if not os.path.exists(output_model_path):
            os.makedirs(output_model_path)

        t_variables = self.medn_model.trainable_variables

        for v_idx, v in enumerate(t_variables):
            np.save(os.path.join(output_model_path, 'var_'+str(v_idx)+'.npy'), v)

    @staticmethod
    def _load_data_n(db, prep, data_list, n_samples=25600):
        count = 0
        for f in data_list:
            n_vox_path = os.path.join(prep.exp_out, db.name, prep.name,
                                      'n_vox_' + str(f))
            with open(n_vox_path, 'r') as f_vox:
                count += n_samples

        data = np.zeros((count, prep.n_in_len, prep.n_in_ch),
                        dtype=np.float32)
        gt = np.zeros((count, prep.n_out_len, prep.n_out_ch),
                      dtype=np.float32)
        count = 0
        for f in data_list:
            data_f = np.load(os.path.join(prep.exp_out, db.name,
                                          prep.name,
                                          'data_' + str(f) + '.npy'))
            gt_f = np.load(os.path.join(prep.exp_out, db.name,
                                        'ground_truth',
                                        prep.gt_type + '_' + str(f) + '.npy'))
            select = np.random.choice(data_f.shape[0], n_samples, replace=False)
            data[count:count + n_samples, :, :] = data_f[select, :, :]
            gt[count:count + n_samples, :, :] = gt_f[select, :, :]
            count += n_samples

        return data, gt

    def train_and_valid(self, db, prep):
        """Run training and validation of the model."""
        """
            Arguments:
                prep (Preprocessor): Preprocessor object
        """

        np.random.seed(1234)

        self.medn_model = MEDNPlus(lr=self.lr, n_in_len=prep.n_in_len, n_in_ch=prep.n_in_ch,
                                   n_shells=prep.n_shells, nDict=self.n_dict)

        output_dir = os.path.join(prep.exp_out, self.name())
        if not os.path.exists(output_dir):
            os.makedirs(output_dir)

        print("Loading validation data...")

        output_valid_error_path = os.path.join(output_dir, 'errors_valid.txt')
        output_model_path = os.path.join(output_dir, 'model_{}')

        for e in range(self.n_epochs + 1):
            print("Epoch:", e)
            select_train = np.random.choice(len(prep.train_list),
                                            np.min([10,
                                                    len(prep.train_list)]),
                                            replace=False)

            train_data, train_gt = self._load_data_n(db, prep, select_train)
            n_samples_t = train_gt.shape[0]
            train_tf = \
                tf.data.Dataset.from_tensor_slices((train_data, train_gt)).shuffle(n_samples_t).batch(self.batch_size)

            for t_idx, (t_batch, t_labs) in enumerate(train_tf):
                if t_idx == 2000:
                    break
                train_loss = self.train_step(t_batch, t_labs)

            select_valid = np.random.choice(len(prep.valid_list),
                                            np.min([10,
                                                    len(prep.valid_list)]),
                                            replace=False)
            v_list = [prep.valid_list[idx] for idx in select_valid]
            valid_data, valid_gt = self._load_data_n(db, prep, v_list, n_samples=12800)
            n_samples_v = valid_gt.shape[0]
            valid_tf = tf.data.Dataset.from_tensor_slices((valid_data, valid_gt)).shuffle(n_samples_v).batch(self.batch_size)

            valid_mse = np.zeros(self.batch_size * 1000)
            valid_abs = np.zeros((self.batch_size * 1000, 3))
            for v_idx, (v_batch, v_labs) in enumerate(valid_tf):
                if v_idx == 1000:
                    break
                v_estims = self.valid_step(v_batch)
                valid_mse[v_idx * self.batch_size:(v_idx + 1) * self.batch_size] = \
                    np.sum((v_labs - v_estims) ** 2, axis=1)[:, 0]
                valid_abs[v_idx * self.batch_size:(v_idx + 1) * self.batch_size, :] = \
                    np.abs(v_labs - v_estims)[:, :, 0]

            with open(output_valid_error_path, 'a+') as f_err:
                f_err.write('%s %s \n' % (np.ravel(np.mean(valid_abs, axis=0)), np.mean(valid_mse)))

            if e % 100 == 0 and e:
                self.test(db, prep, e)
                self.save_model_weights(output_model_path.format(e))

            if e == 60:
                self.medn_model.optimizer.lr.assign(self.lr/5)
                print(self.medn_model.optimizer.lr.read_value())

            if e == 80:
                self.medn_model.optimizer.lr.assign(self.lr/10)
                print(self.medn_model.optimizer.lr.read_value())

    def test(self, db, prep, model_no):
        """Run testing of the model."""
        """
            Arguments:
                db (Database): Database object
                prep (Preprocessor): Preprocessor object
                model_no (int): ordinal number of model
        """
        output_dir = os.path.join(prep.exp_out, self.name())

        test_output_dir = os.path.join(output_dir, 'test_' + str(model_no))
        if not os.path.exists(test_output_dir):
            os.makedirs(test_output_dir)

        for t in prep.test_list:

            test_data, test_gt = self._load_data(db, prep, [t])
            test_tf = tf.data.Dataset.from_tensor_slices((test_data, test_gt)).batch(self.batch_size)

            estims = np.zeros_like(test_gt)
            for t_idx, (t_batch, t_labs) in enumerate(test_tf):
                t_estim = self.valid_step(t_batch)
                estims[t_idx * self.batch_size:(t_idx + 1) * self.batch_size, :, :] = t_estim

            estim_out_path = os.path.join(test_output_dir, db.subjects_list[t])
            if not os.path.exists(estim_out_path):
                os.makedirs(estim_out_path)
            np.save(os.path.join(estim_out_path, 'estim.npy'), estims)

    def name(self):
        """Method that returns object's name including parameters."""
        return "%s(name=%s, lr=%s, n_dict=%s)" % (type(self).__name__, self.exp_name, self.lr, self.n_dict)
"""Class for dMRI descriptor (micro-structure parameters, fODF)
    estimation using  FCN."""


"""
Exact implementation of:

References:

[1] Golkov, V., Dosovitskiy, A., Sperl, J. I., Menzel, M. I., 
Czisch, M., Sämann, P., ... & Cremers, D. (2016). 
Q-space deep learning: twelve-fold shorter and model-free diffusion MRI scans. 
IEEE transactions on medical imaging, 35(5), 1344-1351.

"""

import sys
import os

import numpy as np

import time

import tensorflow as tf
from .. import Estimator
from tensorflow.keras import Model
from tensorflow.keras.layers import Dense, Flatten, Dropout


class FCN(Model):

    def __init__(self, units=150, n_feats=3, dp=0.1, lr=1.e-4):
        super(FCN, self).__init__()

        self.n_feats = n_feats
        self.lr = lr
        self.dp = dp
        self.units = units

        o_init = tf.keras.initializers.Orthogonal()
        self.fcn1 = Dense(self.units, activation='relu', kernel_initializer=o_init)
        self.dp1 = Dropout(self.dp)
        self.fcn2 = Dense(self.units, activation='relu', kernel_initializer=o_init)
        self.dp2 = Dropout(self.dp)
        self.fcn3 = Dense(self.units, activation='relu', kernel_initializer=o_init)
        self.dp3 = Dropout(self.dp)
        self.fcn4 = Dense(self.n_feats, activation=None, kernel_initializer=o_init)
        self.dp4 = Dropout(self.dp)

        self.loss = tf.keras.losses.MeanSquaredError()

        self.optimizer = tf.keras.optimizers.SGD(learning_rate=lr, momentum=0.9)

    def __call__(self, input, training=None):

        x = Flatten()(input)

        x1 = self.dp1(self.fcn1(x), training=training)
        x2 = self.dp2(self.fcn2(x1), training=training)
        x3 = self.dp3(self.fcn3(x2), training=training)
        x4 = self.dp4(self.fcn4(x3), training=training)
        x = tf.expand_dims(x4, axis=2)
        return x


class EstimatorFCN_Golkov(Estimator):

    def __init__(self, batch_size=128, n_epochs=10000, lr=0.001, dp=0.1, n_feats=2, exp_name=None, **kwargs):
        super(EstimatorFCN_Golkov, self).__init__(**kwargs)
        self.batch_size = batch_size
        self.n_epochs = n_epochs
        self.n_feats = n_feats
        self.lr = lr
        self.dp = dp
        self.exp_name = exp_name

    @tf.function
    def train_step(self, data_batch, labels_batch):

        with tf.GradientTape() as clf_tape:

            # Estimated class probabilities
            clf_estims = self.fcn_model(data_batch, training=True)

            clf_loss = self.fcn_model.loss(labels_batch, clf_estims)

        t_variables = self.fcn_model.trainable_variables

        clf_grads = clf_tape.gradient(clf_loss, t_variables)

        # Updating trainable weights of classifier
        self.fcn_model.optimizer.\
            apply_gradients(zip(clf_grads, t_variables))
        return clf_loss

    @tf.function
    def valid_step(self, data_batch):

        # Estimated class probabilities
        clf_estims = self.fcn_model(data_batch, training=False)

        return clf_estims

    @tf.function
    def get_trainable_parameters(self):
        print(self.fcn_model.trainable_variables)
        return self.fcn_model.trainable_variables

    def save_model_weights(self, output_model_path):

        if not os.path.exists(output_model_path):
            os.makedirs(output_model_path)

        t_variables = self.fcn_model.trainable_variables
        for v_idx, v in enumerate(t_variables):
            np.save(os.path.join(output_model_path, 'var_' + str(v_idx) + '.npy'), v)

    def train_and_valid(self, db, prep):
        """Run training and validation of the model."""
        """
            Arguments:
                prep (Preprocessor): Preprocessor object
        """

        self.fcn_model = FCN(n_feats=prep.n_out_len, lr=self.lr, dp=self.dp)

        output_dir = os.path.join(prep.exp_out, self.name())
        if not os.path.exists(output_dir):
            os.makedirs(output_dir)

        print("Loading validation data...")

        output_valid_error_path = os.path.join(output_dir, 'errors_valid.txt')
        output_train_error_path = os.path.join(output_dir, 'errors_train.txt')
        output_model_path = os.path.join(output_dir, 'model_{}')

        select_train_valid = np.random.choice(len(prep.train_list),
                                              np.min([30,
                                                      len(prep.train_list)]),
                                              replace=False)

        train_valid_data, train_valid_gt = self._load_data(db, prep, select_train_valid)

        idx_shuffle = np.arange(0, train_valid_gt.shape[0])
        np.random.shuffle(idx_shuffle)

        train_valid_data_shuffle = train_valid_data[idx_shuffle, :, :]
        train_valid_gt_shuffle = train_valid_gt[idx_shuffle, :, :]

        n_train = int(0.9 * train_valid_gt.shape[0])
        train_data = train_valid_data_shuffle[0:n_train, :, :]
        train_gt = train_valid_gt_shuffle[0:n_train, :, :]

        valid_data = train_valid_data_shuffle[n_train:, :, :]
        valid_gt = train_valid_gt_shuffle[n_train:, :, :]

        n_samples_v = valid_gt.shape[0]
        valid_tf = tf.data.Dataset.from_tensor_slices((valid_data, valid_gt)).batch(4 * self.batch_size)

        train_mse_track = []
        valid_mse_track = []
        for e in range(self.n_epochs + 1):
            print("Epoch:", e)

            if e == 10:
                self.lr = 0.01
                self.fcn_model.optimizer.lr.assign(self.lr)
                print(self.fcn_model.optimizer.lr.read_value())

            n_samples_t = train_gt.shape[0]
            train_tf = \
                tf.data.Dataset.from_tensor_slices((train_data, train_gt)).shuffle(n_samples_t).batch(self.batch_size)

            for t_idx, (t_batch, t_labs) in enumerate(train_tf):
                train_loss = self.train_step(t_batch, t_labs)

            train_mse = np.zeros(n_samples_t)
            train_abs = np.zeros((n_samples_t, prep.n_out_len))
            for t_idx, (t_batch, t_labs) in enumerate(train_tf):
                t_estims = self.valid_step(t_batch)
                train_mse[t_idx * self.batch_size:(t_idx + 1) * self.batch_size] = \
                    np.sum((t_labs - t_estims) ** 2, axis=1)[:, 0]
                train_abs[t_idx * self.batch_size:(t_idx + 1) * self.batch_size, :] = \
                    np.abs(t_labs - t_estims)[:, :, 0]

            print("train_mse:", e, np.mean(train_mse))
            train_mse_track.append(np.mean(train_mse))
            if e >= 20:
                train_mse_last_10 = np.asarray(train_mse_track)[-10:]
                mean_5_first = np.mean(train_mse_last_10[0:5])
                mean_5_last = np.mean(train_mse_last_10[5:])
                print("Train 5 mean first, mean last:", mean_5_first, mean_5_last)
                if np.abs(mean_5_first - mean_5_last) < 0.0001:
                    self.lr *= 0.9
                    self.fcn_model.optimizer.lr.assign(self.lr)
                    print(self.fcn_model.optimizer.lr.read_value())

            valid_mse = np.zeros(n_samples_v)
            valid_abs = np.zeros((n_samples_v, prep.n_out_len))
            for v_idx, (v_batch, v_labs) in enumerate(valid_tf):
                v_estims = self.valid_step(v_batch)
                valid_mse[v_idx * 4 * self.batch_size:(v_idx + 1) * 4 * self.batch_size] = \
                    np.sum((v_labs - v_estims) ** 2, axis=1)[:, 0]
                valid_abs[v_idx * 4 * self.batch_size:(v_idx + 1) * 4 * self.batch_size, :] = \
                    np.abs(v_labs - v_estims)[:, :, 0]

            print("valid_mse:", e, np.mean(valid_mse))
            valid_mse_track.append(np.mean(valid_mse))

            if e % 10 and e:
                self.test(db, prep, e)

            if e >= 30:
                valid_mse_last_20 = np.asarray(valid_mse_track)[-20:]
                mean_10_first = np.mean(valid_mse_last_20[0:10])
                mean_10_last = np.mean(valid_mse_last_20[10:])
                print("Valid 10 mean first, mean last:", mean_10_first, mean_10_last)
                if mean_10_last > mean_10_first:
                    self.save_model_weights(output_model_path.format(e))
                    self.test(db, prep, e)

            with open(output_valid_error_path, 'a+') as f_err:
                f_err.write('%s %s \n' % (np.ravel(np.mean(valid_abs, axis=0)), np.mean(valid_mse)))
            with open(output_train_error_path, 'a+') as f_err:
                f_err.write('%s %s \n' % (np.ravel(np.mean(train_abs, axis=0)), np.mean(train_mse)))
            print("\n")

    def test(self, db, prep, model_no):
        """Run testing of the model."""
        """
            Arguments:
                db (Database): Database object
                prep (Preprocessor): Preprocessor object
                model_no (int): ordinal number of model
        """
        output_dir = os.path.join(prep.exp_out, self.name())

        test_output_dir = os.path.join(output_dir, 'test_' + str(model_no))
        if not os.path.exists(test_output_dir):
            os.makedirs(test_output_dir)

        for t in prep.test_list:

            test_data, test_gt = self._load_data(db, prep, [t])
            test_tf = tf.data.Dataset.from_tensor_slices((test_data, test_gt)).batch(self.batch_size)

            estims = np.zeros_like(test_gt)
            for t_idx, (t_batch, t_labs) in enumerate(test_tf):
                t_estim = self.valid_step(t_batch)
                estims[t_idx * self.batch_size:(t_idx + 1) * self.batch_size, :, :] = t_estim

            estim_out_path = os.path.join(test_output_dir, db.subjects_list[t])
            if not os.path.exists(estim_out_path):
                os.makedirs(estim_out_path)
            np.save(os.path.join(estim_out_path, 'estim.npy'), estims)

    def name(self):
        """Method that returns object's name including parameters."""
        return "%s(name=%s, dp=%s, exp_name=%s)" % (type(self).__name__, self.exp_name, self.dp, self.exp_name)

"""Fourier domain S2CNN class."""

from .. import Estimator

import os
from os.path import join
import json
import numpy as np
from numpy import eye
from numpy.random import randn

import tensorflow as tf
from tensorflow.keras.layers import Dense
from tensorflow.keras.layers import Activation

from sympy.physics.quantum.cg import CG
from fourier_s2cnn.utils.clebsch_gordan import CG_dict_2d_R


class EstimatorFourier_S2_Zonal_CNN(Estimator):
    """Fourier domain S2CNN class."""

    """
        Attributes:
            layers_L (list): list of S2 and SO(3) bandwidths per layer
            n_in (int): number of input shells/channels
            n_len (int): number of input points
            n_feats (int): number of features to estimate
            Y_inv (numpy): 2D array containing spherical harmonics
            lambda_ (float): denoising weight
            dn_N (int): number of denoising layers
            lr (float): initial learning rate
    """
    def __init__(self, dn_N, lambda_,
                 layers_L, layers_N, fcn_N, n_in=2, n_len=60, w_std=0.1, dn=True,
                 **kwargs):
        """Initialize class attributes."""
        super(EstimatorFourier_S2_Zonal_CNN, self).__init__(**kwargs)
        #--------------------------------------------------------------------#
        self.layers_L = layers_L
        self.layers_N = layers_N
        self.fcn_N = fcn_N

        self.step_h = 2
        self.n_in = n_in
        self.n_len = n_len

        self.dn = dn
        self.lambda_ = lambda_
        self.dn_N = dn_N

        self.w_std = w_std

        #--------------------------------------------------------------------#
        #self.Y_inv = tf.convert_to_tensor(Y_inv, dtype=tf.float32)
        if self.dn:
            self.I = tf.convert_to_tensor(eye(self.n_len, dtype=np.float32))
            self.W_dn = {}
            for n in range(self.dn_N):
                r_int = 0.05 * randn(self.n_len, self.n_len).astype(np.float32)
                self.W_dn[n] = tf.Variable(r_int, trainable=True)
        #--------------------------------------------------------------------#
        self.CGr = CG_dict_2d_R(self.layers_L[0], self.layers_L[1])
        #--------------------------------------------------------------------#
        self.W , self.b = {}, {}
        for n in range(len(self.layers_L) - 1):
            self.W[n] = {}
            if n == 0:
                for l in range(0, self.layers_L[n] + 1, 2):
                    n_in, n_out = self.n_in, self.layers_N[n]
                    w = randn(n_in, n_out).astype(np.float32)
                    self.W[n][l] =tf.Variable(self.w_std * w, trainable=True)
            else:
                for l in range(0, self.layers_L[n] + 1, self.step_h):
                    n_in, n_out = self.layers_N[n - 1], self.layers_N[n]
                    w = randn(n_in, n_out).astype(np.float32)
                    self.W[n][l] = tf.Variable(self.w_std * w, trainable=True)
            #z = randn(self.layers_N[n], 1, 1).astype(np.float32)
            z = np.zeros((self.layers_N[n], 1), dtype=np.float32)
            self.b[n] = tf.Variable(z, trainable=True)

        #--------------------------------------------------------------------#
        # Fully connected layers
        o_init = tf.keras.initializers.Orthogonal()
        self.fcn = []
        for n in range(len(self.fcn_N)):
            self.fcn.append(Dense(self.fcn_N[n], activation='relu', 
                                  kernel_initializer=o_init))
        self.fcn.append(Dense(self.model_out_size, activation='sigmoid', 
                              kernel_initializer=o_init))
        #--------------------------------------------------------------------#
        # Loss function
        self.loss = tf.keras.losses.MeanSquaredError()
        # Optimizer
        self.optimizer = tf.keras.optimizers.Adam(self.lr)
        #--------------------------------------------------------------------#

    def __call__(self, input):
        """Call function."""

        #--------------------------------------------------------------------#
        in_r = tf.concat([tf.gather(input, 0, axis=2), 
                          tf.gather(input, 1, axis=2)], axis=1)

        if self.dn:
            for n in range(self.dn_N):
                in_r = Activation('relu')\
                    (tf.matmul(in_r, self.I + self.lambda_ * self.W_dn[n]))

        n_p_sh = self.n_len // self.n_in
        sh_1_rng = tf.range(0, n_p_sh)
        sh_2_rng = tf.range(n_p_sh, 2 * n_p_sh)

        in_sh_1 = tf.expand_dims(tf.gather(in_r, sh_1_rng, axis=1), axis=2)
        in_sh_2 = tf.expand_dims(tf.gather(in_r, sh_2_rng, axis=1), axis=2)
        in_dn = tf.concat([in_sh_1, in_sh_2], axis=2)
        in_sph = tf.einsum('npc,clp->ncl', in_dn, self.Y_inv)
        #--------------------------------------------------------------------#

        feats = []
        even_cov = self.get_even_covariants(in_sph, self.layers_L[0])
        for l in range(0, self.layers_L[0] + 1, 2):
            feats.append(tf.reduce_sum(tf.pow(even_cov[l], 2), axis=2))

        sh, sh2 = {}, {}
        sh2[0] = even_cov
        for n in range(1, len(self.layers_L)):
            sh[n] = {}
            sh2[n] = {}
            sh[n] = self.conv_Zonal(sh2[n-1], self.W[n - 1], self.b[n - 1],
                                    self.layers_L[n - 1], symm=True)
            sh2[n] = self.q_S2_non_lin(sh[n], 
                                       self.layers_L[n - 1], self.layers_L[n],
                                       CG_r=self.CGr, symm=True)
            for l in range(0, self.layers_L[n] + 1, self.step_h):
                feats.append(tf.reduce_sum(tf.pow(sh2[n][l], 2), axis=[2]))
        feats = tf.concat(feats, axis=1)
        #--------------------------------------------------------------------#
        # Fully connected layers
        for n in range(len(self.fcn_N)):
            feats = self.fcn[n](feats)
        #--------------------------------------------------------------------#
        return tf.expand_dims(self.fcn[n + 1](feats), axis=2)

    def get_even_covariants(self, x, L):
        """Create a dictionary with SH coefficients of even degree."""
        """
            Arguments:
                x (array): all SH coefficients (multi-channel)
                L (array): maximal SH degree
            Returns:
                dictionary with SH coefficients split according to degree
        """
        even_covariants = {}
        s = 0
        for l in range(0, L + 1, 2):
            o = 2 * l + 1
            even_covariants[l] = tf.gather(x, tf.range(s, s + o), axis=2)
            s += o

        return even_covariants

    def conv_Zonal(self, even_cov, W, b, L, symm=True, n=1):
        """Convolution between spherical signals and kernels in spectral domain."""
        """
            Arguments:
                even_cov (dict): dictionary with SH coefficients
                W (dict): dictionary with trainable kernels
                b (array): biases
                L (int): maximal SH degree

            Returns:
                dictionary with RH coefficients
        """
        if not symm:
            s = 1
        else:
            s = 2

        sh = {}
        c = 0
        for l in range(0, L + 1, s):
            if l == 0:
                sh[l] = tf.einsum('nil, io->nol', even_cov[l], W[l]) + b
            else:
                sh[l] = tf.einsum('nil, io->nol', even_cov[l], W[l])
            c += 1
        return sh


    def q_S2_non_lin(self, sh, L_in, L_out, CG_r, symm=True):

        if not symm:
            s = 1
        else:
            s = 2

        sh_n = {}
        for l in range(0, L_out + 1, s):
            for l1 in range(0, L_in + 1, s):
                for l2 in range(0, L_in + 1, s):
                    if l1 not in sh or l2 not in sh:
                        continue
                    if l2 < l1:
                        continue
                    if np.abs(l2 - l1) <= l <= (l1 + l2):

                        n, c, _ = sh[l1].shape

                        kron = tf.einsum('ncl,nck->nclk', sh[l1], sh[l2])
                        kron = tf.reshape(kron, [n, c, -1])

                        if l not in sh_n:
                            sh_n[l] = tf.einsum('nck,lk->ncl', kron, CG_r[l][l1, l2])
                        else:
                            sh_n[l] += tf.einsum('nck,lk->ncl', kron, CG_r[l][l1, l2])
        return sh_n


    @tf.function
    def train_step(self, data_batch, labels_batch):
        """Run training step for one batch."""
        """
            Arguments:
                data_batch : input data batch
                labels_batch: labels batch
            Returns:
                training loss
        """
        with tf.GradientTape() as clf_tape:

            # Estimated class probabilities
            clf_estims = self(data_batch)
            clf_loss = self.loss(labels_batch, clf_estims)

        t_variables = []
        for n in self.W_dn:
            t_variables.append(self.W_dn[n])
        for n in self.W:
            for l in self.W[n]:
                t_variables.append(self.W[n][l])
            t_variables.append(self.b[n])
        t_variables = t_variables + self.trainable_variables


        acc = 0
        for t_idx, t in enumerate(t_variables):
            c = 1
            sh = list(t.shape)
            for s in sh:
                c *= s
            acc += c
        print(acc)
        import sys
        sys.exit(2)


        clf_grads = clf_tape.gradient(clf_loss, t_variables)
        self.optimizer.apply_gradients(zip(clf_grads, t_variables))

        return clf_loss

    @tf.function
    def valid_step(self, data_batch):
        """Run validation step for one batch."""
        """
            Arguments:
                data_batch: input data batch
            Returns:
                estimated features
        """
        clf_estims  = self(data_batch)
        return clf_estims

    def save_model_weights(self, output_m_path):
        """Save model weights."""
        """
            Arguments:
                output_m_path (str): output model path
        """
        if not os.path.exists(output_m_path):
            os.makedirs(output_m_path)

        np.save(join(output_m_path, 'Y_inv.npy'), self.Y_inv.numpy())

        for sh_idx in self.coords_s2:
            coords_path = join(output_m_path, 'coords_{}.npy'.format(sh_idx))
            np.save(coords_path, self.coords_s2[sh_idx])

        t_variables = []
        for n in self.W_dn:
            t_variables.append(self.W_dn[n])
        for n in self.W:
            for l in self.W[n]:
                t_variables.append(self.W[n][l])
            t_variables.append(self.b[n])
        t_variables = t_variables + self.trainable_variables

        for v_idx, v in enumerate(t_variables):
            var_path = join(output_m_path, 'var_' + str(v_idx) + '.npy')
            np.save(var_path, v.numpy())


    @property
    def name(self):
        """Return object's name including parameters."""
        return "%s(name=%s, dn=%s, dn_N=%s, lambda=%s, " \
               "layers_L=%s, layers_N=%s, fcn_N=%s, " \
               "w_std=%s, lr=%s, batch=%s, n_train=%s)" % \
               (type(self).__name__, self.exp_name, 
                self.dn, self.dn_N, self.lambda_,
                self.layers_L, self.layers_N, self.fcn_N,
                self.w_std, self.lr, self.batch_size, self.n_train)

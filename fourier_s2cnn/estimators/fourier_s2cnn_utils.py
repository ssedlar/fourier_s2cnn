"""Functions related to Fourier domain spherical CNN."""

from sympy.physics.quantum.cg import CG
import numpy as np
import tensorflow as tf

from collections import OrderedDict


def convert_real_to_complex(L):
    """Convert real to complex spherical harmonics."""
    """
        Arguments:
            L (int): maximum SH degree
        Returns:
            numpy array: mapping between real and complex SH basis
    """
    n_sph = 2 * L + 1

    M = np.zeros((n_sph, n_sph), dtype=np.complex64)

    M[L, L] = 1
    for m in range(-L, 0):
        M[m + L, m + L] = -1 / np.sqrt(2) * 1j
        M[m + L, -m + L] = 1 / np.sqrt(2)
        M[-m + L, m + L] = np.power(-1, np.abs(m)) / np.sqrt(2) * 1j
        M[-m + L, -m + L] = np.power(-1, np.abs(m)) / np.sqrt(2)

    return M


def clebsch_gordan_matrix_R(L1, L2, L):
    """Create Clebsch-Gordan matrix associated to real WignerD matrices."""
    """
        Arguments:
            L1 (int): RH degree of the left RH coefficients
            L2 (int): RH degree of the right RH coefficients
            L (int): RH degree of the output RH coefficients
    """
    CG_M = np.zeros(((2 * L1 + 1) * (2 * L2 + 1), 2 * L + 1))
    for m in range(-L1, L1 + 1):
        for m_p in range(-L2, L2 + 1):
            M_p = m + m_p
            cg = CG(L1, m, L2, m_p, L, M_p).doit()
            if cg:
                CG_M[(m + L1) * (2 * L2 + 1) + m_p + L2, m + m_p + L] = cg

    M1 = convert_real_to_complex(L1)
    M2 = convert_real_to_complex(L2)
    M = convert_real_to_complex(L)

    if (L1 + L2 + L) % 2 == 0:
        return np.real(np.dot(M.conj().T, np.dot(CG_M.T, np.kron(M1, M2))))
    else:
        return np.imag(np.dot(M.conj().T, np.dot(CG_M.T, np.kron(M1, M2))))


def real_clebsch_gordan_all(L, L_max):
    """Create a dictionary with Clebsch-Gordan matrices."""
    """
        Arguments:
            L (int) : maximal RH degree of input
            L_max (int): maximal RH degree of output
        Returns:
            dictionary of Clebsch-Gordan matrices
    """
    if L_max > 2 * L:
        print("L_max should not be higher than 2*L")
        raise

    CG_all_r = {}
    CG_all_l = {}
    for l in range(0, L_max + 1):
        CG_all_r[l] = {}
        CG_all_l[l] = {}
        for l1 in range(0, L + 1):
            for l2 in range(0, L + 1):
                if l2 < l1:
                    continue
                if np.abs(l2 - l1) <= l <= (l1 + l2):

                    cg_ = clebsch_gordan_matrix_R(l1, l2, l)

                    cg_r = np.reshape(cg_,
                                      [2 * l + 1, 2 * l1 + 1, 2 * l2 + 1])
                    cg_l = np.reshape(cg_,
                                      [2 * l + 1, 2 * l1 + 1, 2 * l2 + 1]).\
                        transpose(0, 2, 1)

                    if l2 != l1:
                        CG_all_r[l][l1, l2] = \
                            tf.convert_to_tensor(np.sqrt(2) * cg_r,
                                                 dtype=tf.float32)
                        CG_all_l[l][l1, l2] = \
                            tf.convert_to_tensor(np.sqrt(2) * cg_l,
                                                 dtype=tf.float32)
                    else:
                        CG_all_r[l][l1, l2] = \
                            tf.convert_to_tensor(cg_r, dtype=tf.float32)
                        CG_all_l[l][l1, l2] = \
                            tf.convert_to_tensor(cg_l, dtype=tf.float32)

    return CG_all_r, CG_all_l

@tf.function
def get_even_covariants(x, L):
    """Create a dictionary with SH coefficients of even degree."""
    """
        Arguments:
            x (array): all SH coefficients (multi-channel)
            L (array): maximal SH degree
        Returns:
            dictionary with SH coefficients split according to degree
    """
    even_covariants = OrderedDict()
    s = 0
    for l in range(0, L + 1, 2):
        o = 2 * l + 1
        even_covariants[l] = tf.gather(x, tf.range(s, s + o), axis=2)
        s += o

    return even_covariants


def conv_Zonal(even_cov, W, b, L, symm=True, n=1):
    """Convolution between spherical signals and kernels in spectral domain."""
    """
        Arguments:
            even_cov (dict): dictionary with SH coefficients
            W (dict): dictionary with trainable kernels
            b (array): biases
            L (int): maximal SH degree

        Returns:
            dictionary with RH coefficients
    """
    if not symm:
        s = 1
    else:
        s = 2

    sh = {}
    c = 0
    for l in range(0, L + 1, s):
        W_l = tf.gather(W, c, axis=2)
        if l == 0:
            sh[l] = tf.einsum('nil, io->nol', even_cov[l], W_l) / n + b
        else:
            sh[l] = tf.einsum('nil, io->nol', even_cov[l], W_l) / n
        c += 1
    return sh

@tf.function
def conv_SH(even_cov, W, b, L, symm=True, n=1):
    """Convolution between spherical signals and kernels in spectral domain."""
    """
        Arguments:
            even_cov (dict): dictionary with SH coefficients
            W (dict): dictionary with trainable kernels
            b (array): biases
            L (int): maximal SH degree

        Returns:
            dictionary with RH coefficients
    """
    if not symm:
        s = 1
    else:
        s = 2

    rh = {}
    for l in range(0, L + 1, s):
        if l == 0:
            rh[l] = tf.einsum('nil, iok->nolk', even_cov[l], W[l]) / n + b
        else:
            rh[l] = tf.einsum('nil, iok->nolk', even_cov[l], W[l]) / n
    return rh

@tf.function
def quadratic_non_linearity(rh, L_in, L_out, CG_r, CG_l, symm=False):

    if not symm:
        s = 1
    else:
        s = 2

    rh_n = {}
    for l in range(0, L_out + 1, s):
        for l1 in range(0, L_in + 1, s):
            for l2 in range(0, L_in + 1, s):
                if l1 not in rh or l2 not in rh:
                    continue
                if l2 < l1:
                    continue
                if np.abs(l2 - l1) <= l <= (l1 + l2):

                    cg_r = CG_r[l][l1, l2]
                    cg_l = CG_l[l][l1, l2]

                    n, c, _, _ = rh[l1].shape

                    x = tf.reshape(tf.einsum('ncij,klj->nckli', rh[l2], cg_r),
                                   [n, c, 2 * l + 1, -1])
                    y = tf.reshape(tf.einsum('ncji,klj->nckil', rh[l1], cg_l),
                                   [n, c, 2 * l + 1, -1])
                    z = tf.einsum('ncki,ncji->nckj', y, x)

                    if l not in rh_n:
                        rh_n[l] = z
                    else:
                        rh_n[l] += z
    return rh_n

@tf.function
def quadratic_non_linearity_detailed(rh, L_in, L_out, CG_r, CG_l, symm=False):
    if not symm:
        s = 1
    else:
        s = 2

    rh_n, x_n, y_n = {}, {}, {}
    for l in range(0, L_out + 1, s):
        for l1 in range(0, L_in + 1, s):
            for l2 in range(0, L_in + 1, s):
                if l1 not in rh or l2 not in rh:
                    continue
                if l2 < l1:
                    continue
                if np.abs(l2 - l1) <= l <= (l1 + l2):

                    if l not in x_n:
                        x_n[l] = {}
                        y_n[l] = {}

                    cg_r = CG_r[l][l1, l2]
                    cg_l = CG_l[l][l1, l2]

                    n, c, _, _ = rh[l1].shape

                    x = tf.einsum('ncij,klj->nckli', rh[l2], cg_r)
                    x_l = tf.reshape(x, [n, c, 2 * l + 1, -1])
                    y = tf.einsum('ncji,klj->nckil', rh[l1], cg_l)
                    y_l = tf.reshape(y, [n, c, 2 * l + 1, -1])
                    z = tf.einsum('ncki,ncji->nckj', y_l, x_l)

                    x_n[l][l1, l2] = x
                    y_n[l][l1, l2] = y

                    if l not in rh_n:
                        rh_n[l] = z
                    else:
                        rh_n[l] += z
    return rh_n, y_n, x_n

@tf.function
def quadratic_non_linearity_S2(sh, L_in, L_out, CG_r, CG_l, symm=True):

    if not symm:
        s = 1
    else:
        s = 2

    sh_n = {}
    for l in range(0, L_out + 1, s):
        for l1 in range(0, L_in + 1, s):
            for l2 in range(0, L_in + 1, s):
                if l1 not in sh or l2 not in sh:
                    continue
                if l2 < l1:
                    continue
                if np.abs(l2 - l1) <= l <= (l1 + l2):

                    cg_matrix = tf.convert_to_tensor(clebsch_gordan_matrix_R(l1, l2, l),
                                                     dtype=tf.float32)
                    gaunt = np.sqrt((2 * l1 + 1) * (2 * l2 + 1) /
                                    (4 * np.pi * (2 * l + 1)))
                    cg_const = float(CG(l1, 0, l2, 0, l, 0).doit())

                    gaunt *= cg_const
                    cg_matrix *= gaunt

                    n, c, _ = sh[l1].shape

                    kron = tf.einsum('ncl,nck->nclk', sh[l1], sh[l2])
                    kron = tf.reshape(kron, [n, c, -1])

                    if l not in sh_n:
                        sh_n[l] = tf.einsum('nck,lk->ncl', kron, cg_matrix)
                    else:
                        sh_n[l] += tf.einsum('nck,lk->ncl', kron, cg_matrix)
    return sh_n

@tf.function
def conv_RH(rh_in, W, b, L, symm=False, n=1):

    if not symm:
        s = 1
    else:
        s = 2

    rh_out = {}
    for l in range(0, L + 1, s):
        if l == 0:
            rh_out[l] = (2 * l + 1) * tf.einsum('nilk, iokj->nolj', rh_in[l], W[l]) / n + b
        else:
            rh_out[l] = (2 * l + 1) * tf.einsum('nilk, iokj->nolj', rh_in[l], W[l]) / n
    return rh_out

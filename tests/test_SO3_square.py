"""Check SO(3) square transform."""
import numpy as np

from fourier_s2cnn.utils.sampling_points import get_so3_points

from fourier_s2cnn.utils.fourier_transforms import rh_basis_complex
from fourier_s2cnn.utils.fourier_transforms import rh_basis_real

from fourier_s2cnn.utils.clebsch_gordan import clebsch_gordan_matrix_R
from fourier_s2cnn.utils.clebsch_gordan import clebsch_gordan_matrix_C


def main():
    """Check SO(3) square transform."""
    N = 1000
    L = 3
    so3_points = get_so3_points(N=N)

    D_r = rh_basis_real(so3_points, L=L, even=False)
    D_c = rh_basis_complex(so3_points, L=L, even=False)

    D_r_inv = np.linalg.pinv(D_r)
    D_c_inv = np.linalg.pinv(D_c)

    so3_signal = np.random.randn(N)

    rh_r = np.dot(D_r_inv, so3_signal)
    rh_c = np.dot(D_c_inv, so3_signal)

    so3_signal = np.dot(D_r, rh_r)
    so3_sq = so3_signal * so3_signal

    print("-----------------------------------------------------------------")
    print("Check if square of SO(3) signal in signal domain is equal to "
          "Clebsch-Gordan transform in spectral domain followed by "
          "conversion to signal domain.")
    print("-----------------------------------------------------------------")
    n_rh_out = np.sum([(2 * k + 1) ** 2 for k in range(0, 2 * L + 1)])
    rh_r_test = np.zeros(n_rh_out, dtype=np.float32)
    rh_c_test = np.zeros(n_rh_out, dtype=np.complex64)

    for l in range(0, 2 * L + 1):
        for l1 in range(0, L + 1):
            for l2 in range(0, L + 1):
                if l < np.abs(l1 - l2) or l > l1 + l2:
                    continue
                # Define Clebsch-Gordan matrices for l1, l2, l
                CG_r = clebsch_gordan_matrix_R(l1, l2, l)
                CG_c = clebsch_gordan_matrix_C(l1, l2, l)

                # Get starting and ending indices of RH coefficients for l1
                idx_s1 = int(np.sum([(2 * k + 1) ** 2
                                     for k in range(0, l1)]))
                idx_e1 = int(np.sum([(2 * k + 1) ** 2
                                     for k in range(0, l1 + 1)]))
                # Get RH coefficients for l1
                rh_r_l1 = np.reshape(rh_r[idx_s1:idx_e1],
                                     [2 * l1 + 1, 2 * l1 + 1])
                rh_c_l1 = np.reshape(rh_c[idx_s1:idx_e1],
                                     [2 * l1 + 1, 2 * l1 + 1])

                # Get starting and ending indices of RH coefficients for l2
                idx_s2 = int(np.sum([(2 * k + 1) ** 2
                                     for k in range(0, l2)]))
                idx_e2 = int(np.sum([(2 * k + 1) ** 2
                                     for k in range(0, l2 + 1)]))
                # Get RH coefficients for l2
                rh_r_l2 = np.reshape(rh_r[idx_s2:idx_e2],
                                     [2 * l2 + 1, 2 * l2 + 1])
                rh_c_l2 = np.reshape(rh_c[idx_s2:idx_e2],
                                     [2 * l2 + 1, 2 * l2 + 1])

                # Get starting and ending indices of RH coefficients for l
                idx_s = int(np.sum([(2 * k + 1) ** 2
                                    for k in range(0, l)]))
                idx_e = int(np.sum([(2 * k + 1) ** 2
                                    for k in range(0, l + 1)]))
                # Get RH coefficients for l via Clebsch-Gordan transform
                rh_r_l = np.dot(CG_r,
                                np.dot(np.kron(rh_r_l1, rh_r_l2), CG_r.T))
                rh_r_l = np.ravel(rh_r_l)
                rh_c_l = np.dot(CG_c,
                                np.dot(np.kron(rh_c_l1, rh_c_l2), CG_c.T))
                rh_c_l = np.ravel(rh_c_l)

                rh_r_test[idx_s:idx_e] += rh_r_l
                rh_c_test[idx_s:idx_e] += rh_c_l

    D_r_out = rh_basis_real(so3_points, L=2 * L, even=False)
    D_c_out = rh_basis_complex(so3_points, L=2 * L, even=False)

    # Transform squared SO(3) signal from spectral to signal domain
    so3_out_r = np.dot(D_r_out, rh_r_test)
    so3_out_c = np.dot(D_c_out, rh_c_test)

    print("-----------------------------------------------------------------")
    print("Diff real complex:",
          np.sum(np.abs(so3_out_r - np.real(so3_out_c))))
    print("Diff real:", np.sum(np.abs(so3_out_r - so3_sq)))
    print("Diff complex:", np.sum(np.abs(so3_out_c - so3_sq)))
    print("-----------------------------------------------------------------")


if __name__ == '__main__':
    main()

"""Compare quadratic functions with and without optimization."""
from sympy.physics.quantum.cg import CG
import numpy as np
import tensorflow as tf
import time


def convert_real_to_complex(L):
    """Convert real to complex spherical harmonics."""
    """
        Arguments:
            L (int): maximum SH degree
        Returns:
            numpy array: mapping between real and complex SH basis
    """
    n_sph = 2 * L + 1

    M = np.zeros((n_sph, n_sph), dtype=np.complex64)

    M[L, L] = 1
    for m in range(-L, 0):
        M[m + L, m + L] = -1 / np.sqrt(2) * 1j
        M[m + L, -m + L] = 1 / np.sqrt(2)
        M[-m + L, m + L] = np.power(-1, np.abs(m)) / np.sqrt(2) * 1j
        M[-m + L, -m + L] = np.power(-1, np.abs(m)) / np.sqrt(2)

    return M


def clebsch_gordan_matrix_R(L1, L2, L):
    """Create Clebsch-Gordan matrix associated to real WignerD matrices."""
    """
        Arguments:
            L1 (int): RH degree of the left RH coefficients
            L2 (int): RH degree of the right RH coefficients
            L (int): RH degree of the output RH coefficients
    """
    CG_M = np.zeros(((2 * L1 + 1) * (2 * L2 + 1), 2 * L + 1))
    for m in range(-L1, L1 + 1):
        for m_p in range(-L2, L2 + 1):
            M_p = m + m_p
            cg = CG(L1, m, L2, m_p, L, M_p).doit()
            if cg:
                CG_M[(m + L1) * (2 * L2 + 1) + m_p + L2, m + m_p + L] = cg

    M1 = convert_real_to_complex(L1)
    M2 = convert_real_to_complex(L2)
    M = convert_real_to_complex(L)

    if (L1 + L2 + L) % 2 == 0:
        return np.real(np.dot(M.conj().T, np.dot(CG_M.T, np.kron(M1, M2))))
    else:
        return np.imag(np.dot(M.conj().T, np.dot(CG_M.T, np.kron(M1, M2))))


def real_clebsch_gordan_all_1(L, L_max):
    """Create a dictionary with Clebsch-Gordan matrices."""
    """
        Arguments:
            L (int) : maximal RH degree of input
            L_max (int): maximal RH degree of output
        Returns:
            dictionary of Clebsch-Gordan matrices
    """
    if L_max > 2 * L:
        print("L_max should not be higher than 2*L")
        raise

    CG_all = {}
    for l in range(0, L_max + 1):
        CG_all[l] = {}
        for l1 in range(0, L + 1):
            for l2 in range(0, L + 1):
                if l2 < l1:
                    continue
                if np.abs(l2 - l1) <= l <= (l1 + l2):

                    cg_ = clebsch_gordan_matrix_R(l1, l2, l)

                    if l2 != l1:
                        CG_all[l][l1, l2] = tf.convert_to_tensor(np.sqrt(2) * cg_, dtype=tf.float32)
                    else:
                        CG_all[l][l1, l2] = tf.convert_to_tensor(cg_, dtype=tf.float32)

    return CG_all


def real_clebsch_gordan_all_2(L, L_max):
    """Create a dictionary with Clebsch-Gordan matrices."""
    """
        Arguments:
            L (int) : maximal RH degree of input
            L_max (int): maximal RH degree of output
        Returns:
            dictionary of Clebsch-Gordan matrices
    """
    if L_max > 2 * L:
        print("L_max should not be higher than 2*L")
        raise

    CG_all_r = {}
    CG_all_l = {}
    for l in range(0, L_max + 1):
        CG_all_r[l] = {}
        CG_all_l[l] = {}
        for l1 in range(0, L + 1):
            for l2 in range(0, L + 1):
                if l2 < l1:
                    continue
                if np.abs(l2 - l1) <= l <= (l1 + l2):

                    cg_ = clebsch_gordan_matrix_R(l1, l2, l)

                    cg_r = np.reshape(cg_, [2 * l + 1, 2 * l1 + 1, 2 * l2 + 1])
                    cg_l = np.reshape(cg_, [2 * l + 1, 2 * l1 + 1, 2 * l2 + 1]).transpose(0, 2, 1)

                    if l2 != l1:
                        CG_all_r[l][l1, l2] = tf.convert_to_tensor(np.sqrt(2) * cg_r, dtype=tf.float32)
                        CG_all_l[l][l1, l2] = tf.convert_to_tensor(np.sqrt(2) * cg_l, dtype=tf.float32)
                    else:
                        CG_all_r[l][l1, l2] = tf.convert_to_tensor(cg_r, dtype=tf.float32)
                        CG_all_l[l][l1, l2] = tf.convert_to_tensor(cg_l, dtype=tf.float32)

    return CG_all_r, CG_all_l


def tf_kron(a, b):
    """Perform Kronecker product of RH coefficients over two last dimensions."""
    """
        Arguments:
            a (numpy array Nb x Nc x M x M): 1st input array
            b (numpy array Nb x Nc x N x N): 2nd input array
        Returns:
            numpy array, Kronecker product over last two dimensions
    """
    a_shape = [a.shape[0], a.shape[1], a.shape[2], a.shape[3]]
    b_shape = [b.shape[0], b.shape[1], b.shape[2], b.shape[3]]

    x = tf.einsum('nolk, nopq->nolpkq', a, b)
    x = tf.reshape(x,
                   [a_shape[0], b_shape[1],
                    a_shape[2] * b_shape[2],
                    a_shape[3] * b_shape[3]])
    return x


def qudrature_non_linearity_1(rh, L_in, L_out, CG_dict):
    """Perform quadtratic non-linearity."""
    rh_n = {}
    for l in range(0, L_out + 1):
        for l1 in range(0, L_in + 1):
            for l2 in range(0, L_in + 1):
                if l1 not in rh or l2 not in rh:
                    continue
                if l2 < l1:
                    continue
                if np.abs(l2 - l1) <= l <= (l1 + l2):
                    cg_r = CG_dict[l][l1, l2]

                    if l not in rh_n:
                        rh_n[l] = tf.einsum('ncoi,ki->ncok',
                                            tf.einsum('oi,ncik->ncok',
                                                      cg_r, tf_kron(rh[l1], rh[l2])),
                                            cg_r)
                    else:
                        rh_n[l] += tf.einsum('ncoi,ki->ncok',
                                             tf.einsum('oi,ncik->ncok',
                                                       cg_r, tf_kron(rh[l1], rh[l2])),
                                             cg_r)
    return rh_n


def qudrature_non_linearity_2(rh, L_in, L_out, CG_r, CG_l):
    """Perform quadtratic non-linearity."""
    import matplotlib.pyplot as plt
    rh_n = {}
    for l in range(0, L_out + 1):
        for l1 in range(0, L_in + 1):
            if l1 == 0:
                n, c, _, _ = rh[l1].shape
            for l2 in range(0, L_in + 1):
                if l1 not in rh or l2 not in rh:
                    continue
                if l2 < l1:
                    continue
                if np.abs(l2 - l1) <= l <= (l1 + l2):

                    cg_r = CG_r[l][l1, l2]
                    cg_l = CG_l[l][l1, l2]

                    x = tf.reshape(tf.einsum('ncij,klj->nckli', rh[l2], cg_r), [n, c, 2 * l + 1, -1])
                    y = tf.reshape(tf.einsum('ncji,klj->nckil', rh[l1], cg_l), [n, c, 2 * l + 1, -1])

                    z = tf.einsum('ncki,ncji->nckj', y, x)

                    if l not in rh_n:
                        rh_n[l] = z
                    else:
                        rh_n[l] += z

    return rh_n


def derivative_1(A, Z, lA, lZ, CG_dict, n=8, c=10):

    grad = np.zeros((n, c, 2 * lA + 1, 2 * lA + 1, 2 * lZ + 1, 2 * lZ + 1), dtype=np.float32)

    for l1, l2 in CG_dict[lA]:
        if l2 < l1:
            continue
        for i in range(2 * lZ + 1):
            for j in range(2 * lZ + 1):
                if np.abs(l2 - l1) <= lA <= (l1 + l2):
                    if l1 == lZ:
                        derivative = np.zeros((n, c, 2 * lZ + 1, 2 * lZ + 1), dtype=np.float32)
                        derivative[:, :, i, j] = 1
                        derivative_tf = tf.convert_to_tensor(derivative)
                        tmp_1 = tf_kron(derivative_tf, Z[l2])
                    if l2 == lZ:
                        derivative = np.zeros((n, c, 2 * lZ + 1, 2 * lZ + 1), dtype=np.float32)
                        derivative[:, :, i, j] = 1
                        derivative_tf = tf.convert_to_tensor(derivative)
                        tmp_2 = tf_kron(Z[l1], derivative_tf)

                    if l1 == lZ and l2 != lZ:
                        tmp = tmp_1
                    if l2 == lZ and l1 != lZ:
                        tmp = tmp_2

                    if l1 == lZ and l2 == lZ:
                        tmp = tmp_1 + tmp_2

                    if l1 != lZ and l2 != lZ:
                        continue

                    cg_r = CG_dict[lA][l1, l2]
                    grad[:, :, :, :, i, j] += tf.einsum('ncoi,ki->ncok', tf.einsum('oi,ncik->ncok', cg_r, tmp), cg_r)

    return grad


def derivative_2(A, Z, lA, lZ, CG_dict_r, CG_dict_l, n=8, c=10):

    grad = np.zeros((8, 10, 2 * lA + 1, 2 * lA + 1, 2 * lZ + 1, 2 * lZ + 1), dtype=np.float32)

    import matplotlib.pyplot as plt
    for l1, l2 in CG_dict_r[lA]:
        if l2 < l1:
            continue
        for i in range(2 * lZ + 1):
            for j in range(2 * lZ + 1):
                if np.abs(l2 - l1) <= lA <= (l1 + l2):
                    cg_r = CG_dict_r[lA][l1, l2]
                    cg_l = CG_dict_l[lA][l1, l2]

                    if l1 == lZ:
                        derivative = np.zeros((n, c, 2 * lZ + 1, 2 * lZ + 1), dtype=np.float32)
                        derivative[:, :, i, j] = 1
                        derivative_tf = tf.convert_to_tensor(derivative)

                        dy = tf.reshape(tf.einsum('ncji,klj->nckil', derivative_tf, cg_l), [n, c, 2 * lA + 1, -1])
                        x = tf.reshape(tf.einsum('ncij,klj->nckli', Z[l2], cg_r), [n, c, 2 * lA + 1, -1])
                        grad[:, :, :, :, i, j] += tf.einsum('ncki,ncji->nckj', dy, x)

                    if l2 == lZ:
                        derivative = np.zeros((n, c, 2 * lZ + 1, 2 * lZ + 1), dtype=np.float32)
                        derivative[:, :, i, j] = 1
                        derivative_tf = tf.convert_to_tensor(derivative)
                        y = tf.reshape(tf.einsum('ncji,klj->nckil', Z[l1], cg_l), [n, c, 2 * lA + 1, -1])
                        dx = tf.reshape(tf.einsum('ncij,klj->nckli', derivative_tf, cg_r), [n, c, 2 * lA + 1, -1])
                        grad[:, :, :, :, i, j] += tf.einsum('ncki,ncji->nckj', y, dx)

    return grad


def main():
    """Compare quadratic function with and without optimization."""
    print("Should be cleaned...")
    CG_dict = real_clebsch_gordan_all_1(6, 6)
    CG_dict_r, CG_dict_l = real_clebsch_gordan_all_2(6, 6)

    np.random.seed(123)
    Z_1 = {}
    for i in range(0, 6 + 1):
        Z_1[i] = tf.convert_to_tensor(np.random.randn(8, 10, 2 * i + 1, 2 * i + 1).astype(np.float32),
                                      dtype=tf.float32)

    time_start = time.time()
    A_1 = qudrature_non_linearity_1(Z_1, 6, 6, CG_dict)
    time_end = time.time()

    dA_1_dZ = derivative_1(A_1, Z_1, lA=4, lZ=6, CG_dict=CG_dict)

    print("Time elapsed 1:", time_end - time_start)

    time_start = time.time()
    A_2 = qudrature_non_linearity_2(Z_1, 6, 6, CG_dict_r, CG_dict_l)
    time_end = time.time()

    dA_2_dZ = derivative_2(A_2, Z_1, lA=4, lZ=6, CG_dict_r=CG_dict_r, CG_dict_l=CG_dict_l)
    print("Time elapsed 2:", time_end - time_start)

    import matplotlib.pyplot as plt

    plt.figure(1)
    plt.imshow(dA_1_dZ[0, 0, 0, 0, :, :])
    plt.figure(2)
    plt.imshow(dA_2_dZ[0, 0, 0, 0, :, :])
    plt.show()
    print(np.max(np.abs(dA_1_dZ - dA_2_dZ)))
    import sys
    sys.exit(2)
    for l in A_1:
        print(l, np.mean(np.abs(A_1[l] - A_2[l])))

if __name__ == '__main__':
    main()

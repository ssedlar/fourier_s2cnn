"""Check SH basis definitions, conversions and inversions."""
import numpy as np

from fourier_s2cnn.utils.sampling_points import load_s2_points

from fourier_s2cnn.utils.fourier_transforms import sh_basis_real
from fourier_s2cnn.utils.fourier_transforms import sh_basis_complex

from fourier_s2cnn.utils.fourier_transforms import convert_real_to_complex
from fourier_s2cnn.utils.fourier_transforms import convert_complex_to_real

from fourier_s2cnn.utils.fourier_transforms import lms_sh_inv
from fourier_s2cnn.utils.fourier_transforms import lms_tikhonov_sh_inv
from fourier_s2cnn.utils.fourier_transforms import lms_laplace_beltrami_sh_inv
from fourier_s2cnn.utils.fourier_transforms import gram_schmidt_sh_inv


def check_conversion_unitary_matrix(L=6):
    """Check orthogonality of unitary matrices."""
    """
        Arguments:
            L (int): signal bandwidth
    """
    M = convert_real_to_complex(L)
    M_inv = convert_complex_to_real(L)

    M_d = np.dot(M, M_inv)

    print("-----------------------------------------------------------------")
    print("Check orthogonality of unitary matrix")
    real_diff = np.sum(np.abs(np.real(M_d) - np.eye(2 * L + 1)))
    imag = np.sum(np.abs(np.imag(M_d)))
    if real_diff < 1.e-6 and imag < 1.e-6:
        print("OK", real_diff, imag)
    print("-----------------------------------------------------------------")
    print("\n")


def check_real_complex_conversion(L=6, N=30):
    """Check conversion of real to complex SH basis."""
    """
        Arguments:
            L (int): signal bandwidth
            N (int): number of sampling points
    """
    print("-----------------------------------------------------------------")
    print("Check conversion from real to complex spherical harmonics basis")

    s2_points = load_s2_points(N=N)
    Y_r = sh_basis_real(s2_points, L=6, even=False)
    Y_c = sh_basis_complex(s2_points, L=6, even=False)

    c = 0
    for l in range(0, L + 1):
        M = convert_real_to_complex(L=l)
        Y_c_test = np.dot(M, Y_r[:, c:c + 2 * l + 1].T).T
        Y_c_def = Y_c[:, c:c + 2 * l + 1]
        c += 2 * l + 1
        print("Degree, difference:", l, np.sum(np.abs(Y_c_def - Y_c_test)))
        print("\n")
    print("-----------------------------------------------------------------")


def check_complex_real_conversion(L=6, N=30):
    """Check conversion of complex to real SH basis."""
    """
        Arguments:
            L (int): signal bandwidth
            N (int): number of sampling points
    """
    print("-----------------------------------------------------------------")
    print("Check conversion from complex to real spherical harmonics basis")

    s2_points = load_s2_points(N=N)
    Y_r = sh_basis_real(s2_points, L=L, even=False)
    Y_c = sh_basis_complex(s2_points, L=L, even=False)

    c = 0
    for l in range(0, L + 1):
        M = convert_complex_to_real(L=l)
        Y_r_test = np.dot(M, Y_c[:, c:c + 2 * l + 1].T).T
        Y_r_def = Y_r[:, c:c + 2 * l + 1]
        c += 2 * l + 1
        print("Degree, difference:", l, np.sum(np.abs(Y_r_def - Y_r_test)))
        print("\n")
    print("-----------------------------------------------------------------")


def check_SH_estimation(L=6, N=30, n_samples=1000):
    """Check SH estimation."""
    """
        Arguments:
            L (int): signal bandwidth
            N (int): number of sampling points
            n_samples (int): number of S2 signals to generate
    """
    print("-----------------------------------------------------------------")
    print("Check estimation of spherical harmonic coefficients")

    s2_points = load_s2_points(N=N)
    s2_points_500 = load_s2_points(N=500)

    print("Get spherical harmonic basis...")
    Y = sh_basis_real(s2_points, L=L)

    print("Get inversed spherical harmonic basis...")
    print("Least mean square / Moore-Penrose...")
    Y_inv_lms = lms_sh_inv(s2_points, L=L)
    print("Least mean square with Tikhonov regularization...")
    Y_inv_tikh = lms_tikhonov_sh_inv(s2_points, L=L, lambda_=0.001)
    print("Least mean square with Laplace-Beltrami regularization...")
    Y_inv_lb = lms_laplace_beltrami_sh_inv(s2_points, L=L, lambda_=0.001)
    print("Gram-Schmidt orthonormalization...")
    Y_inv_gs = gram_schmidt_sh_inv(s2_points, L=L, n_iters=1000)

    print("Generate positive antipodally symmetric S2 signal samples...")
    Y_inv_lms_500 = lms_sh_inv(s2_points_500, L=L)
    S2_signals_500 = 1 + 0.5 * np.random.randn(500, n_samples)
    S2_signals_500 *= (S2_signals_500 >= 0)
    SH_coeffs = np.dot(Y_inv_lms_500, S2_signals_500)
    S2_signals = np.dot(Y, SH_coeffs)

    print("Add Rician noise...")
    n1 = 0.3 * np.random.randn(*S2_signals.shape)
    n2 = 0.3 * np.random.randn(*S2_signals.shape)
    S2_signals_n = np.sqrt((S2_signals + n1) ** 2 + n2**2)

    print("Estimate spherical harmonic coefficients...")
    print("With least mean square...")
    SH_coeffs_lms = np.dot(Y_inv_lms, S2_signals_n)
    print("With least mean square with Tikhonov regularization...")
    SH_coeffs_tikh = np.dot(Y_inv_tikh, S2_signals_n)
    print("With least mean square with Laplace-Beltrami regularization...")
    SH_coeffs_lb = np.dot(Y_inv_lb, S2_signals_n)
    print("With Gram-Schmidt orthonormalization...")
    SH_coeffs_gs = np.dot(Y_inv_gs, S2_signals_n)

    diff_lms = np.mean(np.abs(SH_coeffs - SH_coeffs_lms), axis=1)
    diff_tikh = np.mean(np.abs(SH_coeffs - SH_coeffs_tikh), axis=1)
    diff_lb = np.mean(np.abs(SH_coeffs - SH_coeffs_lb), axis=1)
    diff_gs = np.mean(np.abs(SH_coeffs - SH_coeffs_gs), axis=1)

    print("Mean absolute differences per each degree...")
    c = 0
    for l in range(0, L + 1, 2):
        print(l, "LMS:")
        print(diff_lms[c:c + 2 * l + 1])
        print(l, "LMS + Tikhonov:")
        print(diff_tikh[c:c + 2 * l + 1])
        print(l, "LMS + Laplace-Beltrami:")
        print(diff_lb[c:c + 2 * l + 1])
        print(l, "Gram-Schmidt:")
        print(diff_gs[c:c + 2 * l + 1])
        c += 2 * l + 1
        print("\n")
    print("-----------------------------------------------------------------")


def main():
    """Run test functions."""
    check_conversion_unitary_matrix(L=6)
    check_real_complex_conversion(L=6, N=30)
    check_complex_real_conversion(L=6, N=30)
    check_SH_estimation()


if __name__ == '__main__':
    main()

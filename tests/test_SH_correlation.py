"""Check correlation of S2 signals."""
import numpy as np

from fourier_s2cnn.utils.sampling_points import load_s2_points
from fourier_s2cnn.utils.sampling_points import get_so3_points

from fourier_s2cnn.utils.fourier_transforms import sh_basis_real
from fourier_s2cnn.utils.fourier_transforms import sh_basis_complex

from fourier_s2cnn.utils.fourier_transforms import wigner_D_complex
from fourier_s2cnn.utils.fourier_transforms import wigner_D_real


def main():
    """Test correlation of SH coefficients."""
    antipodal_symmetry = False

    if antipodal_symmetry:
        s = 2
    else:
        s = 1

    s2_points = load_s2_points(N=500)
    so3_points = get_so3_points(10)
    n = 3  # Select SO(3) rotation sample

    s2_signal_1 = np.random.randn(500)
    s2_signal_2 = np.random.randn(500)

    L = 6
    Y_r = sh_basis_real(s2_points, L=L, even=antipodal_symmetry)
    Y_c = sh_basis_complex(s2_points, L=L, even=antipodal_symmetry)

    Y_r_inv = np.linalg.pinv(Y_r)
    Y_c_inv = np.linalg.pinv(Y_c)

    sph_r_1 = np.dot(Y_r_inv, s2_signal_1)
    sph_r_2 = np.dot(Y_r_inv, s2_signal_2)
    sph_c_1 = np.dot(Y_c_inv, s2_signal_1)
    sph_c_2 = np.dot(Y_c_inv, s2_signal_2)

    print("-----------------------------------------------------------------")
    print("Check if correlation of real and complex spherical harmonic"
          " coefficients yields same SO(3) signals")

    c = 0
    for l in range(0, L + 1, s):
        corr_r = np.dot(np.reshape(sph_r_1[c:c + 2 * l + 1], [-1, 1]),
                        np.reshape(sph_r_2[c:c + 2 * l + 1], [1, -1]))
        corr_c = np.dot(np.reshape(sph_c_1[c:c + 2 * l + 1], [-1, 1]),
                        np.reshape(sph_c_2[c:c + 2 * l + 1].conj(), [1, -1]))
        Rr = wigner_D_real(so3_points[n:n + 1, :], L=l)[:, :, 0]
        Rc = wigner_D_complex(so3_points[n:n + 1, :], L=l)[:, :, 0]
        c += 2 * l + 1

        p_r = np.sum(corr_r * Rr)
        p_c = np.sum(corr_c * Rc)
        print("SO(3) diff:", l, np.sum(np.abs(p_r - p_c)))
    print("-----------------------------------------------------------------")


if __name__ == '__main__':
    main()

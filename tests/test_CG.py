"""Check Clebsch-Gordan transform."""
import numpy as np
from sympy.physics.quantum.cg import CG

from fourier_s2cnn.utils.sampling_points import get_so3_points

from fourier_s2cnn.utils.fourier_transforms import wigner_D_complex
from fourier_s2cnn.utils.fourier_transforms import wigner_D_real

from fourier_s2cnn.utils.clebsch_gordan import clebsch_gordan_matrix_C
from fourier_s2cnn.utils.clebsch_gordan import clebsch_gordan_matrix_R


def test_CG_transform_complex_1(L1, L2):
    """Check Clebsch-Gordan transform  1."""
    print("-----------------------------------------------------------------")
    print("Express kronecker product of complex Wigner D matrices of "
          "degrees L1 and L2 via Clebsch-Gordan transform of "
          "irreducible complex Wigner D matrices.")
    so3_points = get_so3_points(10)[5:6, :]

    D_L1 = wigner_D_complex(so3_points, L1)[:, :, 0]
    D_L2 = wigner_D_complex(so3_points, L2)[:, :, 0]

    D_12_kron = np.kron(D_L1, D_L2)

    D_12_test = np.zeros_like(D_12_kron)
    for m in range(-L1, L1 + 1):
        for n in range(-L1, L1 + 1):
            for m_p in range(-L2, L2 + 1):
                for n_p in range(-L2, L2 + 1):
                    Mp = m + m_p
                    Np = n + n_p
                    for Lp in range(np.abs(L1 - L2), L1 + L2 + 1):
                        D_Lp = wigner_D_complex(so3_points, Lp)[:, :, 0]
                        cg_m = CG(L1, m, L2, m_p, Lp, Mp).doit()
                        cg_n = CG(L1, n, L2, n_p, Lp, Np).doit()
                        if cg_m != 0 and cg_n != 0:
                            D_12_test[(m + L1) * (2 * L2 + 1) + m_p + L2,
                                      (n + L1) * (2 * L2 + 1) + n_p + L2] += \
                                cg_m * cg_n * D_Lp[m + m_p + Lp, n + n_p + Lp]
    print("Wigner D diff:", np.sum(np.abs(D_12_test - D_12_kron)))
    print("-----------------------------------------------------------------")


def test_CG_transform_complex_2(L1, L2, L):
    """Check Clebsch-Gordan transform 2."""
    print("-----------------------------------------------------------------")
    print("Check if complex Wigner-D matrix of degree L is equal to "
          "Clebsch-Gordan transform of complex Wigner-D matrices of "
          "degrees L1 and L2.")
    so3_points = get_so3_points(10)[5:6, :]

    D_L1 = wigner_D_complex(so3_points, L1)[:, :, 0]
    D_L2 = wigner_D_complex(so3_points, L2)[:, :, 0]
    D_L = wigner_D_complex(so3_points, L)[:, :, 0]

    D_12_kron = np.kron(D_L1, D_L2)

    CG = clebsch_gordan_matrix_C(L1, L2, L)

    D_L_test = np.dot(np.dot(CG, D_12_kron), CG.T)

    print("Wigner D diff:", np.sum(np.abs(D_L - D_L_test)))
    print("-----------------------------------------------------------------")


def test_CG_transform_real_1(L1, L2, L):
    """Check Clebsch-Gordan transform real."""
    print("-----------------------------------------------------------------")
    print("Check if real Wigner-D matrix of degree L is equal to "
          "Clebsch-Gordan transform of real Wigner-D matrices of "
          "degrees L1 and L2.")
    so3_points = get_so3_points(10)[5:6, :]

    D_L1 = wigner_D_real(so3_points, L1)[:, :, 0]
    D_L2 = wigner_D_real(so3_points, L2)[:, :, 0]
    D_L = wigner_D_real(so3_points, L)[:, :, 0]

    D_12_kron = np.kron(D_L1, D_L2)

    CG = clebsch_gordan_matrix_R(L1, L2, L)

    D_L_test = np.dot(np.dot(CG, D_12_kron), CG.T)

    print("Wigner D diff:", np.sum(np.abs(D_L - D_L_test)))
    print("-----------------------------------------------------------------")


def main():
    """Test Clebsch-Gordan transforms."""

    print("-----------------------------------------------------------------")
    print("Test: L1=even/L2=odd")
    test_CG_transform_complex_1(L1=2, L2=3)
    print("-----------------------------------------------------------------")
    print("Test: L1=even/L2=even")
    test_CG_transform_complex_1(L1=2, L2=4)
    print("-----------------------------------------------------------------")
    print("Test: L1=odd/L2=odd")
    test_CG_transform_complex_1(L1=3, L2=1)
    print("-----------------------------------------------------------------")
    print("Test: L1=odd/L2=even")
    test_CG_transform_complex_1(L1=3, L2=2)
    print("\n\n")
    print("-----------------------------------------------------------------")
    print("Test: L1=even/L2=even/L=even")
    test_CG_transform_complex_2(L1=2, L2=4, L=4)
    print("-----------------------------------------------------------------")
    print("Test: L1=even/L2=odd/L=even")
    test_CG_transform_real_1(L1=2, L2=3, L=4)
    print("-----------------------------------------------------------------")
    print("Test: L1=odd/L2=even/L=even")
    test_CG_transform_real_1(L1=3, L2=4, L=2)
    print("-----------------------------------------------------------------")
    print("Test: L1=odd/L2=odd/L=odd")
    test_CG_transform_real_1(L1=3, L2=3, L=5)


if __name__ == '__main__':
    main()

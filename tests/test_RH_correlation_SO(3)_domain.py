"""Check correlation of SO(3) signals."""
import numpy as np
import time
from fourier_s2cnn.utils.sampling_points import get_so3_points
from fourier_s2cnn.utils.fourier_transforms import wigner_D_real

def main():
    """Test correlation of SO(3) signals."""
    N = np.power(10, 6)
    so3_points = get_so3_points(N)
    so3_test_point = np.reshape(np.asarray([np.pi/4, np.pi/7, np.pi/5]), [1, 3])
    L = 4

    antipodal_symmetry = True

    if antipodal_symmetry:
        s = 2
    else:
        s = 1

    print("Defining RH basis...")
    D, D_test = {}, {}
    for l in range(0, L + 1, s):
        D[l] = wigner_D_real(so3_points, L=l)
        D_test[l] = wigner_D_real(so3_test_point, L=l)

    print("-----------------------------------------------------------------")
    print("Check if correlation in spectral domain at SO(3) at one point "
          "gives close results as correlation in SO(3) at the same point")
    print("-----------------------------------------------------------------")

    for seed_idx in range(10):
        print("Seed:", seed_idx)

        np.random.seed(seed_idx)

        RH1, RH2 = {}, {}
        for l in range(0, L + 1, s):
            RH1[l] = np.random.randn(2 * l + 1, 2 * l + 1)
            RH2[l] = np.random.randn(2 * l + 1, 2 * l + 1)

        print("Computing SO(3) signals for RH coefficients...")
        for l in range(0, L + 1, s):
            if not l:
                so3_signal_1 = np.einsum('ij,ijk->k', RH1[l], D[l])
                so3_signal_2 = np.einsum('ij,ijk->k', RH2[l], D[l])
                so3_signal_2_r = np.einsum('ij,ijk->k', np.dot(D_test[l][:, :, 0], RH2[l]), D[l])
            else:
                so3_signal_1 += np.einsum('ij,ijk->k', RH1[l], D[l])
                so3_signal_2 += np.einsum('ij,ijk->k', RH2[l], D[l])
                so3_signal_2_r += np.einsum('ij,ijk->k', np.dot(D_test[l][:, :, 0], RH2[l]), D[l])

        print("Computing correlation in SO(3) domain...")
        so3_corr = np.sum(so3_signal_1 * so3_signal_2_r) / N * (8 * np.pi ** 2)

        print("Computing correlation in spectral domain...")
        so3_corr_test = 0
        for l in range(0, L + 1, s):
            corr = (8 * np.pi ** 2) / (2 * l + 1) * np.dot(RH1[l], RH2[l].T)
            so3_corr_test += np.sum(corr * D_test[l][:, :, 0])

        print(so3_corr_test, so3_corr)
        print(so3_corr / so3_corr_test)
        print("-----------------------------------------------------------------")


if __name__ == '__main__':
    main()

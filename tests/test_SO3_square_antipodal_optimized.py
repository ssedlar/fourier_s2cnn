"""Check SO(3) square transform."""
import numpy as np
import tensorflow as tf

from fourier_s2cnn.utils.sampling_points import get_so3_points

from fourier_s2cnn.utils.fourier_transforms import rh_basis_real

from fourier_s2cnn.utils.clebsch_gordan import clebsch_gordan_matrix_R
from fourier_s2cnn.utils.clebsch_gordan import CG_dict_3d_R_sparse

import time

def qudratic_non_linearity_3(rh, L_in, L_out, CG_r, CG_l):
    """Perform quadratic non-linearity - with sparse multiplications implementation."""

    rh_l1 = {}
    rh_l2 = {}
    for l in range(0, L_in + 1, 2):
        rh_l1[l] = tf.reshape(tf.transpose(rh[l], (0, 1, 3, 2)), [-1, 2 * l + 1])
        rh_l2[l] = tf.reshape(rh[l], [-1, 2 * l + 1])

    rh_n = {}
    for l in range(0, L_out + 1):
        for l1 in range(0, L_in + 1, 2):
            if l1 == 0:
                n, c, _, _ = rh[l1].shape
            for l2 in range(0, L_in + 1, 2):
                if l1 not in rh or l2 not in rh:
                    continue
                if l2 < l1:
                    continue
                if np.abs(l2 - l1) <= l <= (l1 + l2):

                    cg_r_sparse = CG_r[l][l1, l2]
                    cg_l_sparse = CG_l[l][l1, l2]

                    x = tf.reshape(tf.sparse.sparse_dense_matmul(rh_l2[l2], cg_r_sparse), 
                                   [n, c, 2 * l2 + 1, 2 * l + 1, 2 * l1 + 1])
                    y = tf.reshape(tf.sparse.sparse_dense_matmul(rh_l1[l1], cg_l_sparse), 
                                   [n, c, 2 * l1 + 1, 2 * l + 1, 2 * l2 + 1])
                    z = tf.einsum('ncikl,nclqi->nckq', y, x)

                    if l not in rh_n:
                        rh_n[l] = z
                    else:
                        rh_n[l] += z
    return rh_n


def main():
    """Check SO(3) square transform."""
    N = 1000
    L = 4
    so3_points = get_so3_points(N=N)

    D_r = rh_basis_real(so3_points, L=L, even=True)

    D_r_inv = np.linalg.pinv(D_r)

    so3_signal = np.random.randn(N)

    rh_r = np.dot(D_r_inv, so3_signal)

    so3_signal = np.dot(D_r, rh_r)
    so3_sq = so3_signal * so3_signal

    D_r_out = rh_basis_real(so3_points, L=2 * L, even=False)

    time_start_1 = time.time()
    print("-----------------------------------------------------------------")
    print("Check if square of SO(3) signal in signal domain is equal to "
          "Clebsch-Gordan transform in spectral domain followed by "
          "conversion to signal domain.")
    print("-----------------------------------------------------------------")
    n_rh_out = np.sum([(2 * k + 1) ** 2 for k in range(0, 2 * L + 1)])
    rh_r_test = np.zeros(n_rh_out, dtype=np.float32)

    for l in range(0, 2 * L + 1):
        for l1 in range(0, L + 1, 2):
            for l2 in range(0, L + 1, 2):
                if l < np.abs(l1 - l2) or l > l1 + l2:
                    continue
                # Define Clebsch-Gordan matrices for l1, l2, l
                CG_r = clebsch_gordan_matrix_R(l1, l2, l).T

                # Get starting and ending indices of RH coefficients for l1
                idx_s1 = int(np.sum([(2 * k + 1) ** 2
                                     for k in range(0, l1, 2)]))
                idx_e1 = int(np.sum([(2 * k + 1) ** 2
                                     for k in range(0, l1 + 2, 2)]))
                # Get RH coefficients for l1
                rh_r_l1 = np.reshape(rh_r[idx_s1:idx_e1],
                                     [2 * l1 + 1, 2 * l1 + 1])

                # Get starting and ending indices of RH coefficients for l2
                idx_s2 = int(np.sum([(2 * k + 1) ** 2
                                     for k in range(0, l2, 2)]))
                idx_e2 = int(np.sum([(2 * k + 1) ** 2
                                     for k in range(0, l2 + 2, 2)]))
                # Get RH coefficients for l2
                rh_r_l2 = np.reshape(rh_r[idx_s2:idx_e2],
                                     [2 * l2 + 1, 2 * l2 + 1])

                # Get starting and ending indices of RH coefficients for l
                idx_s = int(np.sum([(2 * k + 1) ** 2
                                    for k in range(0, l)]))
                idx_e = int(np.sum([(2 * k + 1) ** 2
                                    for k in range(0, l + 1)]))
                # Get RH coefficients for l via Clebsch-Gordan transform
                rh_r_l = np.dot(CG_r.T,
                                np.dot(np.kron(rh_r_l1, rh_r_l2), CG_r))
                rh_r_l = np.ravel(rh_r_l)

                rh_r_test[idx_s:idx_e] += rh_r_l

    # Transform squared SO(3) signal from spectral to signal domain
    so3_out_r = np.dot(D_r_out, rh_r_test)
    print("-----------------------------------------------------------------")
    print("ANTIPODAL Diff real naive:", np.sum(np.abs(so3_out_r - so3_sq)))
    print("-----------------------------------------------------------------")
    time_end_1 = time.time()
    print("Time elapsed:", time_end_1 - time_start_1)


    time_start_2 = time.time()
    print("-----------------------------------------------------------------")
    print("Check if square of SO(3) signal in signal domain is equal to "
          "Clebsch-Gordan transform in spectral domain followed by "
          "conversion to signal domain with optimization.")
    print("-----------------------------------------------------------------")
    CG_dict_r_s, CG_dict_l_s = CG_dict_3d_R_sparse(L=L, L_max=2*L)

    rh = {}
    c = 0
    for l in range(0, L + 1, 2):
        rh[l] = tf.convert_to_tensor(np.reshape(rh_r[c:c+(2 * l + 1)**2], 
                                                [1, 1, 2 * l + 1, 2 * l + 1]), dtype=tf.float32)
        c += (2 * l + 1) ** 2

    rh_sq = qudratic_non_linearity_3(rh, L, 2 * L, CG_dict_r_s, CG_dict_l_s)

    c = 0
    for l in range(0, 2 * L + 1):
        D_l = tf.convert_to_tensor(np.reshape(D_r_out[:, c:c + (2 * l + 1) ** 2], 
                                              [-1, 2 * l + 1, 2 * l + 1]), dtype=tf.float32)
        if not l:
            so3_out_r_opt = tf.einsum('pnm,nm->p', D_l, rh_sq[l][0, 0, :, :])
        else:
            so3_out_r_opt += tf.einsum('pnm,nm->p', D_l, rh_sq[l][0, 0, :, :])
        c += (2 * l + 1) ** 2

    print("-----------------------------------------------------------------")
    print("ANTIPODAL Diff real optimized:", np.sum(np.abs(so3_out_r_opt - so3_sq))) 
    print("-----------------------------------------------------------------")
    time_end_2 = time.time()
    print("Time elapsed:", time_end_2 - time_start_2)

if __name__ == '__main__':
    main()

"""Check correlation of SO(3) signals."""
import numpy as np

from fourier_s2cnn.utils.sampling_points import get_so3_points

from fourier_s2cnn.utils.fourier_transforms import rh_basis_real
from fourier_s2cnn.utils.fourier_transforms import rh_basis_complex

from fourier_s2cnn.utils.fourier_transforms import wigner_D_complex
from fourier_s2cnn.utils.fourier_transforms import wigner_D_real


def main():
    """Test correlation of SO(3) signals."""
    N = 1000
    so3_points = get_so3_points(N)
    n = 3  # Select SO(3) rotation sample

    so3_signal_1 = np.random.randn(N)
    so3_signal_2 = np.random.randn(N)

    antipodal_symmetry = False

    if antipodal_symmetry:
        s = 2
    else:
        s = 1

    L = 6
    D_r = rh_basis_real(so3_points, L=L, even=antipodal_symmetry)
    D_c = rh_basis_complex(so3_points, L=L, even=antipodal_symmetry)

    D_r_inv = np.linalg.pinv(D_r)
    D_c_inv = np.linalg.pinv(D_c)

    rh_r_1 = np.dot(D_r_inv, so3_signal_1)
    rh_r_2 = np.dot(D_r_inv, so3_signal_2)
    rh_c_1 = np.dot(D_c_inv, so3_signal_1)
    rh_c_2 = np.dot(D_c_inv, so3_signal_2)

    print("-----------------------------------------------------------------")
    print("Check if correlation of real and complex rotational harmonic"
          " coefficients yields same SO(3) signals")
    c = 0
    for l in range(0, L + 1, s):
        ln = 2 * l + 1
        corr_r = np.dot(np.reshape(rh_r_1[c:c + ln ** 2], [ln, ln]),
                        np.reshape(rh_r_2[c:c + ln ** 2], [ln, ln]).T)
        corr_r *= (8 * np.pi ** 2) / ln
        corr_c = np.dot(np.reshape(rh_c_1[c:c + ln ** 2], [ln, ln]),
                        np.reshape(rh_c_2[c:c + ln ** 2], [ln, ln]).conj().T)
        corr_c *= (8 * np.pi ** 2) / ln

        Rr = wigner_D_real(so3_points[n:n + 1, :], L=l)[:, :, 0]
        Rc = wigner_D_complex(so3_points[n:n + 1, :], L=l)[:, :, 0]

        p_r = np.sum(corr_r * Rr)
        p_c = np.sum(corr_c * Rc)
        c += ln ** 2
        print("SO(3) diff:", l, np.sum(np.abs(p_r - p_c)))
    print("-----------------------------------------------------------------")


if __name__ == '__main__':
    main()

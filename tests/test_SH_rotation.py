"""Check rotation of S2 signals."""
import numpy as np

from fourier_s2cnn.utils.sampling_points import load_s2_points
from fourier_s2cnn.utils.sampling_points import get_so3_points

from fourier_s2cnn.utils.fourier_transforms import sh_basis_real
from fourier_s2cnn.utils.fourier_transforms import sh_basis_complex

from fourier_s2cnn.utils.fourier_transforms import wigner_D_complex
from fourier_s2cnn.utils.fourier_transforms import wigner_D_real


def main():
    """Test rotation of SH coefficients."""
    s2_points = load_s2_points(N=500)
    so3_points = get_so3_points(10)
    n = 5  # Select SO(3) rotation sample
    antipodal_symmetry = False
    if antipodal_symmetry:
        s = 2
    else:
        s = 1

    s2_signal = np.random.randn(500)

    L = 6
    Y_r = sh_basis_real(s2_points, L=L, even=antipodal_symmetry)
    Y_c = sh_basis_complex(s2_points, L=L, even=antipodal_symmetry)

    Y_r_inv = np.linalg.pinv(Y_r)
    Y_c_inv = np.linalg.pinv(Y_c)

    sph_r = np.dot(Y_r_inv, s2_signal)
    sph_c = np.dot(Y_c_inv, s2_signal)

    print("-----------------------------------------------------------------")
    print("Check if rotation of complex and real spherical harmonic"
          " coefficients yields same S2 signals")

    sph_r_rot = np.zeros_like(sph_r)
    sph_c_rot = np.zeros_like(sph_c)

    c = 0
    for l in range(0, L + 1, s):
        Rr = wigner_D_real(so3_points[n:n + 1, :], L=l)[:, :, 0]
        Rc = wigner_D_complex(so3_points[n:n + 1, :], L=l)[:, :, 0].conj()
        sph_r_rot[c:c + 2 * l + 1] = np.dot(Rr, sph_r[c:c + 2 * l + 1])
        sph_c_rot[c:c + 2 * l + 1] = np.dot(Rc, sph_c[c:c + 2 * l + 1])
        c += 2 * l + 1

    s2_signal_rot_r = np.dot(Y_r, sph_r_rot)
    s2_signal_rot_c = np.dot(Y_c, sph_c_rot)

    print("Signal diff:", np.sum(np.abs(s2_signal_rot_r - s2_signal_rot_c)))
    print("-----------------------------------------------------------------")
    print("Check if rotation and de-rotation yields same initial signal")
    rot_angles = so3_points[n:n + 1, :]
    derot_angles = np.zeros((1, 3), dtype=np.float32)
    derot_angles[0, 0] = -rot_angles[0, 0]
    derot_angles[0, 1] = -rot_angles[0, 2]
    derot_angles[0, 2] = -rot_angles[0, 1]

    sph_r_derot = np.zeros_like(sph_r)
    sph_c_derot = np.zeros_like(sph_c)

    c = 0
    for l in range(0, L + 1, s):
        DRr = wigner_D_real(derot_angles, L=l)[:, :, 0]
        DRc = wigner_D_complex(derot_angles, L=l)[:, :, 0].conj()
        sph_r_derot[c:c + 2 * l + 1] = np.dot(DRr, sph_r_rot[c:c + 2 * l + 1])
        sph_c_derot[c:c + 2 * l + 1] = np.dot(DRc, sph_c_rot[c:c + 2 * l + 1])
        c += 2 * l + 1

    print("Real SH diff:", np.sum(np.abs(sph_r_derot - sph_r)))
    print("Complex SH diff:", np.sum(np.abs(sph_c_derot - sph_c)))
    print("-----------------------------------------------------------------")


if __name__ == '__main__':
    main()

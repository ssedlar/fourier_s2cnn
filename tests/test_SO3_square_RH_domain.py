"""Compare quadratic functions with and without optimization."""

import numpy as np
import tensorflow as tf
import time

from fourier_s2cnn.utils.sampling_points import get_so3_points
from fourier_s2cnn.utils.fourier_transforms import rh_basis_real

from fourier_s2cnn.utils.clebsch_gordan import clebsch_gordan_dict_R
from fourier_s2cnn.utils.clebsch_gordan import CG_dict_3d_R
from fourier_s2cnn.utils.clebsch_gordan import CG_dict_3d_R_sparse

def tf_kron(a, b):
    """Perform Kronecker product of RH coefficients over two last dimensions."""
    """
        Arguments:
            a (numpy array Nb x Nc x M x M): 1st input array
            b (numpy array Nb x Nc x N x N): 2nd input array
        Returns:
            numpy array, Kronecker product over last two dimensions
    """
    a_shape = [a.shape[0], a.shape[1], a.shape[2], a.shape[3]]
    b_shape = [b.shape[0], b.shape[1], b.shape[2], b.shape[3]]

    x = tf.einsum('nolk, nopq->nolpkq', a, b)
    x = tf.reshape(x,
                   [a_shape[0], b_shape[1],
                    a_shape[2] * b_shape[2],
                    a_shape[3] * b_shape[3]])
    return x


def qudratic_non_linearity_1(rh, L_in, L_out, CG_dict):
    """Perform quadratic non-linearity - naive implementation."""
    rh_n = {}
    for l in range(0, L_out + 1):
        for l1 in range(0, L_in + 1):
            for l2 in range(0, L_in + 1):
                if l1 not in rh or l2 not in rh:
                    continue
                if l2 < l1:
                    continue
                if np.abs(l2 - l1) <= l <= (l1 + l2):
                    cg_r = CG_dict[l][l1, l2]

                    if l not in rh_n:
                        rh_n[l] = tf.einsum('ncoi,ki->ncok',
                                            tf.einsum('oi,ncik->ncok',
                                                      cg_r, tf_kron(rh[l1], rh[l2])),
                                            cg_r)
                    else:
                        rh_n[l] += tf.einsum('ncoi,ki->ncok',
                                             tf.einsum('oi,ncik->ncok',
                                                       cg_r, tf_kron(rh[l1], rh[l2])),
                                             cg_r)
    return rh_n

def qudratic_non_linearity_2(rh, L_in, L_out, CG_r, CG_l):
    """Perform quadratic non-linearity - optimized implementation."""
    rh_n = {}
    for l in range(0, L_out + 1):
        for l1 in range(0, L_in + 1):
            if l1 == 0:
                n, c, _, _ = rh[l1].shape
            for l2 in range(0, L_in + 1):
                if l1 not in rh or l2 not in rh:
                    continue
                if l2 < l1:
                    continue
                if np.abs(l2 - l1) <= l <= (l1 + l2):

                    cg_r = CG_r[l][l1, l2]
                    cg_l = CG_l[l][l1, l2]

                    #x = tf.einsum('ncij,klj->nckli', rh[l2], cg_r)
                    #y = tf.einsum('ncji,klj->nckil', rh[l1], cg_l)
                    z = tf.einsum('nckij,ncqij->nckq', 
                                  tf.einsum('ncji,klj->nckil', rh[l1], cg_l), 
                                  tf.einsum('ncij,klj->nckli', rh[l2], cg_r))

                    if l not in rh_n:
                        rh_n[l] = z
                    else:
                        rh_n[l] += z
    return rh_n


def qudratic_non_linearity_3(rh, L_in, L_out, CG_r, CG_l):
    """Perform quadratic non-linearity - with sparse multiplications implementation."""
    rh_l1 = {}
    rh_l2 = {}
    for l in range(0, L_in + 1):
        rh_l1[l] = tf.reshape(tf.transpose(rh[l], (0, 1, 3, 2)), [-1, 2 * l + 1])
        rh_l2[l] = tf.reshape(rh[l], [-1, 2 * l + 1])

    rh_n = {}
    for l in range(0, L_out + 1):
        for l1 in range(0, L_in + 1):
            if l1 == 0:
                n, c, _, _ = rh[l1].shape
            for l2 in range(0, L_in + 1):
                if l1 not in rh or l2 not in rh:
                    continue
                if l2 < l1:
                    continue
                if np.abs(l2 - l1) <= l <= (l1 + l2):

                    cg_r_sparse = CG_r[l][l1, l2]
                    cg_l_sparse = CG_l[l][l1, l2]

                    x = tf.reshape(tf.sparse.sparse_dense_matmul(rh_l2[l2], cg_r_sparse), 
                                   [n, c, 2 * l2 + 1, 2 * l + 1, 2 * l1 + 1])
                    y = tf.reshape(tf.sparse.sparse_dense_matmul(rh_l1[l1], cg_l_sparse), 
                                   [n, c, 2 * l1 + 1, 2 * l + 1, 2 * l2 + 1])
                    z = tf.einsum('ncikl,nclqi->nckq', y, x)

                    if l not in rh_n:
                        rh_n[l] = z
                    else:
                        rh_n[l] += z
    return rh_n


def main():

    L = 8
    """Compare quadratic function with and without optimization."""
    print("-----------------------------------------------------------------")
    print("Compare quadratic function with and without optimization.")
    print("-----------------------------------------------------------------")

    print("-----------------------------------------------------------------")
    print("Create dictionaries with Clebsch-Gordan matrices...")
    print("-----------------------------------------------------------------")
    CG_dict = clebsch_gordan_dict_R(L=L, L_max=L)
    CG_dict_r, CG_dict_l = CG_dict_3d_R(L=L, L_max=L)
    CG_dict_r_s, CG_dict_l_s = CG_dict_3d_R_sparse(L=L, L_max=L)

    print("-----------------------------------------------------------------")
    print("Generate random RH coefficients feature maps...")
    print("-----------------------------------------------------------------")
    np.random.seed(123)
    rh = {}
    for i in range(0, L + 1):
        rh[i] = \
            tf.convert_to_tensor(np.random.randn(256, 16, 2 * i + 1, 2 * i + 1),
                                 dtype=tf.float32)
    print("-----------------------------------------------------------------")
    print("Estimate elapsed times...")
    time_start = time.time()
    rh_sq_1 = qudratic_non_linearity_1(rh, L_in=L, L_out=L, CG_dict=CG_dict)
    time_end = time.time()
    print("Time elapsed (naive implementation):", time_end - time_start)
    print("-----------------------------------------------------------------")

    print("-----------------------------------------------------------------")
    time_start = time.time()
    rh_sq_2 = qudratic_non_linearity_2(rh, L_in=L, L_out=L,
                                       CG_r=CG_dict_r, CG_l=CG_dict_l)
    time_end = time.time()
    print("Time elapsed (optimized):", time_end - time_start)
    print("-----------------------------------------------------------------")

    print("-----------------------------------------------------------------")
    time_start = time.time()
    rh_sq_3 = qudratic_non_linearity_3(rh, L_in=L, L_out=L,
                                       CG_r=CG_dict_r_s, CG_l=CG_dict_l_s)
    time_end = time.time()
    print("Time elapsed (optimized with sparse matrices):", time_end - time_start)
    print("-----------------------------------------------------------------")

    print("-----------------------------------------------------------------")
    print("Compare estimated RH coefficients per degree...")
    for l in rh_sq_1:
        print(l, np.mean(np.abs(rh_sq_1[l] - rh_sq_2[l])))
        print(l, np.mean(np.abs(rh_sq_1[l] - rh_sq_3[l])))
        print("\n")
    print("-----------------------------------------------------------------")


if __name__ == '__main__':
    main()

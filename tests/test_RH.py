"""Check RH basis definitions, conversions and inversions."""
import numpy as np

from fourier_s2cnn.utils.sampling_points import get_so3_points

from fourier_s2cnn.utils.fourier_transforms import wigner_D_complex
from fourier_s2cnn.utils.fourier_transforms import wigner_D_real

from fourier_s2cnn.utils.fourier_transforms import rh_basis_real
from fourier_s2cnn.utils.fourier_transforms import rh_basis_complex

from fourier_s2cnn.utils.fourier_transforms import convert_real_to_complex
from fourier_s2cnn.utils.fourier_transforms import convert_complex_to_real


def check_real_complex_conversion(L=6):
    """Check conversion of real to complex Wigner-D basis."""
    """
        Arguments:
            L (int): signal bandwidth
    """
    so3_points = get_so3_points(N=200)
    n = 10  # Select SO(3) rotation sample

    print("-----------------------------------------------------------------")
    print("Check conversion from real to complex Wigner-D basis")
    for l in range(0, L + 1):
        wd_r = wigner_D_real(so3_points, L=l)
        wd_c = wigner_D_complex(so3_points, L=l)
        M = convert_real_to_complex(L=l)
        M_inv = convert_complex_to_real(L=l)
        wd_c_test = np.dot(np.dot(M, wd_r[:, :, n]), M_inv)
        print("Diff:", l, np.sum(np.abs(wd_c[:, :, n] - wd_c_test)))
        print("\n")
    print("-----------------------------------------------------------------")


def check_complex_real_conversion(L=6):
    """Check conversion of complex to real Wigner-D basis."""
    """
        Arguments:
            L (int): signal bandwidth
    """
    so3_points = get_so3_points(N=200)
    n = 10  # Select SO(3) rotation sample

    print("-----------------------------------------------------------------")
    print("Check conversion from complex to real Wigner-D basis")
    for l in range(0, L + 1):
        wd_r = wigner_D_real(so3_points, L=l)
        wd_c = wigner_D_complex(so3_points, L=l)
        M = convert_real_to_complex(L=l)
        M_inv = convert_complex_to_real(L=l)
        wd_r_test = np.dot(np.dot(M_inv, wd_c[:, :, n]), M)
        print("Diff:", l, np.sum(np.abs(wd_r[:, :, n] - wd_r_test)))
        print("\n")
    print("-----------------------------------------------------------------")


def check_real_and_complex_RH(L=6, N=2000):
    """Check RH estimation with real and complex RH basis."""
    """
        Arguments:
            L (int): signal bandwidth
            N (int): number of sampling points
    """
    print("-----------------------------------------------------------------")
    print("Check if real and complex RH coefficients yield same "
          "SO(3) signals")

    so3_points = get_so3_points(N=N)
    D_r = rh_basis_real(so3_points, L=L)
    D_c = rh_basis_complex(so3_points, L=L)

    D_r_inv = np.linalg.pinv(D_r)
    D_c_inv = np.linalg.pinv(D_c)

    so3_signal = np.random.randn(N)

    rh_r = np.dot(D_r_inv, so3_signal)
    rh_c = np.dot(D_c_inv, so3_signal)

    so3_test_r = np.dot(D_r, rh_r)
    so3_test_c = np.dot(D_c, rh_c)

    print("SO(3) diff:", np.sum(np.abs(so3_test_r - so3_test_c)))
    print("-----------------------------------------------------------------")


def main():
    """Run test functions for Wigner-D basis."""
    check_real_complex_conversion(L=6)
    check_complex_real_conversion(L=6)
    check_real_and_complex_RH(L=6)


if __name__ == '__main__':
    main()

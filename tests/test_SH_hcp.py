"""Estimate SH coefficients from full sampling scheme using LMS."""
import os
import argparse

import numpy as np
import nibabel as nib

from fourier_s2cnn.utils.coordinate_conversions import convert_cart_to_s2

from fourier_s2cnn.utils.fourier_transforms import sh_basis_real


def load_acquisition_scheme(path_to_bvals, path_to_bvecs):
    """Load b values and b vectors."""
    """
        Arguments:
            path_to_bvals (str): path to bvalues file
            path_to_bvecs (str): path to bvecs file
        Returns:
            bvalues (numpy array)
            bvecs (nummpy array)
    """
    with open(path_to_bvals) as f:
        bvals_str = f.readlines()[0].strip()
        bvals = [int(b) for b in str.split(bvals_str)]
    bvals = np.asarray(bvals)

    with open(path_to_bvecs) as f:
        bvecs_str = f.readlines()
        bvecs = []
        for bv_str in bvecs_str:
            bv = [float(bv) for bv in str.split(bv_str.strip())]
            bvecs.append(bv)
    bvecs = np.asarray(bvecs).T

    return bvals, bvecs


def normalize_data(dMRI_data, mask, bvals):
    """Normalize data."""
    """
        Arguments:
            dMRI_data (4D numpy array): diffusion MRI data
            mask (3D numpy array): brain mask
            bvals (1D numpy array): bvalues

        Returns:
            (4D numpy array): normalized diffusion MRI data
    """
    dMRI_data_n = np.zeros_like(dMRI_data)

    idx_0 = np.where(bvals < 50)[0]
    b0_mean = np.mean(dMRI_data[:, :, :, idx_0], axis=3)

    dMRI_norm = (b0_mean[mask == 1] + 1.e-12)
    dMRI_data_n[mask == 1, :] = (dMRI_data[mask == 1, :].T / dMRI_norm).T

    dMRI_data_n_clip = np.clip(dMRI_data_n, 0., 1.)

    return dMRI_data_n_clip


def compute_SH_coeffs(dMRI_data, mask, bvals, bvecs, bval, L=8):
    """Compute SH coeffs for a shell."""
    """
        Arguments:
            dMRI_data (4D numpy array): normalized diffusion MRI data
            mask (3D numpy array): brain mask
            bvals (1D numpy array): bvalues
            bvecs (3D numpy array): bvecs
            bval (float): b-value to select
            L (int): S2 signal bandwidth
        Return:
            sh_data (4D numpy array): estimated SH harmonics
    """
    idx = np.where(np.abs(bvals - bval) < 50)[0]

    bvecs_shell_cart = bvecs[idx, :]

    bvecs_shell_sph = convert_cart_to_s2(bvecs_shell_cart)

    Y_r = sh_basis_real(bvecs_shell_sph, L=L)
    Y_r_inv = np.linalg.pinv(Y_r).T

    n_sph = np.sum([2 * l + 1 for l in range(0, L + 1, 2)])
    w, h, d, _ = dMRI_data.shape
    sh_data = np.zeros((w, h, d, n_sph))

    sh_data[mask == 1, :] = np.dot(dMRI_data[mask == 1, :][:, idx], Y_r_inv)

    return sh_data


def main():
    """Run test functions."""
    print("-----------------------------------------------------------------")
    print("Estimate per shell spherical harmonic coefficients using " +
          "least mean square")
    parser = argparse.\
        ArgumentParser(description='Estimation of spherical harmonic ' +
                       'coefficients with least mean square')
    parser.add_argument('-data_path', dest='data_path', required=True,
                        help='Path to dMRI data for one HCP subject')
    parser.add_argument('-out_path', dest='out_path', required=True,
                        help='Path where to save estimated per shell' +
                        'spherical harmonic coefficients ')
    parser.add_argument('-L', dest='L', required=True, type=int,
                        help='Spherical harmonic bandwidth')
    args = parser.parse_args()

    path_to_bvals = os.path.join(args.data_path, 'bvals')
    path_to_bvecs = os.path.join(args.data_path, 'bvecs')
    bvals, bvecs = load_acquisition_scheme(path_to_bvals, path_to_bvecs)

    path_to_dmri_data = os.path.join(args.data_path, 'data.nii.gz')
    path_to_mask = os.path.join(args.data_path, 'nodif_brain_mask.nii.gz')

    header = nib.load(path_to_dmri_data).header
    dMRI_data = nib.load(path_to_dmri_data).get_data()
    mask = nib.load(path_to_mask).get_data()

    dMRI_data_n = normalize_data(dMRI_data, mask, bvals)

    sh_1000 = compute_SH_coeffs(dMRI_data_n, mask, bvals, bvecs, bval=1000, L=args.L)
    sh_2000 = compute_SH_coeffs(dMRI_data_n, mask, bvals, bvecs, bval=2000, L=args.L)
    sh_3000 = compute_SH_coeffs(dMRI_data_n, mask, bvals, bvecs, bval=3000, L=args.L)

    print("Note: There is (-1) factor difference between " +
          "SH basis of positive order used here and in mrtrix")
    count = 0
    for i in range(0, args.L + 1, 2):
        for m in range(-i, i + 1):
            if m < 0:
                sh_1000[:, :, :, count] *= -1
                sh_2000[:, :, :, count] *= -1
                sh_3000[:, :, :,  count] *= -1
            count += 1

    header['dim'][4] = np.sum([2*i+1 for i in range(0, args.L+1, 2)])
    sh_data_path = os.path.join(args.out_path, 'sh_b_{0}_L_{1}.nii.gz')
    sh_data_nii_1000 = nib.nifti1.Nifti1Image(sh_1000, None, header=header)
    sh_data_nii_2000 = nib.nifti1.Nifti1Image(sh_2000, None, header=header)
    sh_data_nii_3000 = nib.nifti1.Nifti1Image(sh_3000, None, header=header)

    nib.save(sh_data_nii_1000, sh_data_path.format(1000, args.L))
    nib.save(sh_data_nii_2000, sh_data_path.format(2000, args.L))
    nib.save(sh_data_nii_3000, sh_data_path.format(3000, args.L))

if __name__ == '__main__':
    main()

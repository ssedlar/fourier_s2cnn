"""Check S2 square transform."""
import numpy as np
from sympy.physics.quantum.cg import CG

from fourier_s2cnn.utils.sampling_points import load_s2_points

from fourier_s2cnn.utils.fourier_transforms import sh_basis_real
from fourier_s2cnn.utils.fourier_transforms import sh_basis_complex

from fourier_s2cnn.utils.clebsch_gordan import clebsch_gordan_matrix_R
from fourier_s2cnn.utils.clebsch_gordan import clebsch_gordan_matrix_C


def main():
    """Check S2 square transform."""
    print("-----------------------------------------------------------------")
    print("Check square of S2 function in spectral domain.")
    print("Verify if square in S2 domain is equal to ")
    print("Clebsch-Gordan transform of real and complex SH coefficients.")
    print("-----------------------------------------------------------------")

    N = 500
    L = 6
    antipodal_symmetry = True

    if antipodal_symmetry:
        s = 2
    else:
        s = 1

    s2_points = load_s2_points(N=N)

    Y_r = sh_basis_real(s2_points, L=L, even=antipodal_symmetry)
    Y_c = sh_basis_complex(s2_points, L=L, even=antipodal_symmetry)

    Y_r_inv = np.linalg.pinv(Y_r)
    Y_c_inv = np.linalg.pinv(Y_c)

    s2_signal = np.random.randn(N)

    sh_r = np.dot(Y_r_inv, s2_signal)
    sh_c = np.dot(Y_c_inv, s2_signal)
    sh_r_dict = {}
    sh_c_dict = {}
    c = 0
    for l in range(0, L + 1, s):
        sh_r_dict[l] = sh_r[c:c + 2 * l + 1]
        sh_c_dict[l] = sh_c[c:c + 2 * l + 1]
        c += 2 * l + 1

    s2_signal = np.dot(Y_r, sh_r)
    s2_sq = s2_signal * s2_signal

    sh_n_r = {}
    sh_n_c = {}
    for l in range(0, 2*L + 1, s):
        for l1 in range(0, L + 1, s):
            for l2 in range(0, L + 1, s):
                if np.abs(l2 - l1) <= l <= (l1 + l2):

                    cg_matrix_R = clebsch_gordan_matrix_R(l1, l2, l)
                    cg_matrix_C = clebsch_gordan_matrix_C(l1, l2, l)

                    gaunt = np.sqrt((2 * l1 + 1) * (2 * l2 + 1) /
                                    (4 * np.pi * (2 * l + 1)))
                    cg_const = float(CG(l1, 0, l2, 0, l, 0).doit())
                    gaunt *= cg_const
                    cg_matrix_R *= gaunt
                    cg_matrix_C *= gaunt

                    kron_r = np.ravel(np.einsum('l,k->lk', sh_r_dict[l1], sh_r_dict[l2]))
                    kron_c = np.ravel(np.einsum('l,k->lk', sh_c_dict[l1], sh_c_dict[l2]))

                    if l not in sh_n_r:
                        sh_n_r[l] = np.einsum('k,lk->l', kron_r, cg_matrix_R)
                        sh_n_c[l] = np.einsum('k,lk->l', kron_c, cg_matrix_C)
                    else:
                        sh_n_r[l] += np.einsum('k,lk->l', kron_r, cg_matrix_R)
                        sh_n_c[l] += np.einsum('k,lk->l', kron_c, cg_matrix_C)

    Y_r_2 = sh_basis_real(s2_points, L=2*L, even=antipodal_symmetry)
    Y_c_2 = sh_basis_complex(s2_points, L=2*L, even=antipodal_symmetry)

    n_sph = np.sum([2 * l + 1 for l in range(0, 2 * L + 1, s)])
    sh_sq_r = np.zeros(n_sph, dtype=np.float32)
    sh_sq_c = np.zeros(n_sph, dtype=np.complex64)

    c = 0
    for l in range(0, 2 * L + 1, s):
        sh_sq_r[c:c + 2 * l + 1] = sh_n_r[l]
        sh_sq_c[c:c + 2 * l + 1] = sh_n_c[l]
        c += 2 * l + 1

    s2_sq_r_test = np.dot(Y_r_2, sh_sq_r)
    s2_sq_c_test = np.dot(Y_c_2, sh_sq_c)
    '''
    import matplotlib.pyplot as plt
    plt.figure(1)
    plt.plot(s2_sq_r_test, 'b')
    plt.plot(s2_sq, 'r')
    plt.figure(2)
    plt.plot(s2_sq_c_test, 'b')
    plt.plot(s2_sq, 'r')
    plt.show()
    '''
    print("-----------------------------------------------------------------")
    print("Differences between square realized in S2 or in Fourier domain:")
    print("-----------------------------------------------------------------")
    print("With CG transform of real SH coefficients:",
          np.mean(np.abs(s2_sq-s2_sq_r_test) / (np.abs(s2_sq) + 1.e-8)))
    print("With CG transform of complex SH coefficients:",
          np.mean(np.abs(s2_sq-s2_sq_c_test) / (np.abs(s2_sq) + 1.e-8)))
    print("-----------------------------------------------------------------")


if __name__ == '__main__':
    main()

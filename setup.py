"""Setup script."""
from setuptools import setup, find_packages

setup(
    name='fourier_s2cnn',
    version='0.0.0',
    packages=find_packages(),
    url='https://gitlab.inria.fr/ssedlar/fourier_s2cnn',
    license='MIT',
    author='ATHENA Team, Inria',
    author_email='',
    description='A Python package that implements Fourier ' +
        'domain spherical CNN for dMRI analysis.'
)

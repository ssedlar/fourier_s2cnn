import wigner as w
from sympy.physics.quantum.spin import Rotation
import numpy as np
#from dmipy.utils.invariants import *
from dipy.core.geometry import cart2sphere
from scipy.special import lpn, lpmv, gammaln,factorial,erf, gamma, hyp1f1
import matplotlib.pyplot as plt
import spherical_functions as sf

def _LegendreP(m, l, x):
    return (-1)**m * lpmv(m, l, x)



def gaunt_sph_harm(m, l, theta, phi):
    r""" Compute spherical harmonics as defined in [1]
    Parameters
    ----------
    m : int,
        The order of the harmonic.
    l : int,
        The degree of the harmonic.
    theta : float,
        The azimuthal coordinate.
    phi : float,
        The polar coordinate.

    Returns
    --------
    val : float,
        The SH $y^m_l$ sampled at `theta` and `phi`.

    See Also
    --------
    [1] Homeier, Herbert HH, and E. Otto Steinborn. 'Some properties of the
    coupling coefficients of real spherical harmonics and their relation to
    Gaunt coefficients.' Journal of Molecular Structure: THEOCHEM 368 (1996):
    31-37.
    """
    x = np.cos(theta)
    am = np.abs(m)
    val = (1j)**(m+am)
    if l+am == 0:
        f = 1.0
    else:
        f = factorial(l-am)/factorial(l+am)

    val *= np.sqrt((2*l+1)/(4*np.pi) * f).astype(complex)
    val *= _LegendreP(am, l, x).astype(complex)
    val *= np.exp(1j*m*phi)
    return val

def sf_wigner_matrix(l, alpha, beta, eta):
    D = np.zeros((2*l+1,2*l+1), dtype=np.complex)
    for mp in range(-l,l+1):
        for m in range(-l,l+1):
            D[mp+l,m+l] = sf.Wigner_D_element(alpha, beta, eta, l, mp, m)
    return D


def watson_distribution(v, k, vecs):
    w = np.exp(k * np.dot(vecs,v)**2)
    w/= hyp1f1(1./2, 3./2, k)
    return w

def watson_harmonics_coeff(v,k, sh_order):
    r, theta, phi = cart2sphere(v[0], v[1], v[2])
    n_c = (sh_order+1)**2

    coef = np.zeros(n_c, dtype=np.complex)
    M = 1.0/hyp1f1(1./2, 3./2, k)

    counter = 0
    for l in range(0,sh_order+1):
        for m in range(-l,l+1):
            coef[counter] = M*2*np.pi*gaunt_sph_harm(m, l, theta, phi)
            coef[counter] *= k**(l//2) * gamma(l//2 + 0.5) / gamma(l+1.5)
            coef[counter] *= hyp1f1(l//2 +1./2, l + 3./2, k)
            counter+=1

    return coef/(4*np.pi)


def zyz_rotmat(a,b,g):
    R = np.zeros((3,3))
    R[0,0] = np.cos(a)*np.cos(b)*np.cos(g) - np.sin(a)*np.sin(g)
    R[0,1] = -np.cos(g)*np.sin(a) -np.cos(a)*np.cos(b) *np.sin(g)
    R[0,2] = np.cos(a)*np.sin(b)

    R[1,0] = np.cos(a)*np.sin(g) + np.cos(b)*np.cos(g)*np.sin(a)
    R[1,1] = np.cos(a) *np.cos(g) - np.cos(b) * np.sin(a) * np.sin(g)
    R[1,2] = np.sin(a)*np.sin(b)

    R[2,0] = -np.cos(g)*np.sin(b)
    R[2,1] = np.sin(b) * np.sin(g)
    R[2,2] = np.cos(b)

    return R


sh_order = 8

va = np.random.randn(3)
va = va/np.linalg.norm(va)


vb = np.random.randn(3)
vb = vb/np.linalg.norm(vb)

k = 100

coef  = 0.5* watson_harmonics_coeff(va,k, sh_order)+0.5* watson_harmonics_coeff(vb,k, sh_order)

alpha = np.random.rand()*2*np.pi
beta = np.random.rand()*2*np.pi
eta = np.random.rand()*2*np.pi


R = zyz_rotmat(alpha, beta, eta)


v1a = np.dot(R,va)
v1b = np.dot(R,vb)


coef1  = 0.5* watson_harmonics_coeff(v1a,k, sh_order)+0.5* watson_harmonics_coeff(v1b,k, sh_order)

n_c = (sh_order+1)**2

coef2 = np.zeros(n_c, dtype=np.complex)

for l in range(0,sh_order+1):
    start = l**2
    stop = (l+1)**2

    D = w.D_matrix(l, alpha, beta, eta)

    coef2[start:stop] = np.dot(D, coef[start:stop])
#
# for l in range(0,sh_order+1):
#     D = np.zeros((2*l+1,2*l+1), dtype=np.complex)
#
#
#     for mp in range(-l,l+1):
#         for m in range(-l,l+1):
#             D[mp+l,m+l] = sf.Wigner_D_element(alpha, beta, eta, l, mp, m)
#
#     start = l**2
#     stop = (l+1)**2
#
#     # D = w.D_matrix(l, alpha, beta, eta)
#
#     coef2[start:stop] = np.dot(D, coef[start:stop])




plt.figure()
plt.plot(np.real(coef1))
plt.plot(np.real(coef2))
plt.title('real')
plt.show()


plt.figure()
plt.plot(np.imag(coef1))
plt.plot(np.imag(coef2))
plt.title('imag')
plt.show()
    # v1 = np.random.randn(3)#*2 -1.0
    # v1 = v1 / np.linalg.norm(v1)
    # v2 = np.random.randn(3)#*2 -1.0
    # v2 = v2 / np.linalg.norm(v2)
    #
    # angles = min(np.arccos(np.clip(np.dot(v1, v2), -1, 1)), np.pi - np.arccos(np.clip(np.dot(v1, v2), -1, 1)))
    # # angoli[i] = np.rad2deg(angles)
    # angoli[i] = np.dot(v1,v2)
    #
    # coef1  = watson_harmonics_coeff(v1,k, sh_order)
    # coef2  = watson_harmonics_coeff(v2,k, sh_order)
    #
    # coef[i,:] = 0.5*coef1 + 0.5*coef2


#
# for l in range(0, sh_order+1):
#     for m in range(-l,l+1):
#         for mp in range(-l,l+1):
#             beta = np.random.rand()*np.pi*2
#             uno = w.d(l,mp,m,beta)
#             due = np.real(Rotation.d(l,mp,m,beta).doit().evalf())
#             if np.isnan(uno):
#                 print([l,m,mp])
#             else:
#                 err = np.abs(uno-due)
#                 if err > 1e-09:
#                     print([l,m,mp,err])

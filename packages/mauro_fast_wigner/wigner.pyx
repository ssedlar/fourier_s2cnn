cimport numpy as np
import numpy as np
cimport cython
from numpy cimport float64_t, ndarray, complex128_t

from cpython cimport array
from cython.parallel import parallel, prange
from libc.math cimport sqrt, cos, log, sin
cdef extern from "numpy/npy_math.h":
    long double sqrtl(long double x)nogil

cdef extern from "math.h":
    double roundf(double x)nogil
    double pow(double x, double y)nogil

ctypedef complex128_t COMPLEX
ctypedef float64_t DOUBLE
cdef extern from "complex.h":
    double creal(COMPLEX x)nogil


@cython.nonecheck(False)
@cython.boundscheck(False)
@cython.wraparound(False)
cdef long double fact_cy(int n):
    cdef int i
    cdef long double ret
    ret = 1
    for i in range(n):
        ret *= (i+1)
    return ret

@cython.nonecheck(False)
@cython.boundscheck(False)
@cython.wraparound(False)
def my_factorial(int n):
    return fact_cy(n)


@cython.nonecheck(False)
@cython.boundscheck(False)
@cython.wraparound(False)
def my_factorial_product(int n):
    cdef long double res
    res = fact_cy(n)*fact_cy(n)
    return res



@cython.nonecheck(False)
@cython.boundscheck(False)
@cython.wraparound(False)
def d(int j, int mp, int m, double beta):
    cdef long double sb
    cdef long double ro
    sb = fact_cy(j+mp) * fact_cy(j-mp) * fact_cy(j+m)* fact_cy(j-m)

    ro = sqrtl(sb)


    cdef int s_min, s_max

    if j+m < j-mp:
        s_max = j+m
    else:
        s_max= j-mp

    if s_max > 2*j:
        s_max = -2*j

    if s_max < 0:
        s_max = 0

    if m-mp > 0:
        s_min = m-mp
    else:
        s_min = 0

    if s_min < -2*j:
        s_min = -2*j


    cdef int s
    cdef long double num, dem, total=0
    for s in range(s_min,s_max+1):
        num =<long double> pow(-1, mp-m+s) * pow(cos(beta/2.0),2*j+m-mp-2*s) * pow(sin(beta/2.0),mp-m+2*s)
        dem =<long double> fact_cy(j+m-s)*fact_cy(s)*fact_cy(mp-m+s)*fact_cy(j-mp-s)
        total += num/dem

    return ro*total


@cython.nonecheck(False)
@cython.boundscheck(False)
@cython.wraparound(False)
def D_matrix(int l, double alpha, double beta, double gamma):
    cdef ndarray[COMPLEX, ndim=2, mode='c'] D = np.zeros((2*l+1, 2*l+1), dtype=np.complex128)
    cdef int m, mp

    for m in range(-l, l+1):
        for mp in range(-l,l+1):
            D[mp+l, m+l] = np.exp(1j*mp*alpha) * d(l,mp,m,beta) * np.exp(1j*m*gamma)

    return D

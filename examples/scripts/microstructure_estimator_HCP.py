"""Script that runs microstructure estimation."""

import sys
import os
import argparse

from fourier_s2cnn.load_config_file import load_config_file


def main():
    """Function that runs microstructure estimation."""
    """Training, validation and testing."""
    parser = argparse.\
        ArgumentParser(description='Model training, validation and testing.')
    parser.add_argument('-config', dest='config_path', required=True,
                        help='Path to the configuration file.')
    parser.add_argument('-db_in', dest='db_in', required=True,
                        help='Path to the input dataset.')
    parser.add_argument('-exp_out', dest='exp_out', required=True,
                        help='Path to the experiment output.')
    parser.add_argument('-is_s2', dest='is_s2', required=False, type=bool, 
                        default=True,
                        help='Does model take into account spherical nature.')

    args = parser.parse_args()

    if not os.path.exists(args.config_path):
        print("\nConfiguration file does not exist!\n")
        sys.exit(2)

    # 1. Loading configuration file
    modules_objects = load_config_file(args.config_path)

    # 2. Setting up database path and loading subjects list
    db = modules_objects['database']
    db.set_database_path(args.db_in)
    db.set_dmri_template_path()
    db.load_subjects()

    # 3. Setting up preprocessor and preparing data for the estimator
    prep = modules_objects['preprocessor']
    prep.set_exp_out_path(args.exp_out)
    prep.set_prep_src_dir('/'.join(os.path.abspath(__file__).split('/')[:-3]))
    prep.generalize_data_names(db)
    prep.prepare_data(db)
    prep.load_info(db)

    # 4. Setting up estimator
    estim = modules_objects['estimator']
    if args.is_s2:
        estim.load_SH_basis(db, prep)
    estim.set_exp_out_path(args.exp_out)

    # 5. Estimator's training (and validation) and testing at the end
    estim.train_and_valid(db, prep, is_s2=args.is_s2)


if __name__ == '__main__':
    main()
